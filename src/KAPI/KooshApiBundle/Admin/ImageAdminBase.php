<?php

namespace KAPI\KooshApiBundle\Admin;



use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Component\Routing\Exception\RouteNotFoundException;

use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

use Knp\Menu\ItemInterface as MenuItemInterface;

use KAPI\KooshApiBundle\Form\Type\CustomButtonType;

class ImageAdminBase extends Admin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('koosh')
            ->add('user')
            ->add('nudged')
            ->add('created')
            ->add('updated')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('title')    
            ->add('koosh')
            ->add('user')
            ->add('nudged')
            ->add('image', 'string', array('template' => 'KAPIKooshApiBundle:AdminTemplate:list_image.html.twig'))
            ->add('created')
            ->add('updated')
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        // get the current Image instance
        $image = $this->getSubject();

        // use $fileFieldOptions so we can add other options to the field
        $imageFileFieldOptions = array('required' => false, 'data_class' => null);
        if ($image && ($imageFileWebPath = $image->getImageFileWebPath())) {
            // get the container so the full path to the image can be set
            $container = $this->getConfigurationPool()->getContainer();
            $fullPath = $container->get('request')->getBasePath().'/'.$imageFileWebPath;

            // add a 'help' option containing the preview's img tag
            $imageFileFieldOptions['help'] = '<img src="'.$fullPath.'" class="admin-preview" height="240" />';
        }
        
        
        $formMapper
            //->add('id')
            ->add('title')
            ->add('caption')
            ->add('koosh')
            ->add('user')
            //->add('actionField', 'customButton', array('route'=>'admin_kapi_kooshapi_video_edit','params'=>array('id'=>$video->getId()), 'button_name' => 'EDIT'))
            ->add('nudged', 'choice', array('choices' => array(
                                                            '0' => 'No',
                                                            '1' => 'Yes' 
                                                        ))) 
            ->add('imageFile', 'file', $imageFileFieldOptions)
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('title')
            ->add('koosh')
            ->add('user')
            ->add('nudged')
            ->add('created')
            ->add('updated')
        ;
    }
    
    
    public function prePersist($image)
    {
        if(is_object($image->getUser())) {
            $image->setUserId($image->getUser()->getId());
        }
        
    }
    
    public function preUpdate($user)
    {
        
    }
    
    public function postPersist($user)
    {   
        //$this->update($user);
    }
    
    public function setVideoManager(ImageManagerInterface $imageManager)
    {
        $this->imageManager = $imageManager;
    }

    /**
     * @return ImageManagerInterface
     */
    public function getImageManager()
    {
        return $this->imageManager;
    }
    
    
    // In your Admin class

    public function getBatchActions()
    {
        // retrieve the default batch actions (currently only delete)
        $actions = parent::getBatchActions();

        if (
          $this->hasRoute('edit') && $this->isGranted('EDIT') &&
          $this->hasRoute('delete') && $this->isGranted('DELETE')
        ) {
            $actions['sendInfo'] = array(
                'label' => $this->trans('action_send_info', array(), 'SonataAdminBundle'),
                'ask_confirmation' => true
            );

        }

        return $actions;
    }
    
   
}
