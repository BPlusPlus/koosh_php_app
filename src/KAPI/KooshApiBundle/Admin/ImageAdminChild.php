<?php

namespace KAPI\KooshApiBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Component\Routing\Exception\RouteNotFoundException;

use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

use Knp\Menu\ItemInterface as MenuItemInterface;

use KAPI\KooshApiBundleBundle\Entity\KooshComment;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;

use KAPI\KooshApiBundle\Form\Type\CustomButtonType;
use KAPI\KooshApiBundle\Form\Type\VideoPreviewType;
use KAPI\KooshApiBundle\Form\Type\ImagePreviewType;

class ImageAdminChild extends ImageAdminBase 
{
    protected $parentAssociationMapping = 'koosh';
    
    
    public function getParentAssociationMapping()
    {
        return 'koosh';
    }
    
    /**
    * @return Koosh
    */
   protected function getKoosh()
   {
       $admin = $this->getConfigurationPool()->getContainer()->get('kapi_kooshApi.admin.koosh');
       $admin->setRequest($this->getRequest());
       $admin->getObject($admin->getIdParameter());
       return $admin->getSubject();
   }
    
    public function prePersist($image)
    {
        parent::prePersist($image);
        
        $image->setKooshId($this->getKoosh()->getId()); 
    }
    
    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {

        // get the current Image instance
        $image = $this->getCurrentObjectFromCollection($this);       
        
        // use $fileFieldOptions so we can add other options to the field
        $imageFileFieldOptions = array('required' => false, 'data_class' => null);
        $fullPath = '';
        if ($image && ($imageFileWebPath = $image->getImageFileWebPath())) {
            // get the container so the full path to the image can be set
            $container = $this->getConfigurationPool()->getContainer();
            $fullPath = $container->get('request')->getBasePath().'/'.$imageFileWebPath;

            // add a 'help' option containing the preview's img tag
            $imageFileFieldOptions['help'] = '<img src="'.$fullPath.'" class="admin-preview" height="240" />';
        }
        $imageId = (is_object($image)) ? $image->getId() : null;

        $formMapper
            ->add('title')
            ->add('user')
        ;
        
        if(!empty($imageId)) {
            $formMapper
                    ->add('imagePreview', 'imagePreview', array('image_path' => $fullPath,  'button_name' => 'EDIT', 'required' => false, 'label' => 'Image'))
                    ->add('actionField', 'customButton', array('route'=>'admin_kapi_kooshapi_image_edit','params'=>array('id'=>$imageId), 'button_name' => 'EDIT', 'required' => false, 'label' => 'Action'));
        } else {
            $formMapper->add('imageFile', 'file', $imageFileFieldOptions);
        }
    }
        
    public function getCurrentObjectFromCollection($adminChild)
    {
        $getter = 'get' . $adminChild->getParentFieldDescription()
                                   ->getFieldName();
        $parent = $adminChild->getParentFieldDescription()
                       ->getAdmin()
                       ->getSubject();
        $collection = $parent->$getter();

        $session = $adminChild->getRequest()->getSession();
        $number = 0;
        if ($session->get('adminCollection')) {
            $number = $session->get('adminCollection');
            $session->remove('adminCollection');
        }
        else {
            $session->set('adminCollection', 3 - $number);
        }

        return $collection[$number];
    }
}