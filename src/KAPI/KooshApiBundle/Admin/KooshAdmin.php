<?php

namespace KAPI\KooshApiBundle\Admin;



use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Component\Routing\Exception\RouteNotFoundException;

use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

use Knp\Menu\ItemInterface as MenuItemInterface;

use KAPI\KooshApiBundle\Entity\KooshComment;

class KooshAdmin extends Admin
{
    
    
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('title')
            ->add('type', 'doctrine_orm_choice', array('label' => 'Type'), 'choice',
                                                                            array(
                                                                                'choices' =>  array(
                                                                                    'koosh_video' => 'Video Collection',
                                                                                    'koosh_kapture' => 'Image Collection'
                                                                                )
                                                                            )
                )
            ->add('user')
            ->add('status')
            ->add('publicStatus')
            ->add('created')
            ->add('updated')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('title')
            ->add('user')
            ->add('type')
            ->add('status')
            ->add('publicStatus')
            ->add('image', 'string', array('template' => 'KAPIKooshApiBundle:AdminTemplate:list_image.html.twig'))
            ->add('video', 'string', array('template' => 'KAPIKooshApiBundle:AdminTemplate:list_video.html.twig'))
            ->add('created')
            ->add('updated')
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        // get the current Image instance
        $user = $this->getSubject();

        // use $fileFieldOptions so we can add other options to the field
        $imageFileFieldOptions = array('required' => false, 'data_class' => null);
        if ($user && ($imageWebPath = $user->getImageWebPath())) {
            // get the container so the full path to the image can be set
            $container = $this->getConfigurationPool()->getContainer();
            $fullPath = $container->get('request')->getBasePath().'/'.$imageWebPath;

            // add a 'help' option containing the preview's img tag
            $imageFileFieldOptions['help'] = '<img src="'.$fullPath.'" class="admin-preview" height="240" />';
        }
        
        // use $fileFieldOptions so we can add other options to the field
        $videoFileFieldOptions = array('required' => false, 'data_class' => null);
        if ($user && ($videoFileWebPath = $user->getVideoFileWebPath())) {
            // get the container so the full path to the image can be set
            $container = $this->getConfigurationPool()->getContainer();
            $fullPath = $container->get('request')->getBasePath().'/'.$videoFileWebPath;

            // add a 'help' option containing the preview's img tag
            $videoFileFieldOptions['help'] = '<video width="320" height="240" src="'.$fullPath.'" controls>
                                                <source src="'.$fullPath.'" type="video/mp4">
                                                Your browser does not support the video tag.
                                            </video>';
        }
       
        
        $formMapper
            ->with('General')
                ->add('title')
                ->add('genre')
                ->add('user', null, array('required' => true))
                ->add('status', 'choice', array('choices' => array(
                    '0' => 'new', 
                    '1' => 'approved', 
                    '2' => 'finished',
                )))
                ->add('publicStatus', 'choice', array('choices' => array( 
                    'public' => 'public', 
                    'private' => 'private',
                )))
                ->add('type', 'choice', array('choices' => array(
                    'koosh_video' => 'Video Collection', 
                    'koosh_kapture' => 'Image Kapture',
                )))
                ->add('image', 'file', $imageFileFieldOptions)
                ->add('videoFile', 'file', $videoFileFieldOptions)
            ->end()
            ->with('Comments')
                ->add('comments',  'sonata_type_collection', array(
                                                                'required' => true,
                                                                'by_reference' => false,
                                                            ),
                                                            array(
                                                                'admin_code' => 'kapi_kooshApi.admin.kooshCommentChild',
                                                                'edit' => 'inline',
                                                                'inline' => 'table',
                                                                'allow_delete' => true,
                                                            ))
            ->end()
            ->with('Videos')
                ->add('videos',  'sonata_type_collection', array(
                                                                'required' => true,
                                                                'by_reference' => false,
                                                            ),
                                                            array(
                                                                'admin_code' => 'kapi_kooshApi.admin.videoChild',
                                                                'edit' => 'inline',
                                                                'inline' => 'table',
                                                                'allow_delete' => true,
                                                            ))
            ->with('Images')
                ->add('images',  'sonata_type_collection', array(
                                                                'required' => true,
                                                                'by_reference' => false,
                                                            ),
                                                            array(
                                                                'admin_code' => 'kapi_kooshApi.admin.imageChild',
                                                                'edit' => 'inline',
                                                                'inline' => 'table',
                                                                'allow_delete' => true,
                                                            ))
            ->end()
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('title')
            ->add('user')
            ->add('type')
            ->add('status')
            ->add('publicStatus')
            ->add('created')
            ->add('updated')
        ;
    }
    
    
    public function prePersist($koosh)
    {
        $koosh->setCreated(new \DateTime());
        $koosh->setUpdated($koosh->getCreated());
        
        if(is_object($koosh->getUser())) {
            $koosh->setUserId($koosh->getUser()->getId());
        } 
    }
    
    public function preUpdate($user)
    {
        $user->setUpdated(new \DateTime());
        
    }
    
    public function postPersist($user)
    {   
        //$this->update($user);
    }
    
    public function setVideoManager(VideoManagerInterface $videoManager)
    {
        $this->videoManager = $videoManager;
    }

    /**
     * @return VideoManagerInterface
     */
    public function getVideoManager()
    {
        return $this->videoManager;
    }
    
    
    // In your Admin class

    public function getBatchActions()
    {
        // retrieve the default batch actions (currently only delete)
        $actions = parent::getBatchActions();

        if (
          $this->hasRoute('edit') && $this->isGranted('EDIT') &&
          $this->hasRoute('delete') && $this->isGranted('DELETE')
        ) {
            $actions['composeVideo'] = array(
                'label' => $this->trans('action_video_composition', array(), 'SonataAdminBundle'),
                'ask_confirmation' => true
            );

        }

        return $actions;
    }
    
    /*
    protected function configureSideMenu(MenuItemInterface  $menu, $action, AdminInterface $childAdmin = null) {
        
        if (!$childAdmin && !in_array($action, array('edit'))) {
            return;
        }
        
        $admin = $this->isChild() ? $this->getParent() : $this;
        $id = $admin->getRequest()->get('id');

        $menu->addChild(
                'Logout', array('uri' => $admin->generateUrl('logout'))
        );
    }
    */
}
