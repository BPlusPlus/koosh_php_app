<?php
/**
 * https://github.com/liuggio/symfony2-rest-api-the-best-2013-way/tree/master/app
 * https://github.com/nmpolo/Symfony2Rest/blob/master/src/Nmpolo/RestBundle/Controller/UserController.php
 */

namespace KAPI\KooshApiBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\Controller\Annotations\NamePrefix;
use FOS\RestBundle\Controller\Annotations\Prefix;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Patch;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Request\ParamFetcherInterface;

use Symfony\Component\Form\FormTypeInterface;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use KAPI\KooshApi\CalculatorBundle\Exception\InvalidFormException;
use KAPI\KooshApiBundle\Form\PageType;
use KAPI\KooshApiBundle\Model\ClientInterface;


/**
 * @NamePrefix("api_rest_")
 * @Prefix("/api")
 * @RouteResource("koosh")
 * @View
 */
class KooshApiController extends FOSRestController
{

/**
* Gets a list of incomplete kooshes..
*
* @ApiDoc(
* resource = true,
* description = "Gets a list of incomplete kooshes..",
* output = "preferred rate",
* statusCodes = {
* 200 = "Returned when successful",
* 404 = "Returned when the page is not found"
* }
* )
*
* @param \Symfony\Component\HttpFoundation\Request $request
* @param int $userId
* @return mixed
* @Get("/koosh/{userId}")
* 
* @View( serializerGroups={"preferredRate"} )
*/
public function getIncompleteKoosh($userId)
{
    $em = $this->getDoctrine()->getManager();
    $entities = $em->getRepository('KAPIKooshApiBundle:Koosh')->findBy(array('userId' => $userId));

    $incompleteKooshes = array();
    if(sizeof($entities)) {
        foreach($entities AS $key => $koosh) {
            /*
             *      {incomplete_kooshes:[{
                    koosh_title:-Koosh title-,
                    people_invovled:-number of people involved-,
                    image_url:-url of freezeframe image-,
                    koosh_id:-id of the koosh-
                    people_waiting:[{
                    user_name:-user name-,
                    user_id:-user ID-,
                    nudged:-Bool of if user has been nudged yet (see below)-,
                    },
                    },etc]
                    }
                    (people waiting is the list of people who have yet to reply.)
             */
            
            // get waiting people
            $peopleWaiting = array();
            
            
            $incompleteKooshes[$key]['koosh_title'] = $koosh->__get('title');
            $incompleteKooshes[$key]['people_invovled'] = $koosh->__get('title');
            $incompleteKooshes[$key]['image_url'] = $koosh->__get('title');
            $incompleteKooshes[$key]['people_waiting'] = $peopleWaiting;
        }
    }
    
    if (!sizeof($entities)) {
        return array(
            'status' => 'error', 
            'message' => 'Unable to find any incomplete kooshes for user_id: `' . $userId . '`'
        );
    } else {
        return array(
            'status' => 'ok', 
            //'data' => $entity
            'data' => $incompleteKooshes
        );
    }
}
    
/**
* Gets a list of incomplete kooshes..
*
* @ApiDoc(
* resource = true,
* description = "Gets a list of incomplete kooshes..",
* output = "preferred rate",
* statusCodes = {
* 200 = "Returned when successful",
* 404 = "Returned when the page is not found"
* }
* )
*
* @param \Symfony\Component\HttpFoundation\Request $request
* @param int $userId
* @return mixed
* @Get("/koosh/{userId}")
* 
* @View( serializerGroups={"preferredRate"} )
*/
public function nudgeUser($userId)
{
    $em = $this->getDoctrine()->getManager();
    $entities = $em->getRepository('KAPIKooshApiBundle:Koosh')->findBy(array('userId' => $userId));

    if (!sizeof($entities)) {
        return array(
            'status' => 'error', 
            'message' => 'Unable to find any incomplete kooshes for user_id: `' . $userId . '`'
        );
    } else {
        return array(
            'status' => 'ok', 
            //'data' => $entity
            'data' => $entities
        );
    }
}

}