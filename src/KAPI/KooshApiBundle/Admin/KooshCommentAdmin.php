<?php

namespace KAPI\KooshApiBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Component\Routing\Exception\RouteNotFoundException;

use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

use Knp\Menu\ItemInterface as MenuItemInterface;

use KAPI\KooshApiBundleBundle\Entity\KooshComment;

class KooshCommentAdmin extends KooshCommentAdminBase 
{
    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        parent::configureFormFields($formMapper);

        $formMapper->add('koosh', 'sonata_type_model', array(), array('edit' => 'standard'));
    }
    
    public function prePersist($kooshComment) 
    {
        parent::prePersist($kooshComment);
        
        if(is_object($kooshComment->getKoosh())) {
            $kooshComment->setKooshId($kooshComment->getKoosh()->getId());
        }
    }
}
