<?php

namespace KAPI\KooshApiBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Component\Routing\Exception\RouteNotFoundException;

use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

use Knp\Menu\ItemInterface as MenuItemInterface;

use KAPI\KooshApiBundleBundle\Entity\KooshComment;

class KooshCommentAdminBase extends Admin
{
    
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('user')
            ->add('text')
            ->add('created')
            ->add('updated')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('koosh')
            ->add('user')
            ->add('text')
            ->add('created')
            ->add('updated')
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {    
        $formMapper
            ->add('user', null, array('required' => true))
            ->add('text')
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('user')
            ->add('text')
            ->add('created')
            ->add('updated')
        ;
    }
    
    /**
    * @return CommentableInterface
    */
   protected function getParentObject()
   {
       return $this->getParent()->getObject($this->getParent()->getRequest()->get('id'));
   }
   
   /**
    * @return CommentableInterface
    */
   protected function getParentObjectId()
   {
       $admin = $this->isChild() ? $this->getParent() : $this;
       $id = $admin->getRequest()->get('puniqid');
       
       return $id;
   }
    
    public function prePersist($kooshComment)
    {
        //$kooshComment->setCreated(new \DateTime());
        //$kooshComment->setUpdated($kooshComment->getCreated());
        
        if(is_object($kooshComment->getUser())) {
            $kooshComment->setUserId($kooshComment->getUser()->getId());
        } 
        
        //$kooshComment->setKooshId($this->getParentObjectId()); 
    }
    
    public function preUpdate($user)
    {
        $user->setUpdated(new \DateTime());
        
    }
    
    public function postPersist($user)
    {   
        //$this->update($user);
    }
    
    public function setVideoManager(VideoManagerInterface $videoManager)
    {
        $this->videoManager = $videoManager;
    }

    /**
     * @return VideoManagerInterface
     */
    public function getVideoManager()
    {
        return $this->videoManager;
    }
    
    
}
