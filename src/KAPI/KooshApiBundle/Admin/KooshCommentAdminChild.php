<?php

namespace KAPI\KooshApiBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Component\Routing\Exception\RouteNotFoundException;

use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

use Knp\Menu\ItemInterface as MenuItemInterface;

use KAPI\KooshApiBundleBundle\Entity\KooshComment;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;


class KooshCommentAdminChild extends KooshCommentAdminBase 
{
    protected $parentAssociationMapping = 'koosh';
    
    
    public function getParentAssociationMapping()
    {
        return 'koosh';
    }
    
    /**
    * @return Koosh
    */
   protected function getKoosh()
   {
       $admin = $this->getConfigurationPool()->getContainer()->get('kapi_kooshApi.admin.koosh');
       $admin->setRequest($this->getRequest());
       $admin->getObject($admin->getIdParameter());
       return $admin->getSubject();
   }
    
    public function prePersist($kooshComment)
    {
        parent::prePersist($kooshComment);
        
        $kooshComment->setKooshId($this->getKoosh()->getId()); 
    }
    
        
}
