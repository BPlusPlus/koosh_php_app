<?php

namespace KAPI\KooshApiBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Component\Routing\Exception\RouteNotFoundException;

use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

use Knp\Menu\ItemInterface as MenuItemInterface;

use KAPI\KooshApiBundleBundle\Entity\PrivateMessage;

class PrivateMessageAdmin extends KooshCommentAdminBase 
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('userFrom')
            ->add('userTo')
            ->add('message')
            ->add('created')
            ->add('updated')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('userFrom')
            ->add('userTo')
            ->add('message')
            ->add('created')
            ->add('updated')
            ->add('_action', 'actions', array(
                'actions' => array(
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {    
        $userFrom = $this->getSubject();
        
        $formMapper
            ->add('userFrom', null, array('required' => true))
            ->add('userTo', null, array('required' => true))
            ->add('message', null, array('required' => true))
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('userFrom')
            ->add('userTo')
            ->add('message')
            ->add('created')
            ->add('updated')
        ;
    }
    
    
    public function prePersist($privateMessage)
    {
        if(is_object($privateMessage->getUserFrom())) {
            $privateMessage->setUserIdFrom($privateMessage->getUserFrom()->getId());
        } 
        
         if(is_object($privateMessage->getUserTo())) {
            $privateMessage->setUserIdTo($privateMessage->getUserTo()->getId());
        } 
        
        //$kooshComment->setKooshId($this->getParentObjectId()); 
    }
    
    public function preUpdate($privateMessage)
    {
        $privateMessage->setUpdated(new \DateTime());
        
    }
    
    public function postPersist($privateMessage)
    {   
        //$this->update($user);
    }
    
}
