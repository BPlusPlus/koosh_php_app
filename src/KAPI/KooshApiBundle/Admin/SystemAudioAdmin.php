<?php

namespace KAPI\KooshApiBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Component\Routing\Exception\RouteNotFoundException;

use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

use Knp\Menu\ItemInterface as MenuItemInterface;

use KAPI\KooshApiBundleBundle\Entity\SystemAudio;

class SystemAudioAdmin extends Admin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('title')
            ->add('status')
            ->add('created')
            ->add('updated')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('title')
            ->add('status')
            ->add('audio', 'string', array('template' => 'KAPIKooshApiBundle:AdminTemplate:list_audio.html.twig'))
            ->add('created')
            ->add('updated')
            ->add('_action', 'actions', array(
                'actions' => array(
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {    
        // get the current Image instance
        $audio = $this->getSubject();

        // use $fileFieldOptions so we can add other options to the field
        $audioFileFieldOptions = array('required' => false, 'data_class' => null);
        if ($audio && ($audioFileWebPath = $audio->getAudioFileWebPath())) {
            // get the container so the full path to the image can be set
            $container = $this->getConfigurationPool()->getContainer();
            $fullPath = $container->get('request')->getBasePath().'/'.$audioFileWebPath;

            // add a 'help' option containing the preview's img tag
            $audioFileFieldOptions['help'] = '<audio controls>
                                                <source src="'.$fullPath.'" type="audio/mp3">
                                                Your browser does not support the audio tag.
                                            </audio>';
        }
        
        
        $formMapper
            ->add('title', null, array('required' => true))
            ->add('status', 'choice', array('choices' => array(
                                                            '1' => 'Enabled',
                                                            '0' => 'Disabled' 
                                                        ))) 
            ->add('audioFile', 'file', $audioFileFieldOptions)
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('title')
            ->add('status')
            ->add('created')
            ->add('updated')
        ;
    }
    
    
    public function prePersist($systemAudio)
    {
        
    }
    
    public function preUpdate($systemAudio)
    {
        $systemAudio->setUpdated(new \DateTime());
        
    }
    
    public function postPersist($systemAudio)
    {   
        //$this->update($user);
    }
    
}
