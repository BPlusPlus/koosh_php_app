<?php

namespace KAPI\KooshApiBundle\Admin;



use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Component\Routing\Exception\RouteNotFoundException;

use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

use Sonata\AdminBundle\Validator\ErrorElement;

use Knp\Menu\ItemInterface as MenuItemInterface;

class UserAdmin extends Admin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('username')
            ->add('firstName')
            ->add('lastName')
            ->add('email')
            ->add('dob')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('username')
            ->add('email')
            ->add('image', 'string', array('template' => 'KAPIKooshApiBundle:AdminTemplate:list_user_image.html.twig'))
            ->add('created')
            ->add('updated')
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        // get the current Image instance
        $user = $this->getSubject();

        // use $fileFieldOptions so we can add other options to the field
        $fileFieldOptions = array('required' => false, 'data_class' => null);
        if ($user && ($webPath = $user->getWebPath())) {
            // get the container so the full path to the image can be set
            $container = $this->getConfigurationPool()->getContainer();
            $fullPath = $container->get('request')->getBasePath().'/'.$webPath;

            // add a 'help' option containing the preview's img tag
            $fileFieldOptions['help'] = '<img src="'.$fullPath.'" class="admin-preview" />';
        }
        
        $formMapper
            ->with('General')
                ->add('username')
                ->add('password', 'password')
                ->add('firstName')
                ->add('lastName')
                ->add('email')
                ->add('dob', 'birthday', array('years' => range(1960, date('Y')), 'format' => 'dd-MMMM-yyyy'))
                ->add('image', 'file', $fileFieldOptions)
            ->end() 
            ->with('Videos')
                ->add('videos',  'sonata_type_collection', array(
                                                                'required' => true,
                                                                'by_reference' => false,
                                                            ),
                                                            array(
                                                                'admin_code' => 'kapi_kooshApi.admin.videoChild',
                                                                'edit' => 'inline',
                                                                'inline' => 'table',
                                                                'allow_delete' => true,
                                                            ))
            ->end()            
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('username')
            ->add('firstName')
            ->add('lastName')
            ->add('email')
            ->add('dob')
            ->add('created')
            ->add('updated')
        ;
    }
    
    private function manageFileUpload($user) {
        if ($user->getImage()) {
            $user->refreshUpdated();
        }
    }
    
    
    public function postPersist($user)
    {   
        //$this->update($user);
    }
    
    public function setUserManager(UserManagerInterface $userManager)
    {
        $this->userManager = $userManager;
    }

    /**
     * @return UserManagerInterface
     */
    public function getUserManager()
    {
        return $this->userManager;
    }
    
    public function validate(ErrorElement $errorElement, $object)
    {
        
        // find object with the same uniqueField-value
        $username = $this->modelManager->findOneBy($this->getClass(), array('username' => $object->getUsername()));
 
        if(is_object($username)) {
            $errorElement
                ->with('username')
                ->addViolation('The unique field must be unique!')
                ->end();
        }
        
        // find object with the same uniqueField-value
        $email = $this->modelManager->findOneBy($this->getClass(), array('email' => $object->getEmail()));
 
        if(is_object($email)) {
            $errorElement
                ->with('email')
                ->addViolation('The unique field must be unique!')
                ->end();
        }
    }
    
}
