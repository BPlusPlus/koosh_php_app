<?php

namespace KAPI\KooshApiBundle\Admin;



use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Component\Routing\Exception\RouteNotFoundException;

use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

use Knp\Menu\ItemInterface as MenuItemInterface;

class VideoAdmin extends VideoAdminBase
{
    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        parent::configureFormFields($formMapper);

        $formMapper->add('koosh', 'sonata_type_model', array(), array('edit' => 'standard'));
    }
    
    public function prePersist($video) 
    {
        parent::prePersist($video);
        
        if(is_object($video->getKoosh())) {
            $video->setKooshId($video->getKoosh()->getId());
        }
    }
}
