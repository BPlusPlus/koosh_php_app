<?php

namespace KAPI\KooshApiBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Component\Routing\Exception\RouteNotFoundException;

use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

use Knp\Menu\ItemInterface as MenuItemInterface;

use KAPI\KooshApiBundleBundle\Entity\KooshComment;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;

use KAPI\KooshApiBundle\Form\Type\CustomButtonType;
use KAPI\KooshApiBundle\Form\Type\VideoPreviewType;

class VideoAdminChild extends VideoAdminBase 
{
    protected $parentAssociationMapping = 'koosh';
    
    
    public function getParentAssociationMapping()
    {
        return 'koosh';
    }
    
    /**
    * @return Koosh
    */
   protected function getKoosh()
   {
       $admin = $this->getConfigurationPool()->getContainer()->get('kapi_kooshApi.admin.koosh');
       $admin->setRequest($this->getRequest());
       $admin->getObject($admin->getIdParameter());
       return $admin->getSubject();
   }
    
    public function prePersist($video)
    {
        parent::prePersist($video);
        
        $video->setKooshId($this->getKoosh()->getId()); 
    }
    
    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {

        // get the current Image instance
        $video = $this->getCurrentObjectFromCollection($this);

        // use $fileFieldOptions so we can add other options to the field
        $videoFileFieldOptions = array('required' => false, 'data_class' => null);
        $fullPath = '';
        if ($video && ($videoFileWebPath = $video->getVideoFileWebPath())) {
            // get the container so the full path to the image can be set
            $container = $this->getConfigurationPool()->getContainer();
            $fullPath = $container->get('request')->getBasePath().'/'.$videoFileWebPath;

            // add a 'help' option containing the preview's img tag
            $videoFileFieldOptions['help'] = '<video width="240" height="180" src="'.$fullPath.'" controls>
                                                <source src="'.$fullPath.'" type="video/mp4">
                                                Your browser does not support the video tag.
                                            </video>';
        }
        $videoId = (is_object($video)) ? $video->getId() : null;

        $formMapper
            ->add('title')
            ->add('user')
        ;
        
        if(!empty($videoId)) {
            $formMapper
                    ->add('videoPreview', 'videoPreview', array('video_path' => $fullPath,  'button_name' => 'EDIT', 'required' => false, 'label' => 'Video'))
                    ->add('actionField', 'customButton', array('route'=>'admin_kapi_kooshapi_video_edit','params'=>array('id'=>$videoId), 'button_name' => 'EDIT', 'required' => false, 'label' => 'Action'));
        } else {
            $formMapper->add('videoFile', 'file', $videoFileFieldOptions);
        }
    }
        
    public function getCurrentObjectFromCollection($adminChild)
    {
        $getter = 'get' . $adminChild->getParentFieldDescription()
                                   ->getFieldName();
        $parent = $adminChild->getParentFieldDescription()
                       ->getAdmin()
                       ->getSubject();
        $collection = $parent->$getter();

        $session = $adminChild->getRequest()->getSession();
        $number = 0;
        if ($session->get('adminCollection')) {
            $number = $session->get('adminCollection');
            $session->remove('adminCollection');
        }
        else {
            $session->set('adminCollection', 1 - $number);
        }

        return $collection[$number];
    }
}