<?php 
namespace KAPI\KooshApiBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use KAPI\KooshApiBundle\Entity\User;
use KAPI\KooshApiBundle\Entity\KooshLike;
use KAPI\KooshApiBundle\Entity\KooshComment;
use KAPI\KooshApiBundle\Entity\Koosh;

class CalculateFameValueCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('kapi:calculate_fame_value')
            ->setDescription('Calculate Fame Value')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();
        
        // Go through all users
        $users = $em->getRepository('KAPIKooshApiBundle:User')->findAll();
        foreach($users AS $user) {
            // Check total user Koosh Likes
            $query = $em->createQuery(
                'SELECT COUNT(kl.id) AS total 
                FROM KAPIKooshApiBundle:KooshLike kl
                JOIN kl.koosh k 
                WHERE k.userId = :userId'
            )->setParameter('userId', $user->getId());
            $kooshLikes = $query->setMaxResults(1)->getOneOrNullResult();
            $userKooshLikesTotal = $kooshLikes['total'];
            
            // Check total user Koosh Comments
            $query = $em->createQuery(
                'SELECT COUNT(kc.id) AS total 
                FROM KAPIKooshApiBundle:KooshComment kc
                JOIN kc.koosh k 
                WHERE k.userId = :userId'
            )->setParameter('userId', $user->getId());
            $kooshComments = $query->setMaxResults(1)->getOneOrNullResult();
            $userKooshCommentsTotal = $kooshComments['total'];
            
            // Check total user friends
            $userFriendsTotal = sizeof($em->getRepository('KAPIKooshApiBundle:Friendship')->getUserFriends($user->getId()));
            
            
            // Calculate Fame Value
            $fameValue = 0;
            
            // 3 stars for friends
            if($userFriendsTotal >= 400) $fameValue += 3;
            elseif($userFriendsTotal >= 200) $fameValue += 2;
            elseif($userFriendsTotal >= 100) $fameValue += 1;
            
            // 4 stars for likes
            if($userKooshLikesTotal >= 800) $fameValue += 4;
            elseif($userKooshLikesTotal >= 400) $fameValue += 3;
            elseif($userKooshLikesTotal >= 200) $fameValue += 2;
            elseif($userKooshLikesTotal >= 100) $fameValue += 1;
            
            // 3 stars for comments
            if($userKooshCommentsTotal >= 400) $fameValue += 3;
            elseif($userKooshCommentsTotal >= 200) $fameValue += 2;
            elseif($userKooshCommentsTotal >= 100) $fameValue += 1;
            
            $user->setFameValue($fameValue);
            $user->setImage(null);
            $em->persist($user);
            $em->flush();
        }
        
    }
}