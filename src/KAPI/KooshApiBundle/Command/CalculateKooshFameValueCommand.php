<?php 
namespace KAPI\KooshApiBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use KAPI\KooshApiBundle\Entity\User;
use KAPI\KooshApiBundle\Entity\KooshLike;
use KAPI\KooshApiBundle\Entity\KooshComment;
use KAPI\KooshApiBundle\Entity\Koosh;

class CalculateKooshFameValueCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('kapi:calculate_koosh_fame_value')
            ->setDescription('Calculate Koosh Fame Value')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();
        
        // Permanent array
        $permanentArray = array();
        
        // Go through all kooshes
        $kooshes = $em->getRepository('KAPIKooshApiBundle:Koosh')->findAll();
        foreach($kooshes AS $koosh) {
            // Check total user Koosh Likes
            $query = $em->createQuery(
                'SELECT COUNT(kl.id) AS total 
                FROM KAPIKooshApiBundle:KooshLike kl
                WHERE kl.kooshId = :kooshId'
            )->setParameter('kooshId', $koosh->getId());
            $kooshLikes = $query->setMaxResults(1)->getOneOrNullResult();
            $kooshKooshLikesTotal = $kooshLikes['total'];
            
            // Check total user Koosh Comments
            $query = $em->createQuery(
                'SELECT COUNT(kc.id) AS total 
                FROM KAPIKooshApiBundle:KooshComment kc
                WHERE kc.kooshId = :kooshId'
            )->setParameter('kooshId', $koosh->getId());
            $kooshComments = $query->setMaxResults(1)->getOneOrNullResult();
            $kooshKooshCommentsTotal = $kooshComments['total'];
            
            
            // Store to permanent array
            $kooshRating = $kooshKooshLikesTotal + $kooshKooshCommentsTotal;
            $permanentArray[$koosh->getId()] = $kooshRating;
            
        }
        
        // Sort array
        asort($permanentArray);

        // Update fame value
        $index = 1;
        foreach($permanentArray AS $kooshId => $rating) {
            $koosh = $em->getRepository('KAPIKooshApiBundle:Koosh')->findOneBy(array('id' => $kooshId));
            if(!empty($rating)) {
                $fameValue = ceil(($index / (sizeof($permanentArray) / 100)) / 10);
            } else {
                $fameValue = 0;
            }

            // Store calculated fame value
            $koosh->setFameValue($fameValue);
            $koosh->setImage(null);
            $koosh->setAudioFile(null);
            $koosh->setVideoFile(null);
            $em->persist($koosh);
            $em->flush();
            
            $index++;
        }
        
        
        //$output->writeln($text);
    }
}