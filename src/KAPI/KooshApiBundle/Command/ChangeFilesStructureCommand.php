<?php 
namespace KAPI\KooshApiBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use KAPI\KooshApiBundle\Entity\User;
use KAPI\KooshApiBundle\Entity\KooshLike;
use KAPI\KooshApiBundle\Entity\KooshComment;
use KAPI\KooshApiBundle\Entity\Koosh;
use KAPI\KooshApiBundle\Entity\Video;
use KAPI\KooshApiBundle\Entity\Image;

class ChangeFilesStructureCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('kapi:change_files_structure')
            ->setDescription('Change Files Structure')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();
        
        // Go through all users
        $kooshes = $em->getRepository('KAPIKooshApiBundle:Koosh')->findAll();
        foreach($kooshes AS $koosh) {
            /*
            // create specific folder
            $uniqueHash = md5(uniqid(rand()));
            $koosh->setUniqueHash($uniqueHash);
            $koosh->setVideoFile(null);
            $koosh->setImage(null);
            $koosh->setAudioFile(null);
            $em->persist($koosh);
            $em->flush();
            
            $hashPath = $koosh->getAbsolutePath() . $koosh->getCreated()->format('n_Y') . '/' . $uniqueHash . '/';
            if (!file_exists($hashPath)) {
                mkdir($hashPath, 0755, true);
            }
            
            // copy main file
            if(is_file($koosh->getVideoFileAbsolutePath())) {
                copy($koosh->getVideoFileAbsolutePath(), $hashPath.$koosh->getVideoFilePath());
            }
            
            // copy lq file
            if(is_file($koosh->getVideoFileAbsolutePathByQuality(Koosh::QUALITY_2))) {
                copy($koosh->getVideoFileAbsolutePathByQuality(Koosh::QUALITY_2), $hashPath.$koosh->getVideoFilePath2());
            }
            
            // copy image file
            if(is_file($koosh->getImageAbsolutePath())) {
                copy($koosh->getImageAbsolutePath(), $hashPath.$koosh->getImagePath());
            }
            
            // copy audio file
            if(is_file($koosh->getAudioFileAbsolutePath())) {
                copy($koosh->getAudioFileAbsolutePath(), $hashPath.$koosh->getAudioFilePath());
            }
            
            // copy images
            $images = $em->getRepository('KAPIKooshApiBundle:Image')->findBy(array('kooshId' => $koosh->getId()));
            if(sizeof($images)) {
                if (!file_exists($hashPath.'images')) {
                    mkdir($hashPath.'images', 0755, true);
                }
                
                foreach($images AS $image) {
                    // copy image file
                    if(is_file($image->getImageFileAbsolutePath())) {
                        copy($image->getImageFileAbsolutePath(), $hashPath.'images/'.$image->getImageFilePath());
                    }
                }
            }
            */
            // copy videos
            $videos = $em->getRepository('KAPIKooshApiBundle:Video')->findBy(array('kooshId' => $koosh->getId()));
            if(sizeof($videos)) {
                $hashPath = $koosh->getAbsolutePath();
                if (!file_exists($hashPath.'videos')) {
                    mkdir($hashPath.'videos', 0755, true);
                }
                
                foreach($videos AS $video) {
                    // create specific folder
                    $uniqueHash = md5(uniqid(rand()));
                    $video->setUniqueHash($uniqueHash);
                    $video->setVideoFile(null);
                    $video->setImageFile(null);
                    $video->setAudioFile(null);
                    $em->persist($video);
                    $em->flush();

                    if (file_exists($hashPath.'videos/'.$video->getId())) {
                        rename($hashPath.'videos/'.$video->getId(),$hashPath.'videos/'.$video->getUniqueHash());
                    }

                    /*
                    if (!file_exists($hashPath.'videos/'.$uniqueHash)) {
                        mkdir($hashPath.'videos/'.$uniqueHash, 0755, true);
                    }
                    
                    // copy video file
                    if(is_file($video->getVideoFileAbsolutePath())) {
                        copy($video->getVideoFileAbsolutePath(), $hashPath.'videos/'.$uniqueHash.'/'.$video->getVideoFilePath());
                    }
                    
                    // copy image file
                    if(is_file($video->getImageFileAbsolutePath())) {
                        copy($video->getImageFileAbsolutePath(), $hashPath.'videos/'.$uniqueHash.'/'.$video->getImageFilePath());
                    }
                    
                    // copy audio file
                    if(is_file($video->getAudioFileAbsolutePath())) {
                        copy($video->getAudioFileAbsolutePath(), $hashPath.'videos/'.$uniqueHash.'/'.$video->getAudioFilePath());
                    }
                    
                    // copy original video file
                    if(is_file($video->getVideoFileAbsolutePath().'original.mp4')) {
                        copy($video->getVideoFileAbsolutePath().'original.mp4', $hashPath.'videos/'.$uniqueHash.'/'.$video->getVideoFilePath().'original.mp4');
                    }
                    */
                }
            }

        }
        
    }
}