<?php

namespace KAPI\KooshApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        //return $this->render('KAPIKooshApiBundle:Default:index.html.twig', array('name' => $name));
        return $this->redirect($this->generateUrl('sonata_admin_redirect'));
    }
}
