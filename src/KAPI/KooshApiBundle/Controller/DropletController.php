<?php

namespace KAPI\KooshApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use KAPI\KooshApiBundle\Entity\Koosh;

class DropletController extends Controller
{
    private $logger;

    public function indexAction()
    {

    }

    public function finishedAction(Request $request, $kooshId)
    {

        $this->logger = $this->container->get('rabbitmq.logger');
        
        $em = $this->getDoctrine()->getManager();
        $koosh = $em->getRepository('KAPIKooshApiBundle:Koosh')->findOneBy(array('id' => $kooshId));

        if(is_object($koosh)) {

            // copy freeze image from helpers folder
            $imagePath = $koosh->getImageWebPath();
            if(!empty($imagePath)) {
                $this->copyFileFromRemoteServer($koosh->getImageWebPath(), $koosh->getImageAbsolutePath(), $koosh);
            }

            $videoPath = $koosh->getVideoFileWebPath();
            if(!empty($videoPath)) {
                $this->copyFileFromRemoteServer($koosh->getVideoFileWebPath(), $koosh->getVideoFileAbsolutePath(), $koosh);
            }

            /*
            $videoLQPath = $koosh->getVideoFileWebPath(Koosh::QUALITY_2);
            if(!empty($videoLQPath)) {
                $this->copyFileFromRemoteServer($koosh->getVideoFileWebPath(Koosh::QUALITY_2), $koosh->getVideoFileAbsolutePathByQuality(Koosh::QUALITY_2), $koosh);
            }
            */
        }

        return $this->render('KAPIKooshApiBundle:Droplet:finished.html.twig');
    }


    protected function copyFileFromRemoteServer($sourcePath, $targetPath, $koosh) {
        try {
            $dropletServerWebPath = $koosh->getDropletUrl();
            $this->logger->addInfo('COPY file from DROPLET server:' . $dropletServerWebPath . $sourcePath);

            // Create a curl handle to a non-existing location
            $ch = curl_init($dropletServerWebPath . $sourcePath);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $remoteFile = '';
            if( ($remoteFile = curl_exec($ch) ) === false)
            {
                $this->logger->addError('COPY file from DROPLET: ' . curl_error($ch));
            }
            else
            {
                file_put_contents($targetPath, $remoteFile);
            }

            // Close handle
            curl_close($ch);

            //$remoteFile = file_get_contents($mainServerWebPath . $sourcePath);

        } catch (Exception $e) {
            $this->logger->addError('COPY file from DROPLET: ' . $e->getMessage());
            //$this->markAsUnfinished($kooshId, $e->getMessage());
        }
    }
}
