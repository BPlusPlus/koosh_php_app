<?php

namespace KAPI\KooshApiBundle\Controller;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Sonata\AdminBundle\Controller\CRUDController as Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;

use FFMpeg\Coordinate\Dimension;
use FFMpeg\Filters\Video\ResizeFilter;
use FFMpeg\Format\Video\X264;
use FFMpeg\Coordinate\FrameRate;

class KooshAdminController extends Controller
{
    public function batchActionComposeVideo($selectedModelQuery) {
        
        $request = $this->get('request');
        $modelManager = $this->admin->getModelManager();

        $selectedModels = $selectedModelQuery->execute();

        try {
            foreach ($selectedModels as $selectedModel) {
                
                $items = array();
                if($selectedModel->getType() == 'koosh_kapture') {
                    // get all koosh images
                    $kooshImages = $selectedModel->getImages();
                    if(sizeof($kooshImages)) {
                        foreach($kooshImages AS $key => $image) {
                            $items[$key] = array(
                                'unique_id' => $image->getId(), 
                                'caption' => $image->getCaption()
                            );
                        }
                    }
                } else {
                    // get all koosh videos
                    $kooshVideos = $selectedModel->getVideos();
                    if(sizeof($kooshVideos)) {
                        foreach($kooshVideos AS $key => $video) {
                            $items[$key] = array(
                                'unique_id' => $video->getId(), 
                                'muted_bool' => $video->getMuted(), 
                                'caption' => $video->getCaption()
                            );
                        }
                    }
                }
                
                // RABBITMQ message
                $this->container->get('add_video_task')->process(array('kooshId' => $selectedModel->getId(), 'items' => $items, 'fadeType' => 'snap')); 
                
                
                 
                
                
            }
            
            $this->addFlash('sonata_flash_info', 'flash_batch_video_composition_successfull');
            
        } catch (\Exception $e) {
            $this->addFlash('sonata_flash_info', 'flash_batch_video_composition_error');
            $this->addFlash('sonata_flash_info', $e->getMessage());
        }  

        return new RedirectResponse(
          $this->admin->generateUrl('list',$this->admin->getFilterParameters())
        );
    }
}
