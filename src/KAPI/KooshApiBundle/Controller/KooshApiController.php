<?php
/**
 * https://github.com/liuggio/symfony2-rest-api-the-best-2013-way/tree/master/app
 * https://github.com/nmpolo/Symfony2Rest/blob/master/src/Nmpolo/RestBundle/Controller/UserController.php
 */

namespace KAPI\KooshApiBundle\Controller;

use FOS\RestBundle\Controller\Annotations\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\Controller\Annotations\NamePrefix;
use FOS\RestBundle\Controller\Annotations\Prefix;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Patch;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Request\ParamFetcherInterface;

use Symfony\Component\Form\FormTypeInterface;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use KAPI\KooshApiBundle\Exception\InvalidFormException;
use KAPI\KooshApiBundle\Form\PageType;
use KAPI\KooshApiBundle\Model\ClientInterface;

use KAPI\KooshApiBundle\Entity\Notification;
use KAPI\KooshApiBundle\Entity\KooshLike;
use KAPI\KooshApiBundle\Entity\Koosh;
use KAPI\KooshApiBundle\Entity\Video;
use KAPI\KooshApiBundle\Entity\Image;
use KAPI\KooshApiBundle\Entity\KooshItem;
use KAPI\KooshApiBundle\Entity\KooshComment;
use KAPI\KooshApiBundle\Entity\KooshCommentTag;

use RMS\PushNotificationsBundle\Message\iOSMessage;
use RMS\PushNotificationsBundle\Message\AndroidMessage;

/**
 * @NamePrefix("api_rest_")
 * @Prefix("/api")
 * @RouteResource("kooshApi")
 * @View
 */
class KooshApiController extends FOSRestController
{

/**
* Gets a list of incomplete kooshes.
*
* @ApiDoc(
* resource = true,
* description = "Gets a list of incomplete kooshes.",
* output = "preferred rate",
* statusCodes = {
* 200 = "Returned when successful",
* 404 = "Returned when the page is not found"
* }
* )
*
* @param \Symfony\Component\HttpFoundation\Request $request
* @param int $userId
* @return mixed
* @Get("/get_incomplete_koosh/{userId}")
* 
* @View
*/
public function getIncompleteKooshAction($userId)
{
    $baseUrl = $this->getRequest()->getSchemeAndHttpHost() . $this->container->getParameter('web_url_prefix');
    
    $em = $this->getDoctrine()->getManager();
    $entity = $em->getRepository('KAPIKooshApiBundle:Koosh')->findAllIncomplete($userId);

    $incompleteKooshes = array();
    foreach($entity AS $keyK => $koosh) {
        // get people involved
        $users = $em->getRepository('KAPIKooshApiBundle:User')->findAllInvolvedByKoosh($koosh->getId(), $koosh->getType());
        $involvedPeople = sizeof($users);

        $peopleWaiting = array();
        $inviteNotifications = $em->getRepository('KAPIKooshApiBundle:Notification')->findBy(array('kooshId' => $koosh->getId(), 'type' => 'koosh_invite'));
        if(sizeof($inviteNotifications)) {
            foreach($inviteNotifications AS $key => $notification) {
                $peopleWaiting[$key] = array(
                    'user_name' => (is_object($notification->getUserTo()) ? $notification->getUserTo()->getUsername() : 'unknown'), 
                    'user_id' => (is_object($notification->getUserTo()) ? $notification->getUserTo()->getId() : 'unknown'), 
                    'nudged' => (is_object($notification->getUserTo()) ? $notification->getUserTo()->getNudged() : 'unknown')
                );
            }
        }
        
        $kooshImageWebPath = $this->get('kapi_helper')->getProfileThumbnailURL($koosh, true);
        
        $incompleteKooshes[$keyK] = array(
            'koosh_title' => $koosh->getTitle(), 
            'koosh_type' => $koosh->getType(), 
            'people_involved' => $involvedPeople, 
            'image_url' => !empty($kooshImageWebPath) ? $baseUrl . $kooshImageWebPath : '',
            'koosh_id' => $koosh->getId(),
            'user_id' => $koosh->getUserId(),
            'people_waiting' => $peopleWaiting,             
        );
    }
    
    if (!$entity) {
        return array(
            'status' => 'error', 
            'message' => 'There are no incomplete kooshes in the system.'
        );
    } else {
        return array(
            'status' => 'ok', 
            'data' => array('incompleteKooshes' => $incompleteKooshes)
        );
    }
}
    

/**
* Nudges a user if they haven’t submitted their video yet.
*
* @ApiDoc(
* resource = true,
* description = "Nudges a user if they haven’t submitted their video yet.",
* output = "preferred rate",
* statusCodes = {
* 200 = "Returned when successful",
* 404 = "Returned when the page is not found"
* }
* )
*
* @param \Symfony\Component\HttpFoundation\Request $request
* @param int $pin
* @return mixed
* @Post("/nudge_user")
* 
* @View
*/
public function nudgeUserAction(Request $request)
{
      // Get Params
    $userIdToNudge = $request->get('user_id_to_nudge');
    $userId = $request->get('user_id');
    $kooshId = $request->get('koosh_id');
    
    // Validation
    $errors = array();
    if(empty($userIdToNudge)) $errors[] = 'Empty `user_id_to_nudge` param.'; 
    if(empty($userId)) $errors[] = 'Empty `user_id` param.'; 
    if(empty($kooshId)) $errors[] = 'Empty `koosh_id` param.'; 
    
    $em = $this->getDoctrine()->getManager();
    $koosh = $em->getRepository('KAPIKooshApiBundle:Koosh')->findOneBy(array('id' => $kooshId));
    if(is_object($koosh)) {
        // Check user "nudge" flag
        if(!empty($userIdToNudge)) {
            if($koosh->getType() == 'koosh_kapture') {
                $image = $em->getRepository('KAPIKooshApiBundle:Image')->findOneBy(array('userId' => $userIdToNudge, 'kooshId' => $kooshId));
                if(!is_object($image)) $errors[] = "There is no image related to specific user and koosh";
                elseif($image->getNudged() == '1') $errors[] = "User has been already nudged.";
            } elseif($koosh->getType() == 'koosh_video') {
                $video = $em->getRepository('KAPIKooshApiBundle:Video')->findOneBy(array('userId' => $userIdToNudge, 'kooshId' => $kooshId));
                if(!is_object($video)) $errors[] = "There is no video related to specific user and koosh";
                elseif($video->getNudged() == '1') $errors[] = "User has been already nudged.";
            } else {
                $item = $em->getRepository('KAPIKooshApiBundle:KooshItem')->findOneBy(array('userId' => $userIdToNudge, 'kooshId' => $kooshId));
                if(!is_object($item)) $errors[] = "There is no item related to specific user and koosh";
                else {
                    if(($item->getSourceType() == 'video') && ($item->getVideo()->getNudged() == '1')) $errors[] = "User has been already nudged.";
                    elseif(($item->getSourceType() == 'image') && ($item->getImage()->getNudged() == '1')) $errors[] = "User has been already nudged.";
                }
            }
        }
    } else {
        $errors[] = 'Unable to find koosh with id:'.$kooshId.'.'; 
    }
    
    if(sizeof($errors)) {
        return array(
        'status' => 'error',
         'message' => 'Validation error.',
         'validation' => $errors
        );

    } else {
        $userFrom = $em->getRepository('KAPIKooshApiBundle:User')->findOneBy(array('id' => $userId));
        $userTo = $em->getRepository('KAPIKooshApiBundle:User')->findOneBy(array('id' => $userIdToNudge));
        
        $notificationMessage = $userFrom->getUsername() . ' wants you to submit a video for ' . $koosh->getTitle();

        if($this->container->getParameter('bool_send_push_notifications')) {
            // Send PUSH notification - IOs
            try {
                $iosMessage = new iOSMessage();
                $iosMessage->setMessage($notificationMessage);
                $iosMessage->setDeviceIdentifier($userTo->getPushId());

                $this->container->get('rms_push_notifications')->send($iosMessage);
            } catch (Exception $ex) {
                return array(
                    'status' => 'error', 
                    'message' => 'Unable to send iOS pushup notification.'
                );
            }
            // Send PUSH notification - Android
            try {
                $androidMessage = new AndroidMessage();
                $androidMessage->setGCM(true);
                $androidMessage->setMessage($notificationMessage);
                $androidMessage->setDeviceIdentifier($userTo->getPushId());

                $this->container->get('rms_push_notifications')->send($androidMessage);
            } catch (Exception $ex) {
                return array(
                    'status' => 'error', 
                    'message' => 'Unable to send Android pushup notification.'
                );
            }
        }
        
        
        // Update nudged flag
        if(($koosh->getType() == 'koosh_kapture') || (($koosh->getType() == 'koosh_mix') && ($item->getSourceType() == 'image'))) {
            $image->setNudged('1');
            $em->persist($image);
            $em->flush();
        } elseif(($koosh->getType() == 'koosh_video') || (($koosh->getType() == 'koosh_mix') && ($item->getSourceType() == 'video'))) {
            $video->setNudged('1');
            $em->persist($video);
            $em->flush();
        } 
        
        return array(
            'status' => 'ok', 
            'message' => 'User has been successfully nudged.'
        );
    }
}


/**
* Gets a list of complete kooshes.
*
* @ApiDoc(
* resource = true,
* description = "Gets a list of complete kooshes.",
* output = "preferred rate",
* statusCodes = {
* 200 = "Returned when successful",
* 404 = "Returned when the page is not found"
* }
* )
*
* @param \Symfony\Component\HttpFoundation\Request $request
* @param int $userId
* @return mixed
* @Get("/get_complete_koosh/{userId}")
* 
* @View
*/
public function getCompleteKooshAction($userId)
{
    $baseUrl = $this->getRequest()->getSchemeAndHttpHost() . $this->container->getParameter('web_url_prefix');
    
    $em = $this->getDoctrine()->getManager();
    $entity = $em->getRepository('KAPIKooshApiBundle:Koosh')->findBy(array('status' => '1', 'userId' => $userId), array('created' => 'DESC'));

    $completeKooshes = array();
    foreach($entity AS $key => $koosh) {
        // get people involved
        $users = $em->getRepository('KAPIKooshApiBundle:User')->findAllInvolvedByKoosh($koosh->getId(), $koosh->getType());
        $involvedPeople = sizeof($users);
        
        $kooshImageWebPath = $this->get('kapi_helper')->getProfileThumbnailURL($koosh, true);
        
        $completeKooshes[$key] = array(
            'koosh_title' => $koosh->getTitle(), 
            'people_involved' => $involvedPeople, 
            'image_url' => !empty($kooshImageWebPath) ? $baseUrl . $kooshImageWebPath : '',
            'koosh_id' => $koosh->getId(), 
            'user_id' => $koosh->getUserId(), 
        );
    }
    
    if (!$entity) {
        return array(
            'status' => 'error', 
            'message' => 'There are no complete kooshes in the system.'
        );
    } else {
        return array(
            'status' => 'ok', 
            'data' => array('completeKooshes' => $completeKooshes)
        );
    }
}



/**
* Initiates a koosh and sends to the relevant people.
*
* @ApiDoc(
* resource = true,
* description = "Initiates a koosh and sends to the relevant people.",
* output = "",
* statusCodes = {
* 200 = "Returned when successful",
* 404 = "Returned when the page is not found"
* }
* )
*
* @param \Symfony\Component\HttpFoundation\Request $request
* @return mixed
* @Post("/create_a_koosh")
* 
* @View
*/
public function createAKooshAction(Request $request)
{
    $baseUrl = $this->getRequest()->getSchemeAndHttpHost() . $this->container->getParameter('web_url_prefix');
    $em = $this->getDoctrine()->getManager();
    
    // Get Params
    $userId = $request->get('user_id');
    $title = urldecode($request->get('title'));
    $genre = urldecode($request->get('genre'));
    $type = urldecode($request->get('type'));
    $length = urldecode($request->get('length'));
    $publicStatus = urldecode($request->get('public_status'));
    $notificationMessage = urldecode($request->get('notification_message'));
    $userArray = $request->get('user_array');
    $kooshMe = $request->get('koosh_me');
   
    $disableNotifications = (!empty($kooshMe) && ($kooshMe == '1')) ? true : false;
    
    // Validation
    $errors = array();
    if(empty($userId)) $errors[] = 'Empty `user_id` param.'; 
    if(empty($title)) $errors[] = 'Empty `title` param.'; 
    if(empty($genre)) $errors[] = 'Empty `genre` param.';
    if(empty($type)) $errors[] = 'Empty `type` param.';
    if(!empty($type) && ($type != 'koosh_video') && ($type != 'koosh_kapture') && ($type != 'koosh_mix')) $errors[] = 'Wrong `type` param. Allowed values are "koosh_video", "koosh_kapture" or "koosh_mix".';
    if(($type == 'koosh_video') && empty($length)) $errors[] = 'Empty `length` param.';
    if(empty($publicStatus)) $errors[] = 'Empty `public_status` param.';
    if(empty($userArray)) $errors[] = 'Empty `user_array` param.'; 
    
    if(sizeof($errors)) { 
       return array(
                'status' => 'error', 
                'message' => 'Validation error.', 
                'validation' => $errors
            );
       
    } else {
        $user = $em->getRepository('KAPIKooshApiBundle:User')->findOneBy(array('id' => $userId));
        
        if(is_object($user)) {
            // Persist Koosh
            $koosh = new Koosh();
            $koosh->setUser($user);
            $koosh->setTitle($title);
            $koosh->setGenre($genre);
            $koosh->setType($type);
            $koosh->setLength($length);
            $koosh->setPublicStatus($publicStatus);
            $koosh->setNotificationMessage($notificationMessage);
            $koosh->setDisableNotifications($disableNotifications ? 1 : 0);

            $em->persist($koosh);
            $em->flush();

            $usersData = json_decode($userArray, true);

            $this->container->get('api.logger')->addInfo('create_a_koosh - userArray:', array('usersData' => $usersData)); 

            // Send notifiation to involved users
            if(sizeof($usersData['users'])) {
                foreach($usersData['users'] AS $key => $involvedUserData){
                    $involvedUser = $em->getRepository('KAPIKooshApiBundle:User')->findOneBy(array('id' => $involvedUserData['user_id']));

                    $notification = new Notification();
                    $notification->setUserFrom($user);
                    $notification->setUserTo($involvedUser);
                    $notification->setKoosh($koosh);
                    $notification->setAmount(isset($involvedUserData['amount']) ? $involvedUserData['amount'] : 0);
                    $notification->setType('koosh_invite');

                    $em->persist($notification);
                    $em->flush();

                    if($this->container->getParameter('bool_send_push_notifications')  && !$disableNotifications) {
                        // Send PUSH notification - IOs
                        $notificationMessage = $user->getUsername() . ' has invited you to ' . $koosh->getTitle();
                        try {
                            $iosMessage = new iOSMessage();
                            $iosMessage->setMessage($notificationMessage);
                            $iosMessage->setDeviceIdentifier($involvedUser->getPushId());

                            $this->container->get('rms_push_notifications')->send($iosMessage);
                        } catch (Exception $ex) {
                            $this->container->get('api.logger')->addError('delete_koosh - Unable to send iOS pushup notification - pushID:'.$involvedUser->getPushId()); 
                        }
                        // Send PUSH notification - Android
                        try {
                            $androidMessage = new AndroidMessage();
                            $androidMessage->setGCM(true);
                            $androidMessage->setMessage($notificationMessage);
                            $androidMessage->setDeviceIdentifier($involvedUser->getPushId());

                            $this->container->get('rms_push_notifications')->send($androidMessage);
                        } catch (Exception $ex) {
                            $this->container->get('api.logger')->addError('delete_koosh - Unable to send Android pushup notification - pushID:'.$involvedUser->getPushId()); 
                        }
                    }
                }
            }

            return array(
                'status' => 'ok', 
                'message' => 'Koosh has been successfully created.', 
                'data' => array('koosh_id' => $koosh->getId())
            );
        } else {
            return array(
                'status' => 'error', 
                'message' => 'Unable to find user with id:'.$userId.' .'
            );
        }
        
    }
    
}


/**
* Gets the koosh items for completion.
*
* @ApiDoc(
* resource = true,
* description = "Gets the koosh items for completion.",
* output = "preferred rate",
* statusCodes = {
* 200 = "Returned when successful",
* 404 = "Returned when the page is not found"
* }
* )
*
* @param \Symfony\Component\HttpFoundation\Request $request
* @param int $kooshId
* @return mixed
* @Get("/get_koosh_items/{kooshId}")
* 
* @View
*/
public function getKooshItemsAction($kooshId)
{
    $baseUrl = $this->getRequest()->getSchemeAndHttpHost() . $this->container->getParameter('web_url_prefix');
    
    $em = $this->getDoctrine()->getManager();
    $koosh = $em->getRepository('KAPIKooshApiBundle:Koosh')->findOneBy(array('id' => $kooshId));
    
     if (!$koosh) {
        return array(
            'status' => 'error', 
            'message' => 'Unable to find koosh with id:' . $kooshId . ' in the system.'
        );
    } else {
        
        if($koosh->getType() == 'koosh_kapture') {
            $entity = $em->getRepository('KAPIKooshApiBundle:Image')->findBy(array('kooshId' => $kooshId));

            $objects = array();
            foreach($entity AS $key => $image) {
                $imageWebPath = $this->get('kapi_helper')->getProfileThumbnailURL($image, true);
                $objects[$key] = array(
                    'image_url' => !empty($imageWebPath) ? $imageWebPath : '',
                    'url' => $baseUrl . '/' . $image->getImageFileWebPath(),
                    'user_name' => $image->getUser()->getUsername(), 
                    'unique_id' => $image->getId(),
                );
            }
        } elseif($koosh->getType() == 'koosh_video') { 
            $entity = $em->getRepository('KAPIKooshApiBundle:Video')->findBy(array('kooshId' => $kooshId));

            $objects = array();
            foreach($entity AS $key => $video) {
                $videoImageWebPath = $this->get('kapi_helper')->getProfileThumbnailURL($video, true);
                $objects[$key] = array(
                    'image_url' => !empty($videoImageWebPath) ? $videoImageWebPath : '',
                    'url' => $baseUrl . '/' . $video->getVideoFileWebPath(),
                    'user_name' => $video->getUser()->getUsername(), 
                    'unique_id' => $video->getId(),
                );
            }
        } else {
            $entity = $em->getRepository('KAPIKooshApiBundle:KooshItem')->findBy(array('kooshId' => $kooshId));

            $objects = array();
            foreach($entity AS $key => $item) {
                if($item->getSourceType() == 'image') {
                    $image = $item->getImage();
                    $imageWebPath = $this->get('kapi_helper')->getProfileThumbnailURL($image, true);
                    $objects[$key] = array(
                        'image_url' => !empty($imageWebPath) ? $imageWebPath : '',
                        'url' => $baseUrl . '/' . $image->getImageFileWebPath(),
                        'user_name' => $image->getUser()->getUsername(), 
                        'unique_id' => $item->getId(),
                    );
                } else {
                    $video = $item->getVideo();
                    $videoImageWebPath = $this->get('kapi_helper')->getProfileThumbnailURL($video, true);
                    $objects[$key] = array(
                        'image_url' => !empty($videoImageWebPath) ? $videoImageWebPath : '',
                        'url' => $baseUrl . '/' . $video->getVideoFileWebPath(),
                        'user_name' => $video->getUser()->getUsername(), 
                        'unique_id' => $item->getId(),
                    ); 
                }
            }
        }
        if (!$entity) {
            return array(
                'status' => 'error', 
                'message' => 'There are no related videos in the system.'
            );
        } else {
            return array(
                'status' => 'ok', 
                'data' => array('objects' => $objects)
            );
        }
       
    }
}


/**
 * @param \Symfony\Component\HttpFoundation\Request $request
 * @return mixed
 * @Post("/koosh_this_video")
 * @View
 */
public function koostHisVideoAudioAction(Request $request) {
    return $this->kooshThisVideoAction($request);
}

/**
* Sends the information to the backend for video compilation.
*
* @ApiDoc(
* resource = true,
* description = "Sends the information to the backend for video compilation.",
* output = "",
* statusCodes = {
* 200 = "Returned when successful",
* 404 = "Returned when the page is not found"
* }
* )
*
* @param \Symfony\Component\HttpFoundation\Request $request
* @return mixed
* @Patch("/koosh_this_video")
* 
* @View
*/
public function kooshThisVideoAction(Request $request) {
    
    $baseUrl = $this->getRequest()->getSchemeAndHttpHost() . $this->container->getParameter('web_url_prefix');
    
    $em = $this->getDoctrine()->getManager();
    
    // Get Params
    $userId = $request->get('user_id');
    $kooshId = $request->get('koosh_id');
    $fadeType = $request->get('fade_type');
    $orderArray = $request->get('order_array');
    $addAudio = $request->get('audio_type'); // 'none', 'clips', 'koosh', 'system_audio_koosh'
    $audioData = $request->get('audio_data');
    $systemAudioId = $request->get('system_audio_id');
    
    // {"items":[{"unique_id":"2","caption":"Video Caption","muted_bool":"0"},{"unique_id":"1","caption":"Video test Caption","muted_bool":"1"}]}
    
    $audioUploadedFile = $request->files->get(str_replace('.', '_', $audioData));
    
    // Validation
    $errors = array();
    if(empty($userId)) $errors[] = 'Empty `user_id` param.'; 
    if(empty($kooshId)) $errors[] = 'Empty `koosh_id` param.'; 
    if(empty($fadeType)) $errors[] = 'Empty `fade_type` param.';
    if(empty($orderArray)) $errors[] = 'Empty `order_array` param.';
    if(!sizeof($orderArray)) $errors[] = 'You have to specify some videos/images.';
    if(($addAudio !== null )&&(($addAudio != 'koosh') && ($addAudio != 'clips') && ($addAudio != 'none') && ($addAudio != 'system_audio_koosh'))) {
        $errors[] = 'Wrong `audio_type` param. (possible options: `none`, `clips`, `koosh`, `system_audio_koosh`)';
    }
    if($addAudio == 'system_audio_koosh') {
        if(empty($systemAudioId)) $errors[] = 'Empty `system_audio_id` param.'; 
        else {
            $systemAudio = $em->getRepository('KAPIKooshApiBundle:SystemAudio')->findOneBy(array('id' => $systemAudioId));
            if(!is_object($systemAudio)) $errors[] = 'There is no system audio track with id:' . $systemAudioId . '.';
        }
    }
    if(!empty($fadeType) && ($fadeType != 'none')) {
        $transition = $em->getRepository('KAPIKooshApiBundle:Transition')->findOneBy(array('code' => $fadeType));
        if(!is_object($transition)) $errors[] = 'There is no transition with code:' . $fadeType . '.';
    }
        
    // Unique email and username validation
    $em = $this->getDoctrine()->getManager();
    
    $koosh = $em->getRepository('KAPIKooshApiBundle:Koosh')->findOneBy(array('id' => $kooshId));
    if(is_object($koosh)) {
        if(!empty($addAudio) && ($addAudio == 'koosh') && empty($audioData)) {
            $errors[] = 'Empty `audio_data` param.'; 
        }
    } else {
        $errors[] = 'There is no koosh with id:' . $kooshId . '.';
    }
    
    // Audio file format validation
    if(is_object($audioUploadedFile)) {
        $extension = strtolower($audioUploadedFile->guessExtension());
        if(($extension != 'mp3') && ($extension != 'mpga') && ($extension != 'mp4a')) $errors[] = 'Audio should be in ´.mp3´ format.';
    }
    
    if(sizeof($errors)) { 
       return array(
                'status' => 'error', 
                'message' => 'Validation error.', 
                'validation' => $errors
            );
       
    } else {
        // Try to upload and update video
        try { 
            $items = json_decode($orderArray, true);
            
            
            // Store audio
            if(!empty($addAudio) && ($addAudio == 'koosh')) { 
                $koosh->setAudioFile($audioUploadedFile);
                $em->persist($koosh);
                $em->flush();
                
            } else {
                $koosh->setAudioFile(null);
            }
            
            // RABBITMQ message
            $this->container->get('add_video_task')->process(array('kooshId' => $kooshId, 'items' => $items['items'], 'fadeType' => $fadeType));
            
            
            // Track added time
            $koosh->setRabbitQueueAdded(new \DateTime());
            $koosh->setAddAudio($addAudio);
            $koosh->setSystemAudioId($systemAudioId);
            $koosh->setAudioFile(null);
            $em->persist($koosh);
            $em->flush();
            

            if($koosh->getType() == 'koosh_video') {
                $message = "Videos have been successfully kooshed.";
            } else {
                $message = "Pictures have been successfully kooshed.";
            };
            
            return array(
                'status' => 'ok', 
                'message' => $message,
                'data' => array(
                    'koosh_url' => $baseUrl . '/' . $koosh->getVideoFileWebPath()
                )
            );
        } catch (Exception $e) {
            return array(
                'status' => 'error', 
                'message' => 'Unable to koosh videos - error: ' . $e->getMessage(), 
            );
        }
    }
}

/**
* Uploads and assigns audio to the specific video record
*
* @ApiDoc(
* resource = true,
* description = "Uploads and assigns audio to the specific video record.",
* output = "user id",
* statusCodes = {
* 200 = "Returned when successful",
* 404 = "Returned when the page is not found"
* }
* )
*
* @param \Symfony\Component\HttpFoundation\Request $request
* @return mixed
* @Post("/upload_audio")
* 
* @View
*/
public function uploadAudioAction(Request $request)
{
    $em = $this->getDoctrine()->getManager();
    
    // Get Params
    $userId = $request->get('user_id');
    $videoId = $request->get('video_id');
    $audioData = $request->get('audio_data');
    
    $audioUploadedFile = $request->files->get(str_replace('.', '_', $audioData));
    
    
    // Validation
    $errors = array();
    if(empty($userId)) $errors[] = 'Empty `user_id` param.'; 
    if(empty($videoId)) $errors[] = 'Empty `video_id` param.'; 
    if(empty($audioData)) $errors[] = 'Empty `audio_data` param.'; 
    if(!isset($request->files) || !sizeof($request->files)) {
        $errors[] = 'Empty POST FILES data.'; 
    }
    
    $video = $em->getRepository('KAPIKooshApiBundle:Video')->findOneBy(array('id' => $videoId));

    if(is_object($video)) {
        // Audio file format validation
        if(is_object($audioUploadedFile)) {
            $extension = strtolower($audioUploadedFile->guessExtension());
            if(($extension != 'mp3') && ($extension != 'mpga') && ($extension != 'mp4a')) $errors[] = 'Audio should be in ´.mp3´ format.';
        }
    } else {
        $errors[] = 'Unable to find video with `video_id`.'; 
    }
    
    
    if(sizeof($errors)) {
        return array(
        'status' => 'error',
         'message' => 'Validation error.',
         'validation' => $errors
        );

    } else {
        
        $user = $em->getRepository('KAPIKooshApiBundle:User')->findOneBy(array('id' => $userId));

        $video->setVideoFile(null);
        $video->setAddAudio(1);
        $video->setAudioFile($audioUploadedFile);

        $em->persist($video);
        $em->flush();


        return array(
            'status' => 'ok', 
            'message' => 'Audio has been successfully uploaded.', 
            'data' => array('video_id' => $video->getId())
        );
    }
}


/**
* Gets a list of the top 10 newest kooshes from all kooshes.
*
* @ApiDoc(
* resource = true,
* description = "Gets a list of the top 10 newest kooshes from all kooshes.",
* output = "preferred rate",
* statusCodes = {
* 200 = "Returned when successful",
* 404 = "Returned when the page is not found"
* }
* )
*
* @param \Symfony\Component\HttpFoundation\Request $request
* @param int $userId
* @return mixed
* @Get("/get_latest_kooshes/{userId}")
* 
* @View
*/
public function getLatestKooshesAction($userId)
{
    $baseUrl = $this->getRequest()->getSchemeAndHttpHost() . $this->container->getParameter('web_url_prefix');
    
    $em = $this->getDoctrine()->getManager();
    
    $user = $em->getRepository('KAPIKooshApiBundle:User')->findOneBy(array('id' => $userId));

    if (!is_object($user)) {
        return array(
            'status' => 'error', 
            'message' => 'Unable to find user with id:' . $userId . '.'
        );
    } else {
    
        $kooshes = $em->getRepository('KAPIKooshApiBundle:Koosh')->findBy(array('status' => '3'), array('created' => 'DESC'), 10);

        $latestKooshes = array();
        $latestKooshesKey = 0;
        foreach($kooshes AS $key => $koosh) {
            $kooshImageWebPath = $this->get('kapi_helper')->getProfileThumbnailURL($koosh, true);
            
            // Check friendship
            if(strtolower($koosh->getPublicStatus()) == 'private') {
                if($userId == $koosh->getUser()->getId()) {
                    $checkInvolved = true;
                } else {
                    //$checkFriendship = $em->getRepository('KAPIKooshApiBundle:Friendship')->checkExistingFriendship($userId, $koosh->getUser()->getId());
                    $checkInvolved = $em->getRepository('KAPIKooshApiBundle:User')->checkInvolvedByKoosh($userId, $koosh->getId(), $koosh->getType());
                }
            }
            
            if((strtolower($koosh->getPublicStatus()) == 'public') || ((strtolower($koosh->getPublicStatus()) == 'private') && ($checkInvolved))) {
                $latestKooshes[$latestKooshesKey] = array(
                    'koosh_title' => $koosh->getTitle(), 
                    'koosh_likes' => sizeof($koosh->getKooshLikes()), 
                    'koosh_comments' => sizeof($koosh->getComments()), 
                    'koosh_creator' => is_object($koosh->getUser()) ? $koosh->getUser()->getFirstName().' '.$koosh->getUser()->getLastName() : '',
                    'koosh_public_status' => $koosh->getPublicStatus(), 
                    'koosh_id' => $koosh->getId(), 
                    'user_id' => $koosh->getUserId(), 
                    'image_url' => !empty($kooshImageWebPath) ? $kooshImageWebPath : '',
                );
                
                $latestKooshesKey++;
            }
        }

        if (!sizeof($latestKooshes)) {
            return array(
                'status' => 'error', 
                'message' => 'There are no kooshes in the system.'
            );
        } else {
            return array(
                'status' => 'ok', 
                'data' => array('koosh_list' => $latestKooshes)
            );
        }
    }
}
    

/**
* Gets a list of the top 10 newest kooshes from all kooshes.
*
* @ApiDoc(
* resource = true,
* description = "Gets a list of the top 10 newest kooshes from all kooshes.",
* output = "preferred rate",
* statusCodes = {
* 200 = "Returned when successful",
* 404 = "Returned when the page is not found"
* }
* )
*
* @param \Symfony\Component\HttpFoundation\Request $request
* @return mixed
* @Patch("/search_kooshes")
* 
* @View
*/
public function searchKooshesAction(Request $request)
{
    $baseUrl = $this->getRequest()->getSchemeAndHttpHost() . $this->container->getParameter('web_url_prefix');
    
    $em = $this->getDoctrine()->getManager();
    
    // Get Params
    $userId = $request->get('user_id');
    $searchString = $request->get('search_string');
    $genreString = $request->get('genre_string');
    
    $user = $em->getRepository('KAPIKooshApiBundle:User')->findOneBy(array('id' => $userId));

    if (!is_object($user)) {
        return array(
            'status' => 'error', 
            'message' => 'Unable to find user with id:' . $userId . '.'
        );
    } else {
    
        // Validation
        $errors = array();
        if(empty($userId)) $errors[] = 'Empty `user_id` param.'; 
        if(empty($searchString) && empty($genreString)) $errors[] = 'Empty both `search_string` and `genre_string` params. One of them has to be set.';        

        if(sizeof($errors)) { 
           return array(
                    'status' => 'error', 
                    'message' => 'Validation error.', 
                    'validation' => $errors
                );

        } else {
            
            $kooshes = $em->getRepository('KAPIKooshApiBundle:Koosh')->findAllBySearchString($searchString, $genreString);
            
            $latestKooshes = array();
            $latestKooshesKey = 0;
            foreach($kooshes AS $key => $koosh) {
               $kooshImageWebPath = $this->get('kapi_helper')->getProfileThumbnailURL($koosh, true);
                
                // Check friendship
                if(strtolower($koosh->getPublicStatus()) == 'private') {
                    if($userId == $koosh->getUser()->getId()) {
                        $checkInvolved = true;
                    } else {
                        //$checkFriendship = $em->getRepository('KAPIKooshApiBundle:Friendship')->checkExistingFriendship($userId, $koosh->getUser()->getId());
                        $checkInvolved = $em->getRepository('KAPIKooshApiBundle:User')->checkInvolvedByKoosh($userId, $koosh->getId(), $koosh->getType());
                    }
                }

                if((strtolower($koosh->getPublicStatus()) == 'public') || ((strtolower($koosh->getPublicStatus()) == 'private') && ($checkInvolved))) {
                    
                    // Get profile image
                    $imagePath = $this->get('kapi_helper')->getProfileThumbnailURL($koosh->getUser(), true); 
                    
                    $latestKooshes[$latestKooshesKey] = array(
                        'koosh_title' => $koosh->getTitle(), 
                        'koosh_likes' => sizeof($koosh->getKooshLikes()), 
                        'koosh_comments' => sizeof($koosh->getComments()), 
                        'koosh_creator' => is_object($koosh->getUser()) ? $koosh->getUser()->getFirstName().' '.$koosh->getUser()->getLastName() : '',
                        'koosh_id' => $koosh->getId(), 
                        'user_id' => $koosh->getUserId(), 
                        'user_image' => !empty($imagePath) ? $imagePath : '', 
                        'image_url' => !empty($kooshImageWebPath) ? $kooshImageWebPath : '', 
                        'fame_value' => $koosh->getUser()->getFameValue()
                    );
                    $latestKooshesKey++;
                }
            }

            if (!sizeof($kooshes)) {
                return array(
                    'status' => 'error', 
                    'message' => 'There are no kooshes in the system.'
                );
            } else {
                return array(
                    'status' => 'ok', 
                    'data' => array('koosh_list' => $latestKooshes)
                );
            }
        }
    }
}

/**
* Gets a list of the top 10 newest kooshes from all kooshes.
*
* @ApiDoc(
* resource = true,
* description = "Gets a list of the top 10 newest kooshes from all kooshes.",
* output = "preferred rate",
* statusCodes = {
* 200 = "Returned when successful",
* 404 = "Returned when the page is not found"
* }
* )
*
* @param \Symfony\Component\HttpFoundation\Request $request
* @return mixed
* @Patch("/search_followers_kooshes")
* 
* @View
*/
public function searchFollowersKooshesAction(Request $request)
{
    $baseUrl = $this->getRequest()->getSchemeAndHttpHost() . $this->container->getParameter('web_url_prefix');
    
    $em = $this->getDoctrine()->getManager();
    
    // Get Params
    $userId = $request->get('user_id');
    $searchString = $request->get('search_string');
    $genreString = $request->get('genre_string');
    
    $user = $em->getRepository('KAPIKooshApiBundle:User')->findOneBy(array('id' => $userId));

    if (!is_object($user)) {
        return array(
            'status' => 'error', 
            'message' => 'Unable to find user with id:' . $userId . '.'
        );
    } else {
    
        // Validation
        $errors = array();
        if(empty($userId)) $errors[] = 'Empty `user_id` param.'; 
        if(empty($searchString) && empty($genreString)) $errors[] = 'Empty both `search_string` and `genre_string` params. One of them has to be set.';        

        if(sizeof($errors)) { 
           return array(
                    'status' => 'error', 
                    'message' => 'Validation error.', 
                    'validation' => $errors
                );

        } else {
            
            $kooshes = $em->getRepository('KAPIKooshApiBundle:Koosh')->findAllBySearchString($searchString, $genreString);
            
            $latestKooshes = array();
            $latestKooshesKey = 0;
            foreach($kooshes AS $key => $koosh) {
               $kooshImageWebPath = $this->get('kapi_helper')->getProfileThumbnailURL($koosh, true);
                
                // Check friendship
                if(strtolower($koosh->getPublicStatus()) == 'private') {
                    if($userId == $koosh->getUser()->getId()) {
                        $checkInvolved = true;
                    } else {
                        $checkInvolved = $em->getRepository('KAPIKooshApiBundle:User')->checkInvolvedByKoosh($userId, $koosh->getId(), $koosh->getType());
                    }
                }
                
                $checkFriendship = $em->getRepository('KAPIKooshApiBundle:Friendship')->checkExistingFriendship($userId, $koosh->getUser()->getId());
                
                
                if(((strtolower($koosh->getPublicStatus()) == 'public') || ((strtolower($koosh->getPublicStatus()) == 'private') && ($checkInvolved))) && $checkFriendship) {
                    
                    // Get profile image
                    $imagePath = $this->get('kapi_helper')->getProfileThumbnailURL($koosh->getUser(), true); 
                    
                    $latestKooshes[$latestKooshesKey] = array(
                        'koosh_title' => $koosh->getTitle(), 
                        'koosh_likes' => sizeof($koosh->getKooshLikes()), 
                        'koosh_comments' => sizeof($koosh->getComments()), 
                        'koosh_creator' => is_object($koosh->getUser()) ? $koosh->getUser()->getFirstName().' '.$koosh->getUser()->getLastName() : '',
                        'koosh_id' => $koosh->getId(), 
                        'user_id' => $koosh->getUserId(), 
                        'user_image' => !empty($imagePath) ? $imagePath : '', 
                        'image_url' => !empty($kooshImageWebPath) ? $kooshImageWebPath : '', 
                        'fame_value' => $koosh->getUser()->getFameValue()
                    );
                    $latestKooshesKey++;
                }
            }

            if (!sizeof($kooshes)) {
                return array(
                    'status' => 'error', 
                    'message' => 'There are no kooshes in the system.'
                );
            } else {
                return array(
                    'status' => 'ok', 
                    'data' => array('koosh_list' => $latestKooshes)
                );
            }
        }
    }
}

/**
* Get the koosh information to view it.
*
* @ApiDoc(
* resource = true,
* description = "Get the koosh information to view it.",
* output = "preferred rate",
* statusCodes = {
* 200 = "Returned when successful",
* 404 = "Returned when the page is not found"
* }
* )
*
* @param \Symfony\Component\HttpFoundation\Request $request
* @return mixed
* @Patch("/view_koosh")
* 
* @View
*/
public function viewKooshAction(Request $request)
{
    $baseUrl = $this->getRequest()->getSchemeAndHttpHost() . $this->container->getParameter('web_url_prefix');
    
    $em = $this->getDoctrine()->getManager();
    
    // Get Params
    $userId = $request->get('user_id');
    $kooshId = $request->get('koosh_id');

    // Validation
    $errors = array();
    if(empty($userId)) $errors[] = 'Empty `user_id` param.'; 
    if(empty($kooshId)) $errors[] = 'Empty `koosh_id` param.'; 
    
    if(sizeof($errors)) { 
       return array(
                'status' => 'error', 
                'message' => 'Validation error.', 
                'validation' => $errors
            );
       
    } else {
    
        $user = $em->getRepository('KAPIKooshApiBundle:User')->findOneBy(array('id' => $userId));
        $koosh = $em->getRepository('KAPIKooshApiBundle:Koosh')->findOneBy(array('id' => $kooshId));
        
        if (!is_object($user)) {
            return array(
                'status' => 'error', 
                'message' => 'Unable to find user with id:' . $userId . '.'
            ); 
        } elseif (!is_object($koosh)) {
            return array(
                'status' => 'error', 
                'message' => 'Unable to find koosh with id:' . $kooshId . '.'
            );
        } else {
            // Get koosh comments
            $commentList = array();
            if(sizeof($koosh->getComments())) {
                foreach($koosh->getComments() AS $key => $comment) {
                    $userImageWebPath = $this->get('kapi_helper')->getProfileThumbnailURL($comment->getUser());

                    
                    // Get all user kooshes likes
                    $kooshesLikes = 0;
                    if(sizeof($comment->getUser()->getKooshes())) {
                        foreach($comment->getUser()->getKooshes() AS $commentKoosh) {
                            $kooshesLikes += sizeof($commentKoosh->getKooshLikes());
                        }
                    }

                    // Get user friends count
                    $friends = $em->getRepository('KAPIKooshApiBundle:Friendship')->getUserFriends($comment->getUser()->getId());

                    // Get kooshes
                    $kooshes = $em->getRepository('KAPIKooshApiBundle:Koosh')->findBy(array('userId' => $comment->getUser()->getId(), 'status' => '3'));

                    
                    $commentList[$key] = array(
                        'comment_id' => $comment->getId(), 
                        'comment' => $comment->getText(), 
                        'user_name' => (is_object($comment->getUser()) ? $comment->getUser()->getUsername() : 'unknown'), 
                        'user_id' => (is_object($comment->getUser()) ? $comment->getUser()->getId() : 'unknown'), 
                        'friend_number' => sizeof($friends), 
                        'koosh_likes' => $kooshesLikes, 
                        'koosh_number' => sizeof($kooshes),
                        'user_image' => !empty($userImageWebPath) ? $userImageWebPath : '',
                    );
                }
            }

            // Check if user already starred the video
            $starredKoosh = is_object($em->getRepository('KAPIKooshApiBundle:KooshLike')->findOneBy(array('kooshId' => $kooshId, 'userId' => $userId))) ? 1 : 0;
            
            // Embed Code
            $embedCode = '<video width="320" height="240" controls>
                            <source src="'.$baseUrl . '/' . $koosh->getVideoFileWebPath().'" type="video/mp4">
                            Your browser does not support the video tag.
                        </video>';
            
            $kooshImageWebPath = $this->get('kapi_helper')->getProfileThumbnailURL($koosh, true);
            $kooshInfo = array(
                'koosh_url_q2' => $baseUrl . '/' . $koosh->getVideoFileWebPath(),
                'koosh_url' => $baseUrl . '/' . $koosh->getVideoFileWebPath(Koosh::QUALITY_2),
                'koosh_title' => $koosh->getTitle(), 
                'created_time' => $this->ago($koosh->getCreated()), 
                'starred_bool' => $starredKoosh, 
                'embed_code' => $embedCode, 
                'share_url' => $baseUrl . '/' . $koosh->getVideoFileWebPath(),
                'image_url' => !empty($kooshImageWebPath) ? $kooshImageWebPath : '',
                'koosh_creator' => (is_object($koosh->getUser()) ? $koosh->getUser()->getUsername() : 'unknown'), 
                'user_id' => (is_object($koosh->getUser()) ? $koosh->getUser()->getId() : 'unknown'), 
                'comment_list' => $commentList
            );

            return array(
                'status' => 'ok', 
                'data' => $kooshInfo
            );
        }
    }
}

public function pluralize( $count, $text ) 
{ 
    return $count . ( ( $count == 1 ) ? ( " $text" ) : ( " ${text}s" ) );
}

public function ago( $datetime )
{
    $interval = date_create('now')->diff( $datetime );
    $suffix = ( $interval->invert ? ' ago' : '' );
    if ( $v = $interval->y >= 1 ) return $this->pluralize( $interval->y, 'year' ) . $suffix;
    if ( $v = $interval->m >= 1 ) return $this->pluralize( $interval->m, 'month' ) . $suffix;
    if ( $v = $interval->d >= 1 ) return $this->pluralize( $interval->d, 'day' ) . $suffix;
    if ( $v = $interval->h >= 1 ) return $this->pluralize( $interval->h, 'hour' ) . $suffix;
    if ( $v = $interval->i >= 1 ) return $this->pluralize( $interval->i, 'minute' ) . $suffix;
    return $this->pluralize( $interval->s, 'second' ) . $suffix;
}

/**
* Star the koosh of id.
*
* @ApiDoc(
* resource = true,
* description = "Star the koosh of id.",
* output = "preferred rate",
* statusCodes = {
* 200 = "Returned when successful",
* 404 = "Returned when the page is not found"
* }
* )
*
* @param \Symfony\Component\HttpFoundation\Request $request
* @return mixed
* @Post("/star_koosh")
* 
* @View
*/
public function starKooshAction(Request $request)
{
    $baseUrl = $this->getRequest()->getSchemeAndHttpHost() . $this->container->getParameter('web_url_prefix');
    
    $em = $this->getDoctrine()->getManager();
    
    // Get Params
    $userId = $request->get('user_id');
    $kooshId = $request->get('koosh_id');
    
    // Validation
    $errors = array();
    if(empty($userId)) $errors[] = 'Empty `user_id` param.'; 
    if(empty($kooshId)) $errors[] = 'Empty `koosh_id` param.'; 
    
    if(sizeof($errors)) { 
       return array(
                'status' => 'error', 
                'message' => 'Validation error.', 
                'validation' => $errors
            );
       
    } else {
    
        $user = $em->getRepository('KAPIKooshApiBundle:User')->findOneBy(array('id' => $userId));
        $koosh = $em->getRepository('KAPIKooshApiBundle:Koosh')->findOneBy(array('id' => $kooshId));
    
        if (!is_object($user)) {
            return array(
                'status' => 'error', 
                'message' => 'Unable to find user with id:' . $userId . '.'
            );
        } elseif (!is_object($koosh)) {
            return array(
                'status' => 'error', 
                'message' => 'Unable to find koosh with id:' . $kooshId . '.'
            );
        } else {
            // Check starred
            $kooshLike = $em->getRepository('KAPIKooshApiBundle:KooshLike')->findOneBy(array('kooshId' => $kooshId, 'userId' => $userId));

            if (is_object($kooshLike)) {
                return array(
                    'status' => 'error', 
                    'message' => 'Koosh has been already starred by this user.'
                );
            } else {
                // Store kooshLike
                $kooshLike = new KooshLike();
                $kooshLike->setKoosh($koosh);
                $kooshLike->setUser($user);
                $em->persist($kooshLike);
                $em->flush();
                
                // send notification
                $notification = new Notification();
                $notification->setUserFrom($user);
                $notification->setUserTo($koosh->getUser());
                $notification->setKoosh($koosh);
                $notification->setType('koosh_like');
                $notification->setPermanent('1');
                $em->persist($notification);
                $em->flush();

                if($this->container->getParameter('bool_send_push_notifications')) {
                    // Send PUSH notification - IOs
                    $notificationMessage = $user->getUsername() . ' likes your koosh ' . $koosh->getTitle();
                    try {
                        $iosMessage = new iOSMessage();
                        $iosMessage->setMessage($notificationMessage);
                        $iosMessage->setDeviceIdentifier($koosh->getUser()->getPushId());

                        $this->container->get('rms_push_notifications')->send($iosMessage);
                    } catch (Exception $ex) {
                        $this->container->get('api.logger')->addError('star_koosh - Unable to send iOS pushup notification - pushID:'.$koosh->getUser()->getPushId()); 
                    }

                    // Send PUSH notification - Android
                    try {
                        $androidMessage = new AndroidMessage();
                        $androidMessage->setGCM(true);
                        $androidMessage->setMessage($notificationMessage);
                        $androidMessage->setDeviceIdentifier($koosh->getUser()->getPushId());

                        $this->container->get('rms_push_notifications')->send($androidMessage);
                    } catch (Exception $ex) {
                        $this->container->get('api.logger')->addError('star_koosh - Unable to send Android pushup notification - pushID:'.$koosh->getUser()->getPushId()); 
                    }
                }
                
                return array(
                    'status' => 'ok', 
                    'message' => 'Koosh has been successfully starred.', 
                    'data' => array(
                        'request_id' => $kooshLike->getId()
                    )
                );
            }
        }
    }
}

/**
* Comment the koosh of id.
*
* @ApiDoc(
* resource = true,
* description = "Comment the koosh of id.",
* output = "preferred rate",
* statusCodes = {
* 200 = "Returned when successful",
* 404 = "Returned when the page is not found"
* }
* )
*
* @param \Symfony\Component\HttpFoundation\Request $request
* @return mixed
* @Post("/comment_koosh")
* 
* @View
*/
public function commentKooshAction(Request $request)
{
    $baseUrl = $this->getRequest()->getSchemeAndHttpHost() . $this->container->getParameter('web_url_prefix');
    
    $em = $this->getDoctrine()->getManager();
    
    // Get Params
    $userId = $request->get('user_id');
    $kooshId = $request->get('koosh_id');
    $comment = urldecode($request->get('comment'));
    $tags = urldecode($request->get('tags'));



    // Validation
    $errors = array();
    if(empty($userId)) $errors[] = 'Empty `user_id` param.'; 
    if(empty($kooshId)) $errors[] = 'Empty `koosh_id` param.'; 
    if(empty($comment)) $errors[] = 'Empty `comment` param.'; 
    
    if(sizeof($errors)) { 
       return array(
                'status' => 'error', 
                'message' => 'Validation error.', 
                'validation' => $errors
            );
       
    } else {
    
        $mainUser = $em->getRepository('KAPIKooshApiBundle:User')->findOneBy(array('id' => $userId));
        $koosh = $em->getRepository('KAPIKooshApiBundle:Koosh')->findOneBy(array('id' => $kooshId));
    
        if (!is_object($mainUser)) {
            return array(
                'status' => 'error', 
                'message' => 'Unable to find user with id:' . $userId . '.'
            );
        } elseif (!is_object($koosh)) {
            return array(
                'status' => 'error', 
                'message' => 'Unable to find koosh with id:' . $kooshId . '.'
            );
        } else {

            // Store kooshComment
            $kooshComment = new KooshComment();
            $kooshComment->setKoosh($koosh);
            $kooshComment->setUser($mainUser);
            $kooshComment->setText($comment);
            $em->persist($kooshComment);
            $em->flush();
            
            // Tag users
            if(!empty($tags)) {
                $tags = json_decode($tags, true);
                if(is_array($tags) && sizeof($tags)) {
                    foreach($tags AS $tagUserId) {
                        $tagUser = $em->getRepository('KAPIKooshApiBundle:User')->findOneBy(array('id' => $tagUserId));
                        if(is_object($tagUser)) {
                            // Store tag
                            $kooshCommentTag = new KooshCommentTag();
                            $kooshCommentTag->setKooshComment($kooshComment);
                            $kooshCommentTag->setUser($tagUser);
                            $em->persist($kooshCommentTag);
                            $em->flush();
                            
                            // Send notification 
                            $notification = new Notification();
                            $notification->setUserFrom($mainUser);
                            $notification->setUserTo($tagUser);
                            $notification->setKoosh($koosh);
                            $notification->setType('tag');
                            $notification->setPermanent('1');
                            $em->persist($notification);
                            $em->flush();
                            
                            
                            if($this->container->getParameter('bool_send_push_notifications')) {
                                // Send PUSH notifications
                                $notificationMessage = $mainUser->getUsername() . ' tagged you in a comment on ' . $koosh->getTitle();
                                try {
                                    $iosMessage = new iOSMessage();
                                    $iosMessage->setMessage($notificationMessage);
                                    $iosMessage->setDeviceIdentifier($tagUser->getPushId());

                                    $this->container->get('rms_push_notifications')->send($iosMessage);
                                } catch (Exception $ex) {
                                    $this->container->get('api.logger')->addError('comment_koosh - Unable to send iOS pushup notification - pushID:'.$tagUser->getPushId()); 
                                }

                                // Send PUSH notification - Android
                                try {
                                    $androidMessage = new AndroidMessage();
                                    $androidMessage->setGCM(true);
                                    $androidMessage->setMessage($notificationMessage);
                                    $androidMessage->setDeviceIdentifier($tagUser->getPushId());

                                    $this->container->get('rms_push_notifications')->send($androidMessage);
                                } catch (Exception $ex) {
                                    $this->container->get('api.logger')->addError('comment_koosh - Unable to send Android pushup notification - pushID:'.$tagUser->getPushId()); 
                                }
                            }
                            
                        }
                    }
                }
            }
            
            
            // go through all users and send notification
            $users = $em->getRepository('KAPIKooshApiBundle:User')->getAllUsersInvolvedByKoosh($kooshId, $koosh->getType());
            
            $sentToUsers = array($userId);
            
            if(sizeof($users)) {
                foreach($users AS $user) {                    

                    $sentToUsers[] = $user->getId();
                    
                    // send notification
                    $notification = new Notification();
                    $notification->setUserFrom($mainUser);
                    $notification->setUserTo($user);
                    $notification->setKoosh($koosh);
                    $notification->setType('koosh_comment');
                    $notification->setPermanent('1');
                    $em->persist($notification);
                    $em->flush();
                    
                    
                    if($this->container->getParameter('bool_send_push_notifications')) {
                        // Send PUSH notification - IOs
                        $notificationMessage = $mainUser->getUsername() . ' commented on koosh ' . $koosh->getTitle();
                        try {
                            $iosMessage = new iOSMessage();
                            $iosMessage->setMessage($notificationMessage);
                            $iosMessage->setDeviceIdentifier($user->getPushId());

                            $this->container->get('rms_push_notifications')->send($iosMessage);
                        } catch (Exception $ex) {
                            $this->container->get('api.logger')->addError('comment_koosh - Unable to send iOS pushup notification - pushID:'.$user->getPushId()); 
                        }

                        // Send PUSH notification - Android
                        try {
                            $androidMessage = new AndroidMessage();
                            $androidMessage->setGCM(true);
                            $androidMessage->setMessage($notificationMessage);
                            $androidMessage->setDeviceIdentifier($user->getPushId());

                            $this->container->get('rms_push_notifications')->send($androidMessage);
                        } catch (Exception $ex) {
                            $this->container->get('api.logger')->addError('comment_koosh - Unable to send Android pushup notification - pushID:'.$user->getPushId()); 
                        }
                    }

                }
            }
            
            // send notifications to all participants of comment conversation
            $conversationUsers = $em->getRepository('KAPIKooshApiBundle:User')->getAllUsersFromKooshComments($kooshId);
            if(sizeof($conversationUsers)) {
                foreach($conversationUsers AS $user) {      
                    if(!in_array($user->getId(), $sentToUsers)) {
                        
                        $sentToUsers[] = $user->getId();
                        
                        // send notification
                        $notification = new Notification();
                        $notification->setUserFrom($mainUser);
                        $notification->setUserTo($user);
                        $notification->setKoosh($koosh);
                        $notification->setType('koosh_comment');
                        $notification->setPermanent('1');
                        $em->persist($notification);
                        $em->flush();


                        if($this->container->getParameter('bool_send_push_notifications')) {
                            // Send PUSH notification - IOs
                            $notificationMessage = $mainUser->getUsername() . ' commented on koosh ' . $koosh->getTitle();
                            try {
                                $iosMessage = new iOSMessage();
                                $iosMessage->setMessage($notificationMessage);
                                $iosMessage->setDeviceIdentifier($user->getPushId());

                                $this->container->get('rms_push_notifications')->send($iosMessage);
                            } catch (Exception $ex) {
                                $this->container->get('api.logger')->addError('comment_koosh - Unable to send iOS pushup notification - pushID:'.$user->getPushId()); 
                            }

                            // Send PUSH notification - Android
                            try {
                                $androidMessage = new AndroidMessage();
                                $androidMessage->setGCM(true);
                                $androidMessage->setMessage($notificationMessage);
                                $androidMessage->setDeviceIdentifier($user->getPushId());

                                $this->container->get('rms_push_notifications')->send($androidMessage);
                            } catch (Exception $ex) {
                                $this->container->get('api.logger')->addError('comment_koosh - Unable to send Android pushup notification - pushID:'.$user->getPushId()); 
                            }
                        }
                    }
                }
            }
            
            // send notifications to the koosh creator
            if(!in_array($koosh->getUser()->getId(), $sentToUsers)) {
                
                // send notification
                $notification = new Notification();
                $notification->setUserFrom($mainUser);
                $notification->setUserTo($koosh->getUser());
                $notification->setKoosh($koosh);
                $notification->setType('koosh_comment');
                $notification->setPermanent('1');
                $em->persist($notification);
                $em->flush();
                
                
                if($this->container->getParameter('bool_send_push_notifications')) {
                    // Send PUSH notification - IOs
                    $notificationMessage = $mainUser->getUsername() . ' commented on koosh ' . $koosh->getTitle();
                    try {
                        $iosMessage = new iOSMessage();
                        $iosMessage->setMessage($notificationMessage);
                        $iosMessage->setDeviceIdentifier($koosh->getUser()->getPushId());

                        $this->container->get('rms_push_notifications')->send($iosMessage);
                    } catch (Exception $ex) {
                        $this->container->get('api.logger')->addError('comment_koosh - Unable to send iOS pushup notification - pushID:'.$koosh->getUser()->getPushId()); 
                    }

                    // Send PUSH notification - Android
                    try {
                        $androidMessage = new AndroidMessage();
                        $androidMessage->setGCM(true);
                        $androidMessage->setMessage($notificationMessage);
                        $androidMessage->setDeviceIdentifier($koosh->getUser()->getPushId());

                        $this->container->get('rms_push_notifications')->send($androidMessage);
                    } catch (Exception $ex) {
                        $this->container->get('api.logger')->addError('comment_koosh - Unable to send Android pushup notification - pushID:'.$koosh->getUser()->getPushId()); 
                    }
                }
                
            }
            
            
            
            return array(
                'status' => 'ok', 
                'message' => 'Koosh has been successfully commented.', 
                'data' => array(
                    'request_id' => $kooshComment->getId()
                )
            );
            
        }
    }
}




/**
* Remove any invites from users notifications as well as removing associated videos and the koosh altogether.
*
* @ApiDoc(
* resource = true,
* description = "Remove any invites from users notifications as well as removing associated videos and the koosh altogether.",
* output = "preferred rate",
* statusCodes = {
* 200 = "Returned when successful",
* 404 = "Returned when the page is not found"
* }
* )
*
* @param \Symfony\Component\HttpFoundation\Request $request
* @param int $kooshId
* @return mixed
* @Delete("/delete_koosh/{kooshId}")
* 
* @View
*/
public function deleteKooshAction($kooshId)
{
    $baseUrl = $this->getRequest()->getSchemeAndHttpHost() . $this->container->getParameter('web_url_prefix');
    
    $em = $this->getDoctrine()->getManager();
    
    $koosh = $em->getRepository('KAPIKooshApiBundle:Koosh')->findOneBy(array('id' => $kooshId));

    if (!is_object($koosh)) {
        return array(
            'status' => 'error', 
            'message' => 'Unable to find koosh with id:' . $kooshId . '.'
        );
    } else {
        
        // send notification
        $notification = new Notification();
        $notification->setUserFrom($koosh->getUser());
        $notification->setUserTo($koosh->getUser());
        $notification->setKoosh($koosh);
        $notification->setType('koosh_deleted');
        $notification->setPermanent('1');
        $em->persist($notification);
        $em->flush();
        
        if($this->container->getParameter('bool_send_push_notifications')) {
            // Send PUSH notification - IOs
            $notificationMessage = $koosh->getUser()->getUsername() . ' has deleted the koosh ' . $koosh->getTitle();
            try {
                $iosMessage = new iOSMessage();
                $iosMessage->setMessage($notificationMessage);
                $iosMessage->setDeviceIdentifier($koosh->getUser()->getPushId());

                $this->container->get('rms_push_notifications')->send($iosMessage);
            } catch (Exception $ex) {
                $this->container->get('api.logger')->addError('delete_koosh - Unable to send iOS pushup notification - pushID:'.$koosh->getUser()->getPushId()); 
            }

            // Send PUSH notification - Android
            try {
                $androidMessage = new AndroidMessage();
                $androidMessage->setGCM(true);
                $androidMessage->setMessage($notificationMessage);
                $androidMessage->setDeviceIdentifier($koosh->getUser()->getPushId());

                $this->container->get('rms_push_notifications')->send($androidMessage);
            } catch (Exception $ex) {
                $this->container->get('api.logger')->addError('delete_koosh - Unable to send Android pushup notification - pushID:'.$koosh->getUser()->getPushId()); 
            }
        }
            
        // remove koosh
        $em->remove($koosh);
        $em->flush();
        
        
        
        return array(
            'status' => 'ok', 
            'message' => 'Koosh has been successfully deleted.'
        );
    }
}

/**
* Remove koosh comment with specific id.
*
* @ApiDoc(
* resource = true,
* description = "Remove koosh comment with specific id.",
* output = "preferred rate",
* statusCodes = {
* 200 = "Returned when successful",
* 404 = "Returned when the page is not found"
* }
* )
*
* @param \Symfony\Component\HttpFoundation\Request $request
* @param int $commentId
* @return mixed
* @Delete("/delete_comment/{commentId}")
* 
* @View
*/
public function deleteCommentAction($commentId)
{
    $baseUrl = $this->getRequest()->getSchemeAndHttpHost() . $this->container->getParameter('web_url_prefix');
    
    $em = $this->getDoctrine()->getManager();
    
    $comment = $em->getRepository('KAPIKooshApiBundle:KooshComment')->findOneBy(array('id' => $commentId));

    if (!is_object($comment)) {
        return array(
            'status' => 'error', 
            'message' => 'Unable to find comment with id:' . $commentId . '.'
        );
    } else {
        
        // remove comment
        $em->remove($comment);
        $em->flush();
        
        
        return array(
            'status' => 'ok', 
            'message' => 'Comment has been successfully deleted.'
        );
    }
}

/**
* Change specified koosh status to opposite of currently set.
*
* @ApiDoc(
* resource = true,
* description = "Change specified koosh status to opposite of currently set.",
* output = "user id",
* statusCodes = {
* 200 = "Returned when successful",
* 404 = "Returned when the page is not found"
* }
* )
*
* @param \Symfony\Component\HttpFoundation\Request $request
* @return mixed
* @Post("/change_status")
* 
* @View
*/
public function changeStatusAction(Request $request)
{
    $em = $this->getDoctrine()->getManager();
    
    // Get Params
    $userId = $request->get('user_id');
    $kooshId = $request->get('koosh_id');
    
    
    // Validation
    $errors = array();
    if(empty($userId)) $errors[] = 'Empty `user_id` param.'; 
    if(empty($kooshId)) $errors[] = 'Empty `koosh_id` param.'; 


    if(sizeof($errors)) {
        return array(
        'status' => 'error',
         'message' => 'Validation error.',
         'validation' => $errors
        );

    } else {

        $user = $em->getRepository('KAPIKooshApiBundle:User')->findOneBy(array('id' => $userId));
        $koosh = $em->getRepository('KAPIKooshApiBundle:Koosh')->findOneBy(array('id' => $kooshId));
        $kooshUserId = is_object($koosh) ? $koosh->getUserId() : 0;
        
        if (!is_object($user)) {
            return array(
                'status' => 'error', 
                'message' => 'Unable to find user with id:' . $userId . '.'
            );
        } elseif (!is_object($koosh)) {
            return array(
                'status' => 'error', 
                'message' => 'Unable to find koosh with id:' . $kooshId . '.'
            );
        } elseif ($kooshUserId != $userId) {
            return array(
                'status' => 'error', 
                'message' => 'You don\'t have permissions to update this koosh.'
            );
        } else {

            $kooshStatus = $koosh->getPublicStatus();
            if(strtolower($kooshStatus) == 'public') {
                $koosh->setPublicStatus('private');
            } else {
                $koosh->setPublicStatus('public');
            }
            $koosh->setImage(null);
            $koosh->setVideoFile(null);
            
            $em->persist($koosh);
            $em->flush();

            return array(
                'status' => 'ok',
                'message' => 'Koosh status has been successfully changed.'
            );
        }
    }
}

    /**
     * Gets a list of all enabled video transitions.
     *
     * @ApiDoc(
     * resource = true,
     * description = "Gets a list of all enabled video transitions.",
     * output = "preferred rate",
     * statusCodes = {
     * 200 = "Returned when successful",
     * 404 = "Returned when the page is not found"
     * }
     * )
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param int $userId
     * @return mixed
     * @Get("/get_transitions_list")
     *
     * @View
     */
    public function getTransitionsListAction()
    {
        $baseUrl = $this->getRequest()->getSchemeAndHttpHost() . $this->container->getParameter('web_url_prefix');

        $em = $this->getDoctrine()->getManager();

        $transitions = $em->getRepository('KAPIKooshApiBundle:Transition')->findBy(array('status' => '1'), array('title' => 'ASC'));

        $transitionsList = array();
        if(sizeof($transitions)) {
            foreach($transitions AS $key => $transition) {
                $maskImagePreviewPath = $transition->getMaskImagePreviewWebPath();
                $transitionsList[] = array(
                    'id' => $transition->getId(),
                    'code' => $transition->getCode(),
                    'title' => $transition->getTitle(),
                    'preview' => !empty($maskImagePreviewPath) ? $baseUrl . '/' . $maskImagePreviewPath : '',
                );
            }
        }

        if(!sizeof($transitionsList)) {
            return array(
                'status' => 'error',
                'message' => 'There are not any transitions available.'
            );
        } else {
            return array(
                'status' => 'ok',
                'data' => array('transitions_list' => $transitionsList)
            );
        }
    }

}
