<?php
/**
 * https://github.com/liuggio/symfony2-rest-api-the-best-2013-way/tree/master/app
 * https://github.com/nmpolo/Symfony2Rest/blob/master/src/Nmpolo/RestBundle/Controller/UserController.php
 */

namespace KAPI\KooshApiBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use Symfony\Component\Validator\Constraints\Email as EmailConstraint;


use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\Controller\Annotations\NamePrefix;
use FOS\RestBundle\Controller\Annotations\Prefix;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Patch;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Request\ParamFetcherInterface;

use Symfony\Component\Form\FormTypeInterface;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use KAPI\KooshApi\CalculatorBundle\Exception\InvalidFormException;
use KAPI\KooshApiBundle\Form\PageType;
use KAPI\KooshApiBundle\Model\ClientInterface;

use Symfony\Component\Security\Core\Util\SecureRandom;

use KAPI\KooshApiBundle\Entity\User;
use KAPI\KooshApiBundle\Entity\Video;
use KAPI\KooshApiBundle\Entity\Image;
use KAPI\KooshApiBundle\Entity\KooshItem;
use KAPI\KooshApiBundle\Entity\Friendship;
use KAPI\KooshApiBundle\Entity\Notification;
use KAPI\KooshApiBundle\Entity\PrivateMessage;

use KAPI\KooshApiBundle\Utils\LocationHelper;
use KAPI\KooshApiBundle\Utils\ArrayHelper;

use FFMpeg\Coordinate\Dimension;
use FFMpeg\Filters\Video\ResizeFilter;
use FFMpeg\Format\Video\X264;
use FFMpeg\Coordinate\FrameRate;
use FFMpeg\Coordinate\TimeCode;

use RMS\PushNotificationsBundle\Message\iOSMessage;
use RMS\PushNotificationsBundle\Message\AndroidMessage;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;


/**
 * @NamePrefix("api_rest_")
 * @Prefix("/api")
 * @RouteResource("userApi")
 * @View
 */
class UserApiController extends FOSRestController
{

/**
* Logs in the user to the app and returns the user id.
*
* @ApiDoc(
* resource = true,
* description = "Logs in the user to the app and returns the user id.",
* output = "user_id",
* statusCodes = {
* 200 = "Returned when successful",
* 404 = "Returned when the page is not found"
* }
* )
*
* @param \Symfony\Component\HttpFoundation\Request $request
* @return mixed
* @Patch("/user_login")
* 
* @View
*/
public function userLoginAction(Request $request)
{
    // Get Params
    $email = $request->get('email');
    $password = $request->get('password');
    $pushId = $request->get('push_id');
    
    // Validation
    $errors = array();
    if(empty($email)) $errors[] = 'Empty `email` param.'; 
    if(empty($password)) $errors[] = 'Empty `password` param.'; 
    
    if(sizeof($errors)) { 
       return array(
                'status' => 'error', 
                'message' => 'Validation error.', 
                'validation' => $errors
            );
       
    } else {        
        
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('KAPIKooshApiBundle:User')->findOneBy(array('email' => $email));
        
        if (!is_object($entity)) {
            return array(
                'status' => 'error', 
                'message' => 'Unable to find user with email: `' . $email . '`'
            );
        } else {
            // check password 
            $encodedPassword = sha1($password.$entity->getSalt());
            if($encodedPassword == $entity->getPassword()) {
                // Change user pushId
                if(!empty($pushId)) {
                    $entity->setPushId($pushId);
                    $entity->setImage(null);
                    $em->persist($entity);
                    $em->flush();
                }
                
                return array(
                    'status' => 'ok', 
                    //'data' => $entity
                    'data' => array('user_id' => $entity->getId())
                );
            } else {
                return array(
                    'status' => 'error', 
                    'message' => 'Wrong password.'
                );
            }
        }
    }
}
   
/**
* Logs in the user to the app using twitter or facebook and returns the user id.
*
* @ApiDoc(
* resource = true,
* description = "Logs in the user to the app using twitter or facebook and returns the user id.",
* output = "user_id",
* statusCodes = {
* 200 = "Returned when successful",
* 404 = "Returned when the page is not found"
* }
* )
*
* @param \Symfony\Component\HttpFoundation\Request $request
* @return mixed
* @Patch("/user_login_social")
* 
* @View
*/
public function userLoginSocialAction(Request $request)
{
    // Get Params
    $email = $request->get('email');
    $socialId = $request->get('social_id');
    $type = $request->get('type');
    $pushId = $request->get('push_id');
    
    // Validation
    $errors = array();
    if(empty($socialId)) $errors[] = 'Empty `social_id` param.';
    if(empty($type)) $errors[] = 'Empty `type` param.';     
    
    if(sizeof($errors)) { 
       return array(
                'status' => 'error', 
                'message' => 'Validation error.', 
                'validation' => $errors
            );
       
    } else {
        $em = $this->getDoctrine()->getManager();
        switch($type) {
            CASE 'twitter': 
                $entity = $em->getRepository('KAPIKooshApiBundle:User')->findOneBy(array('twitterId' => $socialId));
                break;
            CASE 'facebook':
                $entity = $em->getRepository('KAPIKooshApiBundle:User')->findOneBy(array('facebookId' => $socialId));
                break;
            DEFAULT:
                $entity = null;
                break;
        }

        if (!$entity) {
            return array(
                'status' => 'error', 
                'message' => 'Unable to find user with social_id: `' . $socialId . '`'
            );
        } else {
            // Change user pushId
            if(!empty($pushId)) {
                $entity->setPushId($pushId);
                $entity->setImage(null);
                $em->persist($entity);
                $em->flush();
            }
            
            return array(
                'status' => 'ok', 
                'data' => array('user_id' => $entity->getId())
            );
        }
    }
}


/**
* Requests a password reset.
*
* @ApiDoc(
* resource = true,
* description = "Requests a password reset.",
* output = "user id",
* statusCodes = {
* 200 = "Returned when successful",
* 404 = "Returned when the page is not found"
* }
* )
*
* @param \Symfony\Component\HttpFoundation\Request $request
* @return mixed
* @Patch("/forgotten_password")
* 
* @View
*/
public function forgottenPasswordAction(Request $request)
{
    // Get Params
    $email = $request->get('email');
    
    // Validation
    $errors = array();
    if(empty($email)) $errors[] = 'Empty `email` param.'; 
    
    if(sizeof($errors)) {
        return array(
        'status' => 'error',
         'message' => 'Validation error.',
         'validation' => $errors
        );

    } else {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('KAPIKooshApiBundle:User')->findOneBy(array('email' => $email));

        if (!is_object($entity)) {
            return array(
                'status' => 'error', 
                'message' => 'Unable to find user with email: `' . $email . '`'
            );
        } else {
            // Generate and persist new password
            $generator = new SecureRandom();
            $randomPassword = bin2hex($generator->nextBytes(8)); 
            
            $entity->setPassword($randomPassword);
            $entity->setImage(null);
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            
            // Email 
            $message = \Swift_Message::newInstance()
                ->setSubject('Forgotten Password')
                ->setFrom($this->container->getParameter('system_email_from'))
                ->setTo($email)
                ->setContentType("text/html")
                ->setBody(
                    $this->renderView(
                        'KAPIKooshApiBundle:UserApi:forgottenPasswordEmail.txt.twig',
                        array(
                            'email' => $email, 
                            'password' => $randomPassword, 
                        )
                    )
                );

            try {
                //var_dump($this->container->getParameter('system_email_from')); die();
                $this->get('mailer')->send($message);
                
                return array(
                    'status' => 'ok', 
                    'message' => 'Email has been successfully sent.'
                );
            } catch (Exception $e) {
                return array(
                    'status' => 'error', 
                    'message' => 'Unable to send email - error: ' . $e->getMessage()
                );
            }
        }
    }
}

/**
* Creates an account using the information below - email and user_name needs to be unique so check for that..
*
* @ApiDoc(
* resource = true,
* description = "Creates an account using the information below - email and user_name needs to be unique so check for that..",
* output = "",
* statusCodes = {
* 200 = "Returned when successful",
* 404 = "Returned when the page is not found"
* }
* )
*
* @param \Symfony\Component\HttpFoundation\Request $request
* @return mixed
* @Post("/create_account")
* 
* @View
*/
public function createAccountAction(Request $request) {

    // Get Params
    $email = urldecode($request->get('email'));
    $firstName = urldecode($request->get('first_name'));
    $lastName = urldecode($request->get('last_name'));
    $userName = urldecode($request->get('user_name'));
    $dob = urldecode($request->get('dob'));
    $password = urldecode($request->get('password'));
    $imageData = urldecode($request->get('image_data'));
    $pushId = urldecode($request->get('push_id'));
    $facebookId = urldecode($request->get('facebook_id'));
    $twitterId = urldecode($request->get('twitter_id'));
    
    $imageUploadedFile = $request->files->get(str_replace('.', '_', $imageData));
    
    
    $this->container->get('api.logger')->addInfo('IMAGE DATA ATTR: ' . $imageData); 
    $this->container->get('api.logger')->addInfo('POST FILES:', array('object' => $request->files)); 
    $this->container->get('api.logger')->addInfo('FOUND FILE: .', array('object' => $imageUploadedFile)); 
    
    // Validation
    $errors = array();
    if(empty($email)) $errors[] = 'Empty `email` param.'; 
    if(empty($firstName)) $errors[] = 'Empty `first_name` param.'; 
    if(empty($lastName)) $errors[] = 'Empty `last_name` param.';
    if(empty($userName)) $errors[] = 'Empty `user_name` param.'; 
    if(empty($dob)) $errors[] = 'Empty `dob` param.'; 
    if(empty($password)) $errors[] = 'Empty `password` param.'; 
    if(empty($imageData)) $errors[] = 'Empty `image_data` param.'; 
    if(!is_object($imageUploadedFile)) $errors[] = 'Image could not be uploaded.'; 
    
    
    // Unique email and username validation
    $em = $this->getDoctrine()->getManager();
    $entity = $em->getRepository('KAPIKooshApiBundle:User')->findOneBy(array('email' => $email));
    if(is_object($entity)) $errors[] = 'Your `email` has been already used.';
    
    $entity = $em->getRepository('KAPIKooshApiBundle:User')->findOneBy(array('username' => $userName));
    if(is_object($entity)) $errors[] = 'Your `user_name` has been already used.';
    
    // Convert DOB
    $formatedDob = \DateTime::createFromFormat('d/m/Y', $dob);
    if(!$formatedDob) $errors[] = 'Wrong `dob` format. (correct format: d/m/Y)';
    
    if(sizeof($errors)) { 
       return array(
                'status' => 'error', 
                'message' => 'Validation error.', 
                'validation' => $errors
            );
       
    } else {
        
        // Persist User
        $user = new User();
        $user->setEmail($email);
        $user->setTwitterId($twitterId);
        $user->setFacebookId($facebookId);
        $user->setFirstName($firstName);
        $user->setLastName($lastName);
        $user->setUserName($userName);
        $user->setDob($formatedDob);
        $user->setPassword($password);
        $user->setImage($imageUploadedFile);
        $user->setPushId($pushId);
        
        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();
        
        // Email
        $message = \Swift_Message::newInstance()
            ->setSubject('Contact Request')
            ->setFrom($this->container->getParameter('system_email_from'))
            ->setTo($this->container->getParameter('system_email_to'))
            ->setContentType("text/html")
            ->setBody(
                $this->renderView(
                    'KAPIKooshApiBundle:UserApi:newAccountEmail.txt.twig',
                    array(
                        'email' => $email, 
                        'firstName' => $firstName, 
                        'lastName' => $lastName, 
                        'userName' => $userName, 
                        'dob' => $dob, 
                        'password' => $password, 
                        'pushId' => $pushId, 
                        'twitterId' => $twitterId, 
                        'facebookId' => $facebookId, 
                        'imagePath' => $user->getWebPath(), 
                    )
                )
            );
        
        try {
            $this->get('mailer')->send($message);
            
            return array(
                'status' => 'ok', 
                'message' => 'Email has been successfully sent.', 
                'data' => array('user_id' => $user->getId())
            );
        } catch (Exception $e) {
            return array(
                'status' => 'ok', 
                'message' => 'Unable to send email - error: ' . $e->getMessage(), 
                'data' => array('user_id' => $user->getId())
            );
        }
    }
}

/**
* Gets a list of the users outstanding notifications.
*
* @ApiDoc(
* resource = true,
* description = "Gets a list of the users outstanding notifications.",
* output = "preferred rate",
* statusCodes = {
* 200 = "Returned when successful",
* 404 = "Returned when the page is not found"
* }
* )
*
* @param \Symfony\Component\HttpFoundation\Request $request
* @param int $userId
* @return mixed
* @Get("/get_notifications/{userId}")
* 
* @View
*/
public function getNotificationsAction($userId)
{
    $baseUrl = $this->getRequest()->getSchemeAndHttpHost() . $this->container->getParameter('web_url_prefix');
    
    $em = $this->getDoctrine()->getManager();
    $user = $em->getRepository('KAPIKooshApiBundle:User')->findOneBy(array('id' => $userId));
    
    if(!is_object($user)) {
        return array(
            'status' => 'error', 
            'message' => 'There is no user with id:' . $userId . '.'
        );
    } else {
        $notifications = $em->getRepository('KAPIKooshApiBundle:Notification')->findBy(array('userIdTo' => $userId));

        $userNotifications = array();
        if(sizeof($notifications)) {
            foreach($notifications AS $key => $notification) {
                
                $diff = date_diff(new \DateTime(), $notification->getCreated());
                if((($notification->getPermanent() != '1') || (($notification->getPermanent() == '1') && ($diff->format('%a') < 1) )) && (($notification->getType() != 'koosh_invite') || ($notification->getKoosh()->getDisableNotifications() != '1'))) {
                    // Get Image
                    $imagePath = '';
                    if($notification->getType() == 'koosh_friend') {
                        $imagePath = $this->get('kapi_helper')->getProfileThumbnailURL($notification->getUserFrom(), true);
                    } elseif($notification->getType() == 'koosh_view') {
                        $imagePath = $notification->getKoosh()->getImageWebPath();
                        $imagePath = !empty($imagePath) ? $baseUrl . '/' . $imagePath : '';
                    }

                    $userNotifications[] = array(
                        'image_url' => !empty($imagePath) ? $imagePath : '', 
                        'koosh_title' => is_object($notification->getKoosh()) ? $notification->getKoosh()->getTitle() : '', 
                        'user_name' => $notification->getUserFrom()->getUsername(), 
                        'type' => $notification->getType(),
                        'length' => ((($notification->getType() == 'koosh_invite') && is_object($notification->getKoosh())) ? $notification->getKoosh()->getLength() : ''), 
                        'note_message' => ((($notification->getType() == 'koosh_invite') && is_object($notification->getKoosh())) ? $notification->getKoosh()->getNotificationMessage() : ''),
                        'koosh_id' => (($notification->getType() != 'koosh_friend') ? $notification->getKooshId() : ''), 
                        'koosh_type' => ((($notification->getType() != 'koosh_friend') && is_object($notification->getKoosh())) ? $notification->getKoosh()->getType() : ''), 
                        'request_id' => (($notification->getType() == 'koosh_friend') ? $notification->getRequestId() : ''), 
                        'amount' => (($notification->getType() == 'koosh_invite') ? $notification->getAmount() : ''),
                        'notification_id' => $notification->getId(), 
                    );
                }
            }
        }

        if (!sizeof($userNotifications)) {
            return array(
                'status' => 'error', 
                'message' => 'There are not any notifications for this user.'
            );
        } else {
            return array(
                'status' => 'ok', 
                'data' => array('user_notifications' => $userNotifications)
            );
        }
    }
}


/**
* Accepts the invite to the koosh and upload the video.
*
* @ApiDoc(
* resource = true,
* description = "Accepts the invite to the koosh and upload the video.",
* output = "user id",
* statusCodes = {
* 200 = "Returned when successful",
* 404 = "Returned when the page is not found"
* }
* )
*
* @param \Symfony\Component\HttpFoundation\Request $request
* @return mixed
* @Post("/accept_koosh_invite")
* 
* @View
*/
public function acceptKooshInviteAction(Request $request)
{
    $em = $this->getDoctrine()->getManager();
    
    // Get Params
    $userId = $request->get('user_id');
    $kooshId = $request->get('koosh_id');
    $addAudio = $request->get('add_audio');
    $videoData = $request->get('video_data');
    $imageData = $request->get('image_data');
    $audioData = $request->get('audio_data');
    $sourceType = $request->get('source_type');
    $systemAudioId = $request->get('system_audio_id');
    $kooshMe = $request->get('koosh_me');
    
    $disableNotifications = (!empty($kooshMe) && ($kooshMe == '1')) ? true : false;
    
    $videoUploadedFile = $request->files->get(str_replace('.', '_', $videoData));
    $imageUploadedFile = $request->files->get(str_replace('.', '_', $imageData));
    $audioUploadedFile = $request->files->get(str_replace('.', '_', $audioData));
    
    $systemAudio = $em->getRepository('KAPIKooshApiBundle:SystemAudio')->findOneBy(array('id' => $systemAudioId));
    
    $this->container->get('api.logger')->addInfo('IMAGE DATA ATTR: ' . $imageData); 
    $this->container->get('api.logger')->addInfo('POST FILES:', array('object' => $request->files)); 
    $this->container->get('api.logger')->addInfo('POST FILES:', array('object' => $request)); 
    $this->container->get('api.logger')->addInfo('FOUND FILE: .', array('object' => $imageUploadedFile)); 
    
    // Validation
    $errors = array();
    if(empty($userId)) $errors[] = 'Empty `user_id` param.'; 
    if(empty($kooshId)) $errors[] = 'Empty `koosh_id` param.'; 
    
    $koosh = $em->getRepository('KAPIKooshApiBundle:Koosh')->findOneBy(array('id' => $kooshId));

    if(is_object($koosh)) {
        if($koosh->getType() == 'koosh_mix') {
            if(empty($sourceType)) $errors[] = 'Empty `source_type` param.';
            elseif(($sourceType != 'image') && ($sourceType != 'video')) $errors[] = 'Wrong `source_type` value.';
        }
        
        if(($koosh->getType() == 'koosh_kapture') || (($koosh->getType() == 'koosh_mix') && ($sourceType == 'image'))) {
            if(empty($imageData)) $errors[] = 'Empty `image_data` param.'; 
        } elseif(($koosh->getType() == 'koosh_video') || (($koosh->getType() == 'koosh_mix') && ($sourceType == 'video'))) {
            if(empty($videoData)) $errors[] = 'Empty `video_data` param.'; 
        } 
        if(!empty($addAudio) && empty($audioData) && empty($systemAudioId)) {
            $errors[] = 'Empty both `audio_data` and `system_audio_id` param.';     
        }
        if(!empty($addAudio) && !empty($systemAudioId) && !is_object($systemAudio)) {
            $errors[] = 'Unable to find system audio track with id:'.$systemAudioId.'.';
        }
        if(!isset($request->files) || !sizeof($request->files)) {
            if(empty($videoData)) $errors[] = 'Empty POST FILES data.'; 
        }
    } else {
        $errors[] = 'Unable to find koosh with `koosh_id`.'; 
    }

    // Video file format validation
    if(is_object($videoUploadedFile)) {
        $extension = strtolower($videoUploadedFile->guessExtension());
        if($extension != 'mp4') $errors[] = 'Video should be in ´.mp4´ format.';
    }
    
    // Audio file format validation
    if(is_object($audioUploadedFile)) {
        $extension = strtolower($audioUploadedFile->guessExtension());
        if(($extension != 'mp3') && ($extension != 'mpga')) $errors[] = 'Audio should be in ´.mp3´ format.';
    }
    
    if(sizeof($errors)) {
        return array(
        'status' => 'error',
         'message' => 'Validation error.',
         'validation' => $errors
        );

    } else {
        $user = $em->getRepository('KAPIKooshApiBundle:User')->findOneBy(array('id' => $userId));
        
        $notification = $em->getRepository('KAPIKooshApiBundle:Notification')->findOneBy(array('userIdTo' => $userId, 'kooshId' => $kooshId, 'type' => 'koosh_invite'));
        
        if (!is_object($notification)) {
            return array(
                'status' => 'error', 
                'message' => 'Unable to find koosh invite notification for user_id: `' . $userId . '` and koosh_id: `' . $kooshId . '`'
            );
        } else {
            
            $video = null;
            $image = null;
            
            if(($koosh->getType() == 'koosh_kapture') || (($koosh->getType() == 'koosh_mix') && ($sourceType == 'image'))) {

                    $images = $em->getRepository('KAPIKooshApiBundle:Image')->findBy(array('userId' => $userId, 'kooshId' => $kooshId));
                
                    $image = new Image();
                    $image->setUser($user);
                    $image->setKoosh($koosh);
                    $image->setTitle($user->getUsername().' - image ' . sizeof($images));
                    $image->setImageFile($imageUploadedFile);
                    $image->setStatus('1');
                    $image->setSystemAudio($systemAudio);
                    
                    $em->persist($image);
                    $em->flush();
                    
                    
                    if(empty($sourceType)) {
                        $sourceType = 'image';
                    }
                
                
            } elseif(($koosh->getType() == 'koosh_video') || (($koosh->getType() == 'koosh_mix') && ($sourceType == 'video'))) {
                
                    $videos = $em->getRepository('KAPIKooshApiBundle:Video')->findBy(array('userId' => $userId, 'kooshId' => $kooshId));
                
                    $video = new Video();
                    $video->setUser($user);
                    $video->setKoosh($koosh);
                    $video->setTitle($user->getUsername().' - video ' . sizeof($videos));
                    $video->setVideoFile($videoUploadedFile);
                    $video->setStatus('1');
                    $video->setAddAudio($addAudio);
                    $video->setSystemAudio($systemAudio);
                   
                    // Store audio
                    if(!empty($addAudio)) {
                        $video->setAudioFile($audioUploadedFile);
                    }
                    
                    $em->persist($video);
                    $em->flush();

                    
                    if(empty($sourceType)) {
                        $sourceType = 'video';
                    }
                    
                    // Get video info
                    $videoInfo = array('duration' => '0');
                    try {
                        $ffprobe = $this->container->get('dubture_ffmpeg.ffprobe');
                        $streams = $ffprobe->streams($video->getVideoFileAbsolutePath());

                        foreach($streams AS $stream) {
                            if($stream->get('codec_type') == 'video') {
                               $videoInfo['nb_frames'] = $stream->get('nb_frames');
                               $videoInfo['duration'] = $stream->get('duration');
                               $videoInfo['width'] = $stream->get('width');
                            }
                        }
                    } catch (Exception $e) {
                        $this->container->get('api.logger')->addError('FFPROBE get video stream info error: '.$e->getMessage());  
                    }
                    
                    // Store final video freezframe
                    $ffmpeg = $this->container->get('dubture_ffmpeg.ffmpeg');
                    $freezeImageName = sha1(uniqid(mt_rand(), true)) . '.jpg';
                    $freezeImageFilePath = $video->getAbsolutePath() . $freezeImageName;
                    try {
                        $videoF = $ffmpeg->open($video->getVideoFileAbsolutePath());
                        $videoF->frame(TimeCode::fromSeconds($videoInfo['duration']>5 ? 5 : 1))
                               ->save($freezeImageFilePath);
                    } catch (Exception $e) {
                        $this->container->get('api.logger')->addError('FFMPEG get video freeze image error: '.$e->getMessage());  
                    }    
                    $video->setImageFilePath($freezeImageName);
                    $video->setVideoFile(null);
                    $video->setAudioFile(null);
                    $em->persist($video);
                    $em->flush();
                   

            } 
            
            $kooshItem = new KooshItem();
            $kooshItem->setKoosh($koosh);
            $kooshItem->setUser($user);
            $kooshItem->setVideo($video);
            $kooshItem->setImage($image);
            $kooshItem->setSourceType($sourceType);
            $em->persist($kooshItem);
            $em->flush();
            
            if($this->container->getParameter('bool_send_push_notifications') && !$disableNotifications) {
                // Send PUSH notification - IOs
                $notificationMessage = $user->getUsername() . ' has accepted your your koosh ' . $koosh->getTitle() . ' invitation.';
                try {
                    $iosMessage = new iOSMessage();
                    $iosMessage->setMessage($notificationMessage);
                    $iosMessage->setDeviceIdentifier($koosh->getUser()->getPushId());

                    $this->container->get('rms_push_notifications')->send($iosMessage);
                } catch (Exception $ex) {
                    $this->container->get('api.logger')->addError('accept_koosh - Unable to send iOS pushup notification - pushID:'.$koosh->getUser()->getPushId()); 
                }

                // Send PUSH notification - Android
                try {
                    $androidMessage = new AndroidMessage();
                    $androidMessage->setGCM(true);
                    $androidMessage->setMessage($notificationMessage);
                    $androidMessage->setDeviceIdentifier($koosh->getUser()->getPushId());

                    $this->container->get('rms_push_notifications')->send($androidMessage);
                } catch (Exception $ex) {
                    $this->container->get('api.logger')->addError('accept_koosh - Unable to send Android pushup notification - pushID:'.$koosh->getUser()->getPushId()); 
                }
            }
            
            
            return array(
                'status' => 'ok', 
                'message' => 'Email has been successfully sent.', 
                'data' => array('item_id' => $kooshItem->getId())
            );
        }
    }
}

/**
* Decline the invite to the koosh.
*
* @ApiDoc(
* resource = true,
* description = "Decline the invite to the koosh.",
* output = "user id",
* statusCodes = {
* 200 = "Returned when successful",
* 404 = "Returned when the page is not found"
* }
* )
*
* @param \Symfony\Component\HttpFoundation\Request $request
* @return mixed
* @Patch("/decline_koosh_invite")
* 
* @View
*/
public function declineKooshInviteAction(Request $request)
{
    // Get Params
    $userId = $request->get('user_id');
    $kooshId = $request->get('koosh_id');
    
    // Validation
    $errors = array();
    if(empty($userId)) $errors[] = 'Empty `user_id` param.'; 
    if(empty($kooshId)) $errors[] = 'Empty `koosh_id` param.'; 
    
    if(sizeof($errors)) {
        return array(
        'status' => 'error',
         'message' => 'Validation error.',
         'validation' => $errors
        );

    } else {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('KAPIKooshApiBundle:User')->findOneBy(array('id' => $userId));
        
        $koosh = $em->getRepository('KAPIKooshApiBundle:Koosh')->findOneBy(array('id' => $kooshId));
        
        $foundNotification = $em->getRepository('KAPIKooshApiBundle:Notification')->findOneBy(array('userIdTo' => $userId, 'kooshId' => $kooshId, 'type' => 'koosh_invite'));
        
        if (!is_object($user)) {
            return array(
                'status' => 'error', 
                'message' => 'Unable to find user with id: `' . $userId . '`'
            );
        } elseif (!is_object($koosh)) {
            return array(
                'status' => 'error', 
                'message' => 'Unable to find koosh with id: `' . $kooshId . '`'
            );
        } elseif (!is_object($foundNotification)) {
            return array(
                'status' => 'error', 
                'message' => 'Unable to find invite notification for koosh id: `' . $kooshId . '` and user id: `' . $userId . '`'
            );
        } else {
            
            // Delete inivite notification ???
            if(is_object($foundNotification)) {
                $em->remove($foundNotification);
                $em->flush();
            }
            
             // If there are no more invite notifications -> change koosh status
            $inviteNotifications = $em->getRepository('KAPIKooshApiBundle:Notification')->findBy(array('kooshId' => $kooshId, 'type' => 'koosh_invite'));
            if(empty($inviteNotifications)) {
                $koosh->setStatus('1'); // set COMPLETE state
                $em->persist($koosh);
                $em->flush();
            }
            
            // send notification 
            $notification = new Notification();
            $notification->setUserFrom($user);
            $notification->setUserTo($koosh->getUser());
            $notification->setKoosh($koosh);
            $notification->setType('koosh_declined');
            $notification->setPermanent('1');
            $em->persist($notification);
            $em->flush();

            
            if($this->container->getParameter('bool_send_push_notifications')) {
                // Send PUSH notification - IOs
                $notificationMessage = $user->getUsername() . ' has declined the koosh ' . $koosh->getTitle() . ' invitation.';
                try {
                    $iosMessage = new iOSMessage();
                    $iosMessage->setMessage($notificationMessage);
                    $iosMessage->setDeviceIdentifier($koosh->getUser()->getPushId());

                    $this->container->get('rms_push_notifications')->send($iosMessage);
                } catch (Exception $ex) {
                    $this->container->get('api.logger')->addError('decline_koosh - Unable to send iOS pushup notification - pushID:'.$koosh->getUser()->getPushId()); 
                }

                // Send PUSH notification - Android
                try {
                    $androidMessage = new AndroidMessage();
                    $androidMessage->setGCM(true);
                    $androidMessage->setMessage($notificationMessage);
                    $androidMessage->setDeviceIdentifier($koosh->getUser()->getPushId());

                    $this->container->get('rms_push_notifications')->send($androidMessage);
                } catch (Exception $ex) {
                    $this->container->get('api.logger')->addError('decline_koosh - Unable to send Android pushup notification - pushID:'.$koosh->getUser()->getPushId()); 
                }
            }
            
            return array(
                'status' => 'ok', 
                'message' => 'Koosh invitation has been successfully declined.', 
                'data' => array('notification_id' => $foundNotification->getId())
            );
        }
    }
}

/**
* Mark koosh inivtation for specific user as completed
*
* @ApiDoc(
* resource = true,
* description = "Mark koosh inivtation for specific user as completed.",
* output = "user id",
* statusCodes = {
* 200 = "Returned when successful",
* 404 = "Returned when the page is not found"
* }
* )
*
* @param \Symfony\Component\HttpFoundation\Request $request
* @return mixed
* @Patch("/user_finished_submitting")
* 
* @View
*/
public function userFinishedSubmittingAction(Request $request)
{
    // Get Params
    $userId = $request->get('user_id');
    $kooshId = $request->get('koosh_id');
    
    // Validation
    $errors = array();
    if(empty($userId)) $errors[] = 'Empty `user_id` param.'; 
    if(empty($kooshId)) $errors[] = 'Empty `koosh_id` param.'; 
    
    if(sizeof($errors)) {
        return array(
        'status' => 'error',
         'message' => 'Validation error.',
         'validation' => $errors
        );

    } else {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('KAPIKooshApiBundle:User')->findOneBy(array('id' => $userId));
        
        $koosh = $em->getRepository('KAPIKooshApiBundle:Koosh')->findOneBy(array('id' => $kooshId));
        
        $foundNotification = $em->getRepository('KAPIKooshApiBundle:Notification')->findOneBy(array('userIdTo' => $userId, 'kooshId' => $kooshId, 'type' => 'koosh_invite'));
        
        if (!is_object($user)) {
            return array(
                'status' => 'error', 
                'message' => 'Unable to find user with id: `' . $userId . '`'
            );
        } elseif (!is_object($koosh)) {
            return array(
                'status' => 'error', 
                'message' => 'Unable to find koosh with id: `' . $kooshId . '`'
            );
        } elseif (!is_object($foundNotification)) {
            return array(
                'status' => 'error', 
                'message' => 'Unable to find invite notification for koosh id: `' . $kooshId . '` and user id: `' . $userId . '`'
            );
        } else {
            
            // Delete inivite notification ???
            if(is_object($foundNotification)) {
                $em->remove($foundNotification);
                $em->flush(); 
            }
            
            // If there are no more invite notifications -> change koosh status
            $inviteNotifications = $em->getRepository('KAPIKooshApiBundle:Notification')->findBy(array('kooshId' => $kooshId, 'type' => 'koosh_invite'));
            if(empty($inviteNotifications)) {
                $koosh->setStatus('1'); // set COMPLETE state
                $em->persist($koosh);
                $em->flush();
            }
            
            return array(
                'status' => 'ok', 
                'message' => 'Koosh invitation has been successfully submitted.', 
                'data' => array('notification_id' => $foundNotification->getId())
            );
        }
    }
}

/**
* Respond to the friend invite.
*
* @ApiDoc(
* resource = true,
* description = "Respond to the friend invite.",
* output = "user id",
* statusCodes = {
* 200 = "Returned when successful",
* 404 = "Returned when the page is not found"
* }
* )
*
* @param \Symfony\Component\HttpFoundation\Request $request
* @return mixed
* @Patch("/add_friend")
* 
* @View
*/
public function addFriendAction(Request $request)
{
    $em = $this->getDoctrine()->getManager();
    
    // Get Params
    $userId = $request->get('user_id');
    $emailAddress = urldecode($request->get('email_address'));
    $userIdInviting = $request->get('user_id_inviting');
    
    // Validation
    $errors = array();
    if(empty($userIdInviting) && empty($emailAddress)) $errors[] = 'Empty both `user_id_inviting` and `email_address` param.'; 
    if(empty($userId)) $errors[] = 'Empty `user_id` param.';  
    
    // Existing users
    $userFrom = $em->getRepository('KAPIKooshApiBundle:User')->findOneBy(array('id' => $userId));
    if(!is_object($userFrom)) $errors[] = 'Unable to find user with id:' . $userId . '.';
    
    if(!empty($userIdInviting)) {
        $userTo = $em->getRepository('KAPIKooshApiBundle:User')->findOneBy(array('id' => $userIdInviting));
        if(!is_object($userTo)) $errors[] = 'Unable to find user with id:' . $userIdInviting . '.';
    } else {
        $userTo = $em->getRepository('KAPIKooshApiBundle:User')->findOneBy(array('email' => $emailAddress));
        //if(!is_object($userTo)) $errors[] = 'Unable to find user with email address:' . $emailAddress . '.';
    }
    
    
    // Existing Friendship Validation
    if(is_object($userTo)) {
        if($em->getRepository('KAPIKooshApiBundle:Friendship')->checkExistingFriendship($userTo->getId(), $userId)) $errors[] = 'These users are already in friendship.'; 
    }
    
    if(sizeof($errors)) {
        return array(
        'status' => 'error',
         'message' => 'Validation error.',
         'validation' => $errors
        );

    } else {
            
        if(is_object($userTo)) { // invite EXISTING USER and send PUSH notifiaction
            // create friendship
            $friendshipRequest = new Friendship();
            $friendshipRequest->setUserFrom($userFrom);
            $friendshipRequest->setUserTo($userTo);
            $friendshipRequest->setStatus('0');

            $em->persist($friendshipRequest);
            $em->flush();

            // Add notification
            $notification = new Notification();
            $notification->setRequestId($friendshipRequest->getId());
            $notification->setUserFrom($userFrom);
            $notification->setUserTo($userTo);
            $notification->setType('koosh_friend');

            $em->persist($notification);
            $em->flush();

            if($this->container->getParameter('bool_send_push_notifications')) {
                // Send PUSH notification - IOs
                $notificationMessage = $userFrom->getUsername() . ' wants to be friends';
                try {
                    $iosMessage = new iOSMessage();
                    $iosMessage->setMessage($notificationMessage);
                    $iosMessage->setDeviceIdentifier($userTo->getPushId());

                    $this->container->get('rms_push_notifications')->send($iosMessage);
                } catch (Exception $ex) {
                    $this->container->get('api.logger')->addError('add_friend - Unable to send iOS pushup notification - pushID:'.$userTo->getPushId()); 
                }

                // Send PUSH notification - Android
                try {
                    $androidMessage = new AndroidMessage();
                    $androidMessage->setGCM(true);
                    $androidMessage->setMessage($notificationMessage);
                    $androidMessage->setDeviceIdentifier($userTo->getPushId());

                    $this->container->get('rms_push_notifications')->send($androidMessage);
                } catch (Exception $ex) {
                    $this->container->get('api.logger')->addError('add_friend - Unable to send Android pushup notification - pushID:'.$userTo->getPushId()); 
                }
            }
            
            $sendTo = $userTo->getEmail();
            
            return array(
                'status' => 'ok', 
                'message' => 'Friendship request has been successfully stored.'
            );
        } else { // only send email to NON EXISTING user email
            $sendTo = $emailAddress;
            
            // Send info mail
            $message = \Swift_Message::newInstance()
                ->setSubject('Friendship request')
                ->setFrom($this->container->getParameter('system_email_from'))
                ->setTo($sendTo)
                ->setContentType("text/html")
                ->setBody(
                    $this->renderView(
                        'KAPIKooshApiBundle:UserApi:friendshipRequestEmail.txt.twig',
                        array(
                            'email' => $userFrom->getEmail(), 
                            'name' => $userFrom->getFirstName() . ' ' . $userFrom->getLastName(), 
                            'username' => $userFrom->getUsername(), 
                        )
                    )
                );

            try {
                $this->get('mailer')->send($message);

                return array(
                    'status' => 'ok', 
                    'message' => 'Friendship request has been successfully stored. Info email has been successfully sent.'
                );
            } catch (Exception $e) {
                return array(
                    'status' => 'ok', 
                    'message' => 'Friendship request has been successfully stored. Unable to send email - error: ' . $e->getMessage()
                );
            }
        }
    }
}

/**
* Respond to the friend invite.
*
* @ApiDoc(
* resource = true,
* description = "Respond to the friend invite.",
* output = "user id",
* statusCodes = {
* 200 = "Returned when successful",
* 404 = "Returned when the page is not found"
* }
* )
*
* @param \Symfony\Component\HttpFoundation\Request $request
* @return mixed
* @Patch("/friend_request_response")
* 
* @View
*/
public function friendRequestResponseAction(Request $request)
{
    $em = $this->getDoctrine()->getManager();
    
    // Get Params
    $requestId = $request->get('request_id');
    $userId = $request->get('user_id');
    $response = urldecode($request->get('response'));
    
    // Validation
    $errors = array();
    if(empty($requestId)) $errors[] = 'Empty `request_id` param.'; 
    if(empty($userId)) $errors[] = 'Empty `user_id` param.'; 
    if(empty($response)) $errors[] = 'Empty `response` param.';  
    if(($response != 'accept') && ($response != 'reject')) $errors[] = 'Wrong `response` param. (allowed values are: accept | reject)';  
    
    
    if(sizeof($errors)) {
        return array(
        'status' => 'error',
         'message' => 'Validation error.',
         'validation' => $errors
        );

    } else {
            
        $friendshipRequest = $em->getRepository('KAPIKooshApiBundle:Friendship')->findOneBy(array('id' => $requestId));

        if (!is_object($friendshipRequest)) {
            return array(
                'status' => 'error', 
                'message' => 'Unable to find request with id:' . $requestId . '.'
            );
        } else {
            
            // Delete notification
            $notification = $em->getRepository('KAPIKooshApiBundle:Notification')->findOneBy(array('userIdTo' => $friendshipRequest->getUserIdTo(), 'userIdFrom' => $friendshipRequest->getUserIdFrom(), 'type' => 'koosh_friend'));
            if(is_object($notification)) {
                $em->remove($notification);
                $em->flush();
            }
            
            if($response == 'accept') {
                // Accept friendship request
                $friendshipRequest->setStatus('1');
                $em->persist($friendshipRequest);
                $em->flush();
                

                if($this->container->getParameter('bool_send_push_notifications')) {
                    // Send PUSH notification - IOs
                    $notificationMessage = $friendshipRequest->getUserTo()->getUsername() . ' accepted your friendship invitation.';
                    try {
                        $iosMessage = new iOSMessage();
                        $iosMessage->setMessage($notificationMessage);
                        $iosMessage->setDeviceIdentifier($friendshipRequest->getUserFrom()->getPushId());

                        $this->container->get('rms_push_notifications')->send($iosMessage);
                    } catch (Exception $ex) {
                        $this->container->get('api.logger')->addError('add_friend - Unable to send iOS pushup notification - pushID:'.$friendshipRequest->getUserFrom()->getPushId()); 
                    }

                    // Send PUSH notification - Android
                    try {
                        $androidMessage = new AndroidMessage();
                        $androidMessage->setGCM(true);
                        $androidMessage->setMessage($notificationMessage);
                        $androidMessage->setDeviceIdentifier($friendshipRequest->getUserFrom()->getPushId());

                        $this->container->get('rms_push_notifications')->send($androidMessage);
                    } catch (Exception $ex) {
                        $this->container->get('api.logger')->addError('add_friend - Unable to send Android pushup notification - pushID:'.$friendshipRequest->getUserFrom()->getPushId()); 
                    }
                }
                
                return array(
                    'status' => 'ok', 
                    'message' => 'Friendship has been successfully accepted.'
                );
            } else {
                // Reject friendship request
                $friendshipRequest->setStatus('2');
                $em->persist($friendshipRequest);
                $em->flush();
                
                return array(
                    'status' => 'ok', 
                    'message' => 'Friendship has been successfully rejected.'
                );
            }
        }
    }
}

/**
* Gets details of the user.
*
* @ApiDoc(
* resource = true,
* description = "Gets details of the user.",
* output = "preferred rate",
* statusCodes = {
* 200 = "Returned when successful",
* 404 = "Returned when the page is not found"
* }
* )
*
* @param \Symfony\Component\HttpFoundation\Request $request
* @param int $userId
* @return mixed
* @Patch("/get_user_info")
* 
* @View
*/
public function getUserInfoAction(Request $request)
{
    $baseUrl = $this->getRequest()->getSchemeAndHttpHost() . $this->container->getParameter('web_url_prefix');
    $em = $this->getDoctrine()->getManager();
    
    // Get Params
    $userId = $request->get('user_id');
    $infoUserId = $request->get('info_user_id');
    
    // Validation
    $errors = array();
    if(empty($infoUserId)) $errors[] = 'Empty `info_user_id` param.'; 
    if(empty($userId)) $errors[] = 'Empty `user_id` param.'; 
    
    if(sizeof($errors)) {
        return array(
        'status' => 'error',
         'message' => 'Validation error.',
         'validation' => $errors
        );

    } else {
    
        $user = $em->getRepository('KAPIKooshApiBundle:User')->findOneBy(array('id' => $infoUserId));
    
        if(!is_object($user)) {
            return array(
                'status' => 'error', 
                'message' => 'Unable to find user with id:' . $infoUserId . '.'
            );
        } else {
            $kooshes = $em->getRepository('KAPIKooshApiBundle:Koosh')->findBy(array('userId' => $infoUserId, 'status' => '3'), array('created' => 'DESC'));

            $totalUserLikes = 0;

            $kooshList = array();
            $kooshKey = 0;
            if(sizeof($kooshes)) {
                foreach($kooshes AS $key => $koosh) {
                    
                    // Check friendship
                    if(strtolower($koosh->getPublicStatus()) == 'private') {
                        if($userId == $koosh->getUser()->getId()) {
                            $checkInvolved = true;
                        } else {
                            //$checkFriendship = $em->getRepository('KAPIKooshApiBundle:Friendship')->checkExistingFriendship($userId, $koosh->getUser()->getId());
                            $checkInvolved = $em->getRepository('KAPIKooshApiBundle:User')->checkInvolvedByKoosh($userId, $koosh->getId(), $koosh->getType());
                        }
                    }

                    if((strtolower($koosh->getPublicStatus()) == 'public') || ((strtolower($koosh->getPublicStatus()) == 'private') && ($checkInvolved))) {
                        
                        $kooshImageWebPath = $this->get('kapi_helper')->getProfileThumbnailURL($koosh, true);
                        $imagePath = $this->get('kapi_helper')->getProfileThumbnailURL($user, true);
                        
                        $kooshList[$kooshKey] = array(
                            'koosh_title' => $koosh->getTitle(), 
                            'koosh_likes' => sizeof($koosh->getKooshLikes()), 
                            'koosh_comments' => sizeof($koosh->getComments()),
                            'koosh_id' => $koosh->getId(), 
                            'user_id' => $koosh->getUserId(), 
                            'koosh_public_status' => $koosh->getPublicStatus(), 
                            'image_url' =>  !empty($kooshImageWebPath) ? $kooshImageWebPath : '',
                            'user_image' => !empty($imagePath) ? $imagePath : '', 
                            'fame_value' => $koosh->getUser()->getFameValue()
                        );
                        $kooshKey++;
                    }
                    
                    $totalUserLikes += sizeof($koosh->getKooshLikes());
                }
            }

            // Get User Friends
            $friends = $em->getRepository('KAPIKooshApiBundle:Friendship')->getUserFriends($userId);

            // Check Friendship
            if($userId == $infoUserId) {
                $hasFriendship = true;
            } else {
                $hasFriendship = $em->getRepository('KAPIKooshApiBundle:Friendship')->checkExistingFriendship($userId, $infoUserId);
            }
            
            
            $userInfo = array(
                'user_name' => $user->getUsername(), 
                'user_image' => $this->get('kapi_helper')->getProfileThumbnailURL($user, true),
                'likes' => $totalUserLikes,
                'friends' => sizeof($friends), 
                'koosh_list' => $kooshList, 
                'friend_bool' => $hasFriendship, 
                'fame_value' => $user->getFameValue()
            ); 

            return array(
                'status' => 'ok', 
                'data' => $userInfo
            );
        }
    }
}

/**
* Gets account details of the user.
*
* @ApiDoc(
* resource = true,
* description = "Gets account details of the user.",
* output = "preferred rate",
* statusCodes = {
* 200 = "Returned when successful",
* 404 = "Returned when the page is not found"
* }
* )
*
* @param \Symfony\Component\HttpFoundation\Request $request
* @param int $userId
* @return mixed
* @Get("/get_user_account_info/{userId}")
* 
* @View
*/
public function getUserAccountInfoAction($userId)
{
    $baseUrl = $this->getRequest()->getSchemeAndHttpHost() . $this->container->getParameter('web_url_prefix');
    
    $em = $this->getDoctrine()->getManager();
    $user = $em->getRepository('KAPIKooshApiBundle:User')->findOneBy(array('id' => $userId));
    
    if(!is_object($user)) {
        return array(
            'status' => 'error', 
            'message' => 'Unable to find user with id:' . $userId . '.'
        );
    } else {
        
        $userInfo = array(
            'first_name' => $user->getFirstName(), 
            'last_name' => $user->getLastName(), 
            'user_image' => $this->get('kapi_helper')->getProfileThumbnailURL($user, true),
            'email' => $user->getEmail(), 
            'dob' => $user->getDob()->format('d/m/Y'), 
            'user_name' => $user->getUsername() 
        ); 
        
        return array(
            'status' => 'ok', 
            'data' => $userInfo
        );
    }
}

/**
* Change the information linked to the account.
*
* @ApiDoc(
* resource = true,
* description = "Change the information linked to the account.",
* output = "user id",
* statusCodes = {
* 200 = "Returned when successful",
* 404 = "Returned when the page is not found"
* }
* )
*
* @param \Symfony\Component\HttpFoundation\Request $request
* @return mixed
* @Post("/change_user_info")
* 
* @View
*/
public function changeUserInfoAction(Request $request)
{
    $em = $this->getDoctrine()->getManager();
    
    // Get Params
    $userId = $request->get('user_id');
    $email = urldecode($request->get('email'));
    $userName = urldecode($request->get('user_name'));
    $firstName = urldecode($request->get('first_name'));
    $lastName = urldecode($request->get('last_name'));
    $dob = urldecode($request->get('dob'));
    $password = urldecode($request->get('password'));
    $newPassword = urldecode($request->get('new_password'));
    $facebookId = urldecode($request->get('facebook_id'));
    $twitterId = urldecode($request->get('twitter_id'));
    $imageData = urldecode($request->get('image_data'));
    $pushId = urldecode($request->get('push_id'));
    
    $user = $em->getRepository('KAPIKooshApiBundle:User')->findOneBy(array('id' => $userId));

    if (!is_object($user)) {
        return array(
            'status' => 'error', 
            'message' => 'Unable to find user with id:' . $userId . '.'
        );
    } else {
    
        // Validation
        $errors = array();
        if(empty($userId)) $errors[] = 'Empty `user_id` param.'; 
        
        // Valid email address
        if(!empty($email)) {
            $emailConstraint = new EmailConstraint();
            $emailConstraint->message = '';
            $validatorErrors = $this->get('validator')->validateValue($email, $emailConstraint);
            if(sizeof($validatorErrors)) $errors[] = 'Wrong `email` format.';
            
            // Unique email validation
            $entity = $em->getRepository('KAPIKooshApiBundle:User')->findOneBy(array('email' => $email));
            if(is_object($entity) && ($email != $user->getEmail())) $errors[] = '`email` has been already used.';
        }
        
        // Valid userName 
        if(!empty($email)) {    
            $entity = $em->getRepository('KAPIKooshApiBundle:User')->findOneBy(array('username' => $userName));
            if(is_object($entity) && ($userName != $user->getUsername())) $errors[] = '`user_name` has been already used.';
        }

        // Convert DOB
        if(!empty($dob)) {
            $formatedDob = \DateTime::createFromFormat('d/m/Y', $dob);
            if(!$formatedDob) $errors[] = 'Wrong `dob` format. (correct format: d/m/Y)';
        }

        // Old password validation
        if(!empty($newPassword)) {
            $encodedPassword = sha1($password . $user->getSalt()); 
            if($user->getPassword() != $encodedPassword) $errors[] = 'Wrong old `password` value.';
        }

        // Image
        if(!empty($imageData)) {
            $imageUploadedFile = $request->files->get(str_replace('.', '_', $imageData));
        }
        
        if(sizeof($errors)) {
            return array(
            'status' => 'error',
             'message' => 'Validation error.',
             'validation' => $errors
            );

        } else {

            // Update user info if doesn't empty
            if(!empty($email)) {
                $user->setEmail($email);
            }
            if(!empty($userName)) {
                $user->setUsername($userName);
            }
            if(!empty($firstName)) {
                $user->setFirstName($firstName);
            }
            if(!empty($lastName)) {
                $user->setLastName($lastName);
            }
            if(!empty($dob)) {
                $user->setDob($formatedDob);
            }
            if(!empty($newPassword)) {
                $user->setPassword($newPassword);
            }
            if(!empty($facebookId)) {
                $user->setFacebookId($facebookId);
            }
            if(!empty($twitterId)) {
                $user->setTwitterId($twitterId);
            }
            if(!empty($pushId)) {
                $user->setPushId($pushId);
            }
            if(!empty($imageData)) {
                $user->setImage($imageUploadedFile);
            } else {
                $user->setImage(null);
            }
            
            $em->persist($user);
            $em->flush();
            
            return array(
                'status' => 'ok',
                'message' => 'User info has been successfully updated.'
            );
        }
    }
}

/**
* Change specified user longitude and latitude.
*
* @ApiDoc(
* resource = true,
* description = "Change specified user longitued and latitude.",
* output = "user id",
* statusCodes = {
* 200 = "Returned when successful",
* 404 = "Returned when the page is not found"
* }
* )
*
* @param \Symfony\Component\HttpFoundation\Request $request
* @return mixed
* @Post("/update_user_location")
* 
* @View
*/
public function updateUserLocationAction(Request $request)
{
    $em = $this->getDoctrine()->getManager();
    
    // Get Params
    $userId = $request->get('user_id');
    $latitude = $request->get('latitude');
    $longitude = $request->get('longitude');
    
    
    // Validation
    $errors = array();
    if(empty($userId)) $errors[] = 'Empty `user_id` param.'; 
    if(empty($latitude)) $errors[] = 'Empty `latitude` param.'; 
    if(empty($longitude)) $errors[] = 'Empty `longitude` param.'; 


    if(sizeof($errors)) {
        return array(
        'status' => 'error',
         'message' => 'Validation error.',
         'validation' => $errors
        );

    } else {

        $user = $em->getRepository('KAPIKooshApiBundle:User')->findOneBy(array('id' => $userId));

        if (!is_object($user)) {
            return array(
                'status' => 'error', 
                'message' => 'Unable to find user with id:' . $userId . '.'
            );
        } else {

            // Update user info if doesn't empty
            if(!empty($latitude)) {
                $user->setLatitude($latitude);
            }
            if(!empty($longitude)) {
                $user->setLongitude($longitude);
            }

            $user->setImage(null);


            $em->persist($user);
            $em->flush();

            return array(
                'status' => 'ok',
                'message' => 'User location has been successfully updated.'
            );
        }
    }
}

/**
* Searches users that are within a mile of the given location.
*
* @ApiDoc(
* resource = true,
* description = "Searches users that are within a mile of the given location.",
* output = "preferred rate",
* statusCodes = {
* 200 = "Returned when successful",
* 404 = "Returned when the page is not found"
* }
* )
*
* @param \Symfony\Component\HttpFoundation\Request $request
* @param int $userId
* @return mixed
* @Patch("/my_local_users")
* 
* @View
*/
public function myLocalUsersAction(Request $request)
{
    $baseUrl = $this->getRequest()->getSchemeAndHttpHost() . $this->container->getParameter('web_url_prefix');
    
    $em = $this->getDoctrine()->getManager();
    
    // Get Params
    $userId = $request->get('user_id');
    $latitude = $request->get('latitude');
    $longitude = $request->get('longitude');
    
    // Validations
    $errors = array();
    if(empty($userId)) $errors[] = 'Empty `user_id` param.'; 
    if(empty($latitude)) $errors[] = 'Empty `latitude` param.'; 
    if(empty($longitude)) $errors[] = 'Empty `longitude` param.'; 
    
    if(sizeof($errors)) { 
       return array(
                'status' => 'error', 
                'message' => 'Validation error.', 
                'validation' => $errors
            );
       
    } else {
    
        $mainUser = $em->getRepository('KAPIKooshApiBundle:User')->findOneBy(array('id' => $userId));

        if (!is_object($mainUser)) {
            return array(
                'status' => 'error', 
                'message' => 'Unable to find user with id:' . $userId . '.'
            );
        } else {


            $kooshUsersList = array();

            // return also current user data
            if(is_object($mainUser)) {
                $distance = 0;

                $userImageWebPath = $this->get('kapi_helper')->getProfileThumbnailURL($mainUser, true);

                // Get all user kooshes likes
                $kooshesLikes = 0;
                if(sizeof($mainUser->getKooshes())) {
                    foreach($mainUser->getKooshes() AS $koosh) {
                        $kooshesLikes += sizeof($koosh->getKooshLikes());
                    }
                }

                // Get user friends count
                $users = $em->getRepository('KAPIKooshApiBundle:Friendship')->getUserFriends($mainUser->getId());

                // Get kooshes
                $kooshes = $em->getRepository('KAPIKooshApiBundle:Koosh')->findBy(array('userId' => $mainUser->getId(), 'status' => '3', 'publicStatus' => 'public'));

                $kooshUsersList[] = array(
                    'user_id' => $mainUser->getId(), 
                    'friend_number' => sizeof($users), 
                    'user_name' => $mainUser->getUsername(), 
                    'koosh_likes' => $kooshesLikes, 
                    'koosh_number' => sizeof($kooshes), 
                    'user_image' => !empty($userImageWebPath) ? $userImageWebPath : '',
                    'distance_from' => LocationHelper::convertMilesToMeters($distance), 
                );
            }
                
            $users = $em->getRepository('KAPIKooshApiBundle:User')->findAll();
            if(sizeof($users)) {
                foreach($users AS $key => $user) {
                    
                    // Get distance in miles
                    $distance = LocationHelper::calculateDistance($latitude, $longitude, $user->getLatitude(), $user->getLongitude());
                    
                    $userLatitude = $user->getLatitude();
                    $userLongitude = $user->getLongitude();
                    if(!empty($distance) && ($distance <= 1) && !empty($userLatitude) && !empty($userLongitude)) {
                        $userImageWebPath = $this->get('kapi_helper')->getProfileThumbnailURL($user, true);

                        // Get all user kooshes likes
                        $kooshesLikes = 0;
                        if(sizeof($user->getKooshes())) {
                            foreach($user->getKooshes() AS $koosh) {
                                $kooshesLikes += sizeof($koosh->getKooshLikes());
                            }
                        }

                        // Get user friends count
                        $users = $em->getRepository('KAPIKooshApiBundle:Friendship')->getUserFriends($user->getId());

                        // Get kooshes
                        $kooshes = $em->getRepository('KAPIKooshApiBundle:Koosh')->findBy(array('userId' => $user->getId(), 'status' => '3', 'publicStatus' => 'public'));

                        $kooshUsersList[] = array(
                            'user_id' => $user->getId(), 
                            'friend_number' => sizeof($users), 
                            'user_name' => $user->getUsername(), 
                            'koosh_likes' => $kooshesLikes, 
                            'koosh_number' => sizeof($kooshes), 
                            'user_image' => !empty($userImageWebPath) ? $userImageWebPath : '',
                            'distance_from' => ceil(LocationHelper::convertMilesToMeters($distance)), 
                        );
                    }
                }
            }
            
            return array(
                'status' => 'ok', 
                'data' => array('koosh_users_list' => $kooshUsersList)
            );
        }
    }
}

/**
* Gets a list of the users friends.
*
* @ApiDoc(
* resource = true,
* description = "Gets a list of the users friends.",
* output = "preferred rate",
* statusCodes = {
* 200 = "Returned when successful",
* 404 = "Returned when the page is not found"
* }
* )
*
* @param \Symfony\Component\HttpFoundation\Request $request
* @param int $userId
* @return mixed
* @Get("/my_koosh_contacts/{userId}")
* 
* @View
*/
public function myKooshContactsAction($userId)
{
    $baseUrl = $this->getRequest()->getSchemeAndHttpHost() . $this->container->getParameter('web_url_prefix');
    
    $em = $this->getDoctrine()->getManager();
    
    $user = $em->getRepository('KAPIKooshApiBundle:User')->findOneBy(array('id' => $userId));

    if (!is_object($user)) {
        return array(
            'status' => 'error', 
            'message' => 'Unable to find user with id:' . $userId . '.'
        );
    } else {
    
        $friends = $em->getRepository('KAPIKooshApiBundle:Friendship')->getUserFriendsObjects($userId);

        if (!sizeof($friends)) {
            return array(
                'status' => 'error', 
                'message' => 'There are no user friends in the system.'
            );
        } else {
            
            $kooshContactList = array();
            foreach($friends AS $key => $friend) {
                $userImageWebPath = $this->get('kapi_helper')->getProfileThumbnailURL($friend, true);

                // Get all user kooshes likes
                $kooshesLikes = 0;
                if(sizeof($friend->getKooshes())) {
                    foreach($friend->getKooshes() AS $koosh) {
                        $kooshesLikes += sizeof($koosh->getKooshLikes());
                    }
                }
                
                // Get user friends count
                $friends = $em->getRepository('KAPIKooshApiBundle:Friendship')->getUserFriends($friend->getId());
                
                // Get kooshes
                $kooshes = $em->getRepository('KAPIKooshApiBundle:Koosh')->findBy(array('userId' => $friend->getId(), 'status' => '3', 'publicStatus' => 'public'));
                
                $kooshContactList[$key] = array(
                    'user_id' => $friend->getId(), 
                    'friend_number' => sizeof($friends), 
                    'user_name' => $friend->getUsername(), 
                    'koosh_likes' => $kooshesLikes, 
                    'koosh_number' => sizeof($kooshes), 
                    'user_image' => !empty($userImageWebPath) ? $userImageWebPath : '',
                );
            }
            
            ArrayHelper::sortBySubKey($kooshContactList, 'user_name', true);
            
            return array(
                'status' => 'ok', 
                'data' => array('koosh_contact_list' => $kooshContactList)
            );
        }
    }
}

/**
* Gets a list of the users friends plus user data.
*
* @ApiDoc(
* resource = true,
* description = "Gets a list of the users friends plus user data.",
* output = "preferred rate",
* statusCodes = {
* 200 = "Returned when successful",
* 404 = "Returned when the page is not found"
* }
* )
*
* @param \Symfony\Component\HttpFoundation\Request $request
* @param int $userId
* @return mixed
* @Get("/my_invite_contacts/{userId}")
* 
* @View
*/
public function myInviteContactsAction($userId)
{
    $baseUrl = $this->getRequest()->getSchemeAndHttpHost() . $this->container->getParameter('web_url_prefix');

    $em = $this->getDoctrine()->getManager();

    $user = $em->getRepository('KAPIKooshApiBundle:User')->findOneBy(array('id' => $userId));

    if (!is_object($user)) {
        return array(
            'status' => 'error',
            'message' => 'Unable to find user with id:' . $userId . '.'
        );
    } else {

        $kooshContactList = array();


        $friends = $em->getRepository('KAPIKooshApiBundle:Friendship')->getUserFriendsObjects($userId);
        if(sizeof($friends)) {
            foreach($friends AS $key => $friend) {
                $userImageWebPath = $this->get('kapi_helper')->getProfileThumbnailURL($friend, true);

                // Get all user kooshes likes
                $kooshesLikes = 0;
                if(sizeof($friend->getKooshes())) {
                    foreach($friend->getKooshes() AS $koosh) {
                        $kooshesLikes += sizeof($koosh->getKooshLikes());
                    }
                }

                // Get user friends count
                $friends = $em->getRepository('KAPIKooshApiBundle:Friendship')->getUserFriends($friend->getId());

                // Get kooshes
                $kooshes = $em->getRepository('KAPIKooshApiBundle:Koosh')->findBy(array('userId' => $friend->getId(), 'status' => '3', 'publicStatus' => 'public'));

                $kooshContactList[] = array(
                    'user_id' => $friend->getId(),
                    'friend_number' => sizeof($friends),
                    'user_name' => $friend->getUsername(),
                    'koosh_likes' => $kooshesLikes,
                    'koosh_number' => sizeof($kooshes),
                    'user_image' => !empty($userImageWebPath) ? $userImageWebPath : '',
                );
            }
        }

        if(sizeof($kooshContactList)) {
            ArrayHelper::sortBySubKey($kooshContactList, 'user_name', true);
        }

        // ADD request user info to the first place
        if(is_object($user)) {
            $userImageWebPath = $this->get('kapi_helper')->getProfileThumbnailURL($user, true);

            // Get all user kooshes likes
            $kooshesLikes = 0;
            if(sizeof($user->getKooshes())) {
                foreach($user->getKooshes() AS $koosh) {
                    $kooshesLikes += sizeof($koosh->getKooshLikes());
                }
            }

            // Get user friends count
            $friends = $em->getRepository('KAPIKooshApiBundle:Friendship')->getUserFriends($user->getId());

            // Get kooshes
            $kooshes = $em->getRepository('KAPIKooshApiBundle:Koosh')->findBy(array('userId' => $user->getId(), 'status' => '3', 'publicStatus' => 'public'));

            $kooshContactListElement = array(
                'user_id' => $user->getId(),
                'friend_number' => sizeof($friends),
                'user_name' => $user->getUsername(),
                'koosh_likes' => $kooshesLikes,
                'koosh_number' => sizeof($kooshes),
                'user_image' => !empty($userImageWebPath) ? $userImageWebPath : '',
            );
            if(sizeof($kooshContactList)) {
                array_unshift($kooshContactList, $kooshContactListElement);
            } else {
                $kooshContactList[] = $kooshContactListElement;
            }
        }


        return array(
            'status' => 'ok',
            'data' => array('koosh_contact_list' => $kooshContactList)
        );

    }
}

/**
* Searches all koosh users with searchstring.
*
* @ApiDoc(
* resource = true,
* description = "Searches all koosh users with searchstring.",
* output = "preferred rate",
* statusCodes = {
* 200 = "Returned when successful",
* 404 = "Returned when the page is not found"
* }
* )
*
* @param \Symfony\Component\HttpFoundation\Request $request
* @param int $userId
* @return mixed
* @Patch("/search_koosh_contacts")
* 
* @View
*/
public function searchKooshContactsAction(Request $request)
{
    $baseUrl = $this->getRequest()->getSchemeAndHttpHost() . $this->container->getParameter('web_url_prefix');
    
    $em = $this->getDoctrine()->getManager();
    
    // Get Params
    $userId = $request->get('user_id');
    $searchString = $request->get('search_string');
    
    // Replace "%20" character 
    $searchString = str_replace('%20', ' ', $searchString);
    
    // Validations
    $errors = array();
    if(empty($userId)) $errors[] = 'Empty `user_id` param.'; 
    if(empty($searchString)) $errors[] = 'Empty `search_string` param.'; 
    
    if(sizeof($errors)) { 
       return array(
                'status' => 'error', 
                'message' => 'Validation error.', 
                'validation' => $errors
            );
       
    } else {
    
        $user = $em->getRepository('KAPIKooshApiBundle:User')->findOneBy(array('id' => $userId));

        if (!is_object($user)) {
            return array(
                'status' => 'error', 
                'message' => 'Unable to find user with id:' . $userId . '.'
            );
        } else {

            $users = $em->getRepository('KAPIKooshApiBundle:User')->findAllBySearchString($searchString, $userId);

            if (!sizeof($users)) {
                return array(
                    'status' => 'error', 
                    'message' => 'There are no results in the system.'
                );
            } else {

                $kooshContactList = array();
                foreach($users AS $key => $user) {
                    $userImageWebPath = $this->get('kapi_helper')->getProfileThumbnailURL($user, true);

                    // Check Friendship
                    $hasFriendship = $em->getRepository('KAPIKooshApiBundle:Friendship')->checkExistingFriendship($userId, $user->getId());
                    
                    // Get all user kooshes likes
                    $kooshesLikes = 0;
                    if(sizeof($user->getKooshes())) {
                        foreach($user->getKooshes() AS $koosh) {
                            $kooshesLikes += sizeof($koosh->getKooshLikes());
                        }
                    }
                    
                    // Get user friends count
                    $friends = $em->getRepository('KAPIKooshApiBundle:Friendship')->getUserFriends($user->getId());
                    
                    // Get kooshes
                    $kooshes = $em->getRepository('KAPIKooshApiBundle:Koosh')->findBy(array('userId' => $user->getId(), 'status' => '3'));

                    
                    $kooshContactList[$key] = array(
                        'user_id' => $user->getId(), 
                        'friend_number' => sizeof($friends), 
                        'user_name' => $user->getUsername(), 
                        'koosh_likes' => $kooshesLikes, 
                        'koosh_number' => sizeof($kooshes),
                        'user_image' => !empty($userImageWebPath) ? $userImageWebPath : '',
                        'friend_bool' => $hasFriendship
                    );
                }
               
                ArrayHelper::sortBySubKey($kooshContactList, 'user_name', true);
                
                return array(
                    'status' => 'ok', 
                    'data' => array('koosh_contact_list' => $kooshContactList)
                );
            }
        }
    }
}

/**
* Sends a list of phone contacts email addresses and returns if they have koosh, are friends or neither.
*
* @ApiDoc(
* resource = true,
* description = "Sends a list of phone contacts email addresses and returns if they have koosh, are friends or neither.",
* output = "preferred rate",
* statusCodes = {
* 200 = "Returned when successful",
* 404 = "Returned when the page is not found"
* }
* )
*
* @param \Symfony\Component\HttpFoundation\Request $request
* @param int $userId
* @return mixed
* @Patch("/find_contacts")
* 
* @View
*/
public function findContactsAction(Request $request)
{
    $baseUrl = $this->getRequest()->getSchemeAndHttpHost() . $this->container->getParameter('web_url_prefix');
    
    $em = $this->getDoctrine()->getManager();
    
    // Get Params
    $userId = $request->get('user_id');
    $emailArray = $request->get('email_array');
    
    // Validations
    $errors = array();
    if(empty($userId)) $errors[] = 'Empty `user_id` param.'; 
    if(empty($emailArray)) $errors[] = 'Empty `email_array` param.'; 
    
    if(sizeof($errors)) { 
       return array(
                'status' => 'error', 
                'message' => 'Validation error.', 
                'validation' => $errors
            );
       
    } else {
    
        $user = $em->getRepository('KAPIKooshApiBundle:User')->findOneBy(array('id' => $userId));

        if (!is_object($user)) {
            return array(
                'status' => 'error', 
                'message' => 'Unable to find user with id:' . $userId . '.'
            );
        } else {
            if (!sizeof($emailArray)) {
                return array(
                    'status' => 'error', 
                    'message' => 'Empty email_array.'
                );
            } else {
                $emailContacts = json_decode($emailArray, true);
                $contacts = array();
                foreach($emailContacts['contacts'] AS $contact) {
                    /**
                     * status: none / has_koosh / friends
                     */
                    $status = 'none'; 

                    // Check registered
                    $foundUser = $em->getRepository('KAPIKooshApiBundle:User')->findOneBy(array('email' => $contact['email']));
                    if($foundUser) {
                        $status = 'has_koosh';
                        
                        // Check for friendship
                        $friendship = $em->getRepository('KAPIKooshApiBundle:Friendship')->checkExistingFriendship($userId, $foundUser->getId());
                        if($friendship) {
                            $status = 'friends';
                        }
                        
                        // Get all user kooshes likes
                        $kooshesLikes = 0;
                        if(sizeof($foundUser->getKooshes())) {
                            foreach($foundUser->getKooshes() AS $koosh) {
                                $kooshesLikes += sizeof($koosh->getKooshLikes());
                            }
                        }

                        // Get user friends count
                        $friends = $em->getRepository('KAPIKooshApiBundle:Friendship')->getUserFriends($foundUser->getId());
                        
                        $userImageWebPath = $this->get('kapi_helper')->getProfileThumbnailURL($user, true);
                        
                        // Get kooshes
                        $kooshes = $em->getRepository('KAPIKooshApiBundle:Koosh')->findBy(array('userId' => $foundUser->getId(), 'status' => '3'));
                       
                        
                        $contacts[] = array(
                            'user_name' => $foundUser->getFirstName() . ' ' . $foundUser->getLastName(), 
                            'user_id' => $foundUser->getId(), 
                            'friend_number' => sizeof($friends), 
                            'friend_bool' => ($friendship) ? '1' : '0', 
                            'koosh_likes' => $kooshesLikes, 
                            'koosh_number' => sizeof($kooshes),
                            'user_image' => !empty($userImageWebPath) ? $userImageWebPath : '', 
                            'email_address' => $contact['email'],
                            'status' => $status
                        );
                        
                    } else {
                        $contacts[] = array(
                            'email_address' => $contact['email'],
                            'status' => $status
                        );
                    }
                    
                    
                }
                
                
                return array(
                    'status' => 'ok', 
                    'data' => array('find_friend_reply' => $contacts)
                );
            }
        }
    }
}

/**
* Remove friendship.
*
* @ApiDoc(
* resource = true,
* description = "Remove friendship.",
* output = "preferred rate",
* statusCodes = {
* 200 = "Returned when successful",
* 404 = "Returned when the page is not found"
* }
* )
*
* @param \Symfony\Component\HttpFoundation\Request $request
* @param int $kooshId
* @return mixed
* @Patch("/remove_friend")
* 
* @View
*/
public function removeFriendAction(Request $request)
{
    $baseUrl = $this->getRequest()->getSchemeAndHttpHost() . $this->container->getParameter('web_url_prefix');
    
    $em = $this->getDoctrine()->getManager();
    
    // Get Params
    $userId = $request->get('user_id');
    $friendId = $request->get('friend_id');
    
    // Validations
    $errors = array();
    if(empty($userId)) $errors[] = 'Empty `user_id` param.'; 
    if(empty($friendId)) $errors[] = 'Empty `friend_id` param.'; 
    
    if(sizeof($errors)) { 
       return array(
                'status' => 'error', 
                'message' => 'Validation error.', 
                'validation' => $errors
            );
       
    } else {
    
        $friendship1 = $em->getRepository('KAPIKooshApiBundle:Friendship')->findOneBy(array('userIdFrom' => $userId, 'userIdTo' => $friendId));
        if(!is_object($friendship1)) {
            $friendship2 = $em->getRepository('KAPIKooshApiBundle:Friendship')->findOneBy(array('userIdFrom' => $friendId, 'userIdTo' => $userId));
            if(!is_object($friendship2)) {
                return array(
                    'status' => 'error', 
                    'message' => 'Unable to find friend connection.'
                );
            } else {
                $em->remove($friendship2);
                $em->flush();
                
                return array(
                    'status' => 'ok', 
                    'message' => 'Friend connection has been successfully deleted.'
                );
            }
        } else {
            $em->remove($friendship1);
            $em->flush();

            return array(
                'status' => 'ok', 
                'message' => 'Friend connection has been successfully deleted.'
            );
        }
    }
}

/**
* Remove notification from the system.
*
* @ApiDoc(
* resource = true,
* description = "Remove notification from the system.",
* output = "preferred rate",
* statusCodes = {
* 200 = "Returned when successful",
* 404 = "Returned when the page is not found"
* }
* )
*
* @param \Symfony\Component\HttpFoundation\Request $request
* @param int $notificationId
* @return mixed
* @Delete("/clear_notification/{notificationId}")
* 
* @View
*/
public function clearNotificationAction($notificationId)
{
    $baseUrl = $this->getRequest()->getSchemeAndHttpHost() . $this->container->getParameter('web_url_prefix');
    
    $em = $this->getDoctrine()->getManager();
    
    $notification = $em->getRepository('KAPIKooshApiBundle:Notification')->findOneBy(array('id' => $notificationId));

    if (!is_object($notification)) {
        return array(
            'status' => 'error', 
            'message' => 'Unable to find notification with id:' . $notificationId . '.'
        );
    } else {
        
        // remove koosh
        $em->remove($notification);
        $em->flush();
        
        
        return array(
            'status' => 'ok', 
            'message' => 'Notification has been successfully removed.'
        );
    }
}

/**
* Remove specific user account with all dependencies.
*
* @ApiDoc(
* resource = true,
* description = "Remove specific user account with all dependencies.",
* output = "preferred rate",
* statusCodes = {
* 200 = "Returned when successful",
* 404 = "Returned when the page is not found"
* }
* )
*
* @param \Symfony\Component\HttpFoundation\Request $request
* @param int $userId
* @return mixed
* @Delete("/delete_account/{userId}")
* 
* @View
*/
public function deleteAccountAction($userId)
{
    $baseUrl = $this->getRequest()->getSchemeAndHttpHost() . $this->container->getParameter('web_url_prefix');
    
    $em = $this->getDoctrine()->getManager();
    
    $user = $em->getRepository('KAPIKooshApiBundle:User')->findOneBy(array('id' => $userId));

    if (!is_object($user)) {
        return array(
            'status' => 'error', 
            'message' => 'Unable to find user with id:' . $userId . '.'
        );
    } else {
        
        // remove account
        $em->remove($user);
        $em->flush();
       
        
        return array(
            'status' => 'ok', 
            'message' => 'Account has been successfully deleted.'
        );
    }
}



/**
* Sends the private message
*
* @ApiDoc(
* resource = true,
* description = "Sends the private message",
* output = "",
* statusCodes = {
* 200 = "Returned when successful",
* 404 = "Returned when the page is not found"
* }
* )
*
* @param \Symfony\Component\HttpFoundation\Request $request
* @return mixed
* @Post("/send_private_message")
* 
* @View
*/
public function sendPrivateMessageAction(Request $request) {

    // Get Params
    $userId = $request->get('user_id');
    $userIdRecipient = $request->get('user_id_recipient');
    $message = urldecode($request->get('message'));
    
    $em = $this->getDoctrine()->getManager();
    
    $userFrom = $em->getRepository('KAPIKooshApiBundle:User')->findOneBy(array('id' => $userId));
    $userTo = $em->getRepository('KAPIKooshApiBundle:User')->findOneBy(array('id' => $userIdRecipient));

    
    // Validation
    $errors = array();
    if(empty($userId)) $errors[] = 'Empty `user_id` param.'; 
    if(empty($userIdRecipient)) $errors[] = 'Empty `user_id_recipient` param.'; 
    if(empty($message)) $errors[] = 'Empty `message` param.';
    if(!is_object($userFrom)) $errors[] = 'Unable to find user with id:' . $userId . '.';
    if(!is_object($userTo)) $errors[] = 'Unable to find user with id:' . $userIdRecipient . '.';
   
    
    if(sizeof($errors)) { 
       return array(
                'status' => 'error', 
                'message' => 'Validation error.', 
                'validation' => $errors
            );
       
    } else {
        
        // Persist Message
        $privateMessage = new PrivateMessage();
        $privateMessage->setUserFrom($userFrom);
        $privateMessage->setUserTo($userTo);
        $privateMessage->setMessage($message);
        
        $em = $this->getDoctrine()->getManager();
        $em->persist($privateMessage);
        $em->flush();
        
        
        // Send notification 
        $notification = new Notification();
        $notification->setUserFrom($userFrom);
        $notification->setUserTo($userTo); 
        $notification->setType('unread_messages');
        $notification->setPermanent('1');
        $em->persist($notification);
        $em->flush();
        
        if($this->container->getParameter('bool_send_push_notifications')) {
            // Send PUSH notification - IOs
            $notificationMessage = $userFrom->getUsername() . ' sent you a private message.';
            try {
                $iosMessage = new iOSMessage();
                $iosMessage->setMessage($notificationMessage);
                $iosMessage->setDeviceIdentifier($userTo->getPushId());

                $this->container->get('rms_push_notifications')->send($iosMessage);
            } catch (Exception $ex) {
                $this->container->get('api.logger')->addError('send_private_message - Unable to send iOS pushup notification - pushID:'.$userTo->getPushId()); 
            }

            // Send PUSH notification - Android
            try {
                $androidMessage = new AndroidMessage();
                $androidMessage->setGCM(true);
                $androidMessage->setMessage($notificationMessage);
                $androidMessage->setDeviceIdentifier($userTo->getPushId());

                $this->container->get('rms_push_notifications')->send($androidMessage);
            } catch (Exception $ex) {
                $this->container->get('api.logger')->addError('send_private_message - Unable to send Android pushup notification - pushID:'.$userTo->getPushId()); 
            }
        }
            
        return array(
            'status' => 'ok', 
            'message' => 'Private message has been successfully sent.', 
            'data' => array(
                'message_id' => $privateMessage->getId()
            )
        );
    }
}

/**
* Remove specific private message.
*
* @ApiDoc(
* resource = true,
* description = "Remove specific private message.",
* output = "preferred rate",
* statusCodes = {
* 200 = "Returned when successful",
* 404 = "Returned when the page is not found"
* }
* )
*
* @param \Symfony\Component\HttpFoundation\Request $request
* @param int $messageId
* @return mixed
* @Delete("/delete_private_message/{messageId}")
* 
* @View
*/
public function deletePrivateMessageAction($messageId)
{
    $baseUrl = $this->getRequest()->getSchemeAndHttpHost() . $this->container->getParameter('web_url_prefix');
    
    $em = $this->getDoctrine()->getManager();
    
    $privateMessage = $em->getRepository('KAPIKooshApiBundle:PrivateMessage')->findOneBy(array('id' => $messageId));

    if (!is_object($privateMessage)) {
        return array(
            'status' => 'error', 
            'message' => 'Unable to find private message with id:' . $messageId . '.'
        );
    } else {
        
        // remove account
        $em->remove($privateMessage);
        $em->flush();
       
        
        return array(
            'status' => 'ok', 
            'message' => 'Private message has been successfully deleted.'
        );
    }
}

/**
* Remove all messages between two specific users.
*
* @ApiDoc(
* resource = true,
* description = "Remove all messages between two specific users.",
* output = "preferred rate",
* statusCodes = {
* 200 = "Returned when successful",
* 404 = "Returned when the page is not found"
* }
* )
*
* @param \Symfony\Component\HttpFoundation\Request $request
* @return mixed
* @Patch("/remove_private_messages_conversation")
* 
* @View
*/
public function removePrivateMessagesConversationAction(Request $request)
{
    $baseUrl = $this->getRequest()->getSchemeAndHttpHost() . $this->container->getParameter('web_url_prefix');
    $em = $this->getDoctrine()->getManager();
    
    // Get Params
    $userId = $request->get('user_id');
    $userIdRecipient = $request->get('user_id_recipient');
    
    $userFrom = $em->getRepository('KAPIKooshApiBundle:User')->findOneBy(array('id' => $userId));
    $userTo = $em->getRepository('KAPIKooshApiBundle:User')->findOneBy(array('id' => $userIdRecipient));

    // Validation
    $errors = array();
    if(empty($userId)) $errors[] = 'Empty `user_id` param.'; 
    if(empty($userIdRecipient)) $errors[] = 'Empty `user_id_recipient` param.'; 
    if(!is_object($userTo)) $errors[] = 'Unable to find user with id:' . $userIdRecipient . '.';
    if(!is_object($userFrom)) $errors[] = 'Unable to find user with id:' . $userId . '.';
   
    
    if(sizeof($errors)) { 
       return array(
                'status' => 'error', 
                'message' => 'Validation error.', 
                'validation' => $errors
            );
       
    } else {
    
        $privateMessages = $em->getRepository('KAPIKooshApiBundle:PrivateMessage')->findBySender($userIdRecipient, $userId);

        if(sizeof($privateMessages)) {
            foreach($privateMessages AS $key => $message) {
                // remove message 
                $em->remove($message);
                $em->flush();
            }
        }

        if (!sizeof($privateMessages)) {
            return array(
                'status' => 'error', 
                'message' => 'There are no private messages in this conversation.'
            );
        } else {
            return array(
                'status' => 'ok', 
                'message' => 'All private messages included in this conversation have been removed.'
            );
        }
    }
}


/**
* Gets a list of the users messages grouped by sender.
*
* @ApiDoc(
* resource = true,
* description = "Gets a list of the users messages grouped by sender.",
* output = "preferred rate",
* statusCodes = {
* 200 = "Returned when successful",
* 404 = "Returned when the page is not found"
* }
* )
*
* @param \Symfony\Component\HttpFoundation\Request $request
* @param int $userId
* @return mixed
* @Get("/get_private_messages/{userId}")
* 
* @View
*/
public function getPrivateMessagesAction($userId)
{
    $baseUrl = $this->getRequest()->getSchemeAndHttpHost() . $this->container->getParameter('web_url_prefix');
    $em = $this->getDoctrine()->getManager();
    
    
    $userTo = $em->getRepository('KAPIKooshApiBundle:User')->findOneBy(array('id' => $userId));

    // Validation
    $errors = array();
    if(empty($userId)) $errors[] = 'Empty `user_id` param.'; 
    if(!is_object($userTo)) $errors[] = 'Unable to find user with id:' . $userId . '.';
   
    
    if(sizeof($errors)) { 
       return array(
                'status' => 'error', 
                'message' => 'Validation error.', 
                'validation' => $errors
            );
       
    } else {
    
        $privateMessages = $em->getRepository('KAPIKooshApiBundle:PrivateMessage')->findAllGroupedBySender($userId);

        $userMessages = array();
        if(sizeof($privateMessages)) {
            foreach($privateMessages AS $key => $message) {
                
                      
                
                if($message->getUserFrom()->getId() != $userId) {
                    $messageUserName = $message->getUserFrom()->getUsername();
                    $messageUserId = $message->getUserFrom()->getId();
                    
                    // Get Image
                    $imagePath = $this->get('kapi_helper')->getProfileThumbnailURL($message->getUserFrom(), true); 

                    // Get Latest message
                    $latestMessageText = $em->getRepository('KAPIKooshApiBundle:PrivateMessage')->getLatestMessageText($userId, $message->getUserFrom()->getId());          
                } else {
                    $messageUserName = $message->getUserTo()->getUsername();
                    $messageUserId = $message->getUserTo()->getId();
                    
                    // Get Image
                    $imagePath = $this->get('kapi_helper')->getProfileThumbnailURL($message->getUserTo(), true); 

                    // Get Latest message
                    $latestMessageText = $em->getRepository('KAPIKooshApiBundle:PrivateMessage')->getLatestMessageText($userId, $message->getUserTo()->getId());          
                }
                
                $userMessages[] = array (
                    'user_name' => $messageUserName, 
                    'user_id' => $messageUserId,
                    'user_image' => !empty($imagePath) ? $imagePath : '', 
                    'last_message' => $latestMessageText, 
                    'message_time' => $message->getCreated()->format("U"), 
                    'message_id' => $message->getId()
                );
            }
        }

        if (!sizeof($userMessages)) {
            return array(
                'status' => 'error', 
                'message' => 'There are no private messages for user.'
            );
        } else {
            return array(
                'status' => 'ok', 
                'data' => array('message_list' => $userMessages)
            );
        }
    }
}


/**
* Gets a list of the users private messages by sender.
*
* @ApiDoc(
* resource = true,
* description = "Gets a list of the users private messages by sender.",
* output = "preferred rate",
* statusCodes = {
* 200 = "Returned when successful",
* 404 = "Returned when the page is not found"
* }
* )
*
* @param \Symfony\Component\HttpFoundation\Request $request
* @return mixed
* @Patch("/get_private_message_detail")
* 
* @View
*/
public function getPrivateMessageDetailAction(Request $request)
{
    $baseUrl = $this->getRequest()->getSchemeAndHttpHost() . $this->container->getParameter('web_url_prefix');
    $em = $this->getDoctrine()->getManager();
    
    // Get Params
    $userId = $request->get('user_id');
    $userIdRecipient = $request->get('user_id_recipient');
    
    $userFrom = $em->getRepository('KAPIKooshApiBundle:User')->findOneBy(array('id' => $userId));
    $userTo = $em->getRepository('KAPIKooshApiBundle:User')->findOneBy(array('id' => $userIdRecipient));

    // Validation
    $errors = array();
    if(empty($userId)) $errors[] = 'Empty `user_id` param.'; 
    if(empty($userIdRecipient)) $errors[] = 'Empty `user_id_recipient` param.'; 
    if(!is_object($userTo)) $errors[] = 'Unable to find user with id:' . $userIdRecipient . '.';
    if(!is_object($userFrom)) $errors[] = 'Unable to find user with id:' . $userId . '.';
   
    
    if(sizeof($errors)) { 
       return array(
                'status' => 'error', 
                'message' => 'Validation error.', 
                'validation' => $errors
            );
       
    } else {
    
        $privateMessages = $em->getRepository('KAPIKooshApiBundle:PrivateMessage')->findBySender($userIdRecipient, $userId);

        $userMessages = array();
        if(sizeof($privateMessages)) {
            foreach($privateMessages AS $key => $message) {
                
                $userMessages[] = array (
                    'user_name' => $message->getUserFrom()->getUsername(), 
                    'user_id' => $message->getUserFrom()->getId(),
                    'message' => $message->getMessage(), 
                    'message_time' => $message->getCreated()->format("U"), 
                    'message_id' => $message->getId()
                );
            }
        }

        if (!sizeof($userMessages)) {
            return array(
                'status' => 'error', 
                'message' => 'There are no private messages between these users.'
            );
        } else {
            return array(
                'status' => 'ok', 
                'data' => array('message_list' => $userMessages)
            );
        }
    }
}

/**
* Gets a list of all enabled system audio files.
*
* @ApiDoc(
* resource = true,
* description = "Gets a list of all enabled system audio files.",
* output = "preferred rate",
* statusCodes = {
* 200 = "Returned when successful",
* 404 = "Returned when the page is not found"
* }
* )
*
* @param \Symfony\Component\HttpFoundation\Request $request
* @param int $userId
* @return mixed
* @Get("/get_system_audio_list")
* 
* @View
*/
public function getSystemAudioListAction()
{
    $baseUrl = $this->getRequest()->getSchemeAndHttpHost() . $this->container->getParameter('web_url_prefix');
    
    $em = $this->getDoctrine()->getManager();
    
    
    $systemAudioTracks = $em->getRepository('KAPIKooshApiBundle:SystemAudio')->findBy(array('status' => '1'));

    $systemAudioList = array();
    if(sizeof($systemAudioTracks)) {
        foreach($systemAudioTracks AS $key => $audioTrack) {
            $systemAudioList[] = array(
                'id' => $audioTrack->getId(), 
                'title' => $audioTrack->getTitle(), 
                'url' => $baseUrl . '/' . $audioTrack->getAudioFileWebPath(),
            );
        }
    }

    if(!sizeof($systemAudioList)) {
        return array(
            'status' => 'error', 
            'message' => 'There are not any system audio tracks.'
        );
    } else {
        return array(
            'status' => 'ok', 
            'data' => array('system_audio_list' => $systemAudioList)
        );
    }
}



}
