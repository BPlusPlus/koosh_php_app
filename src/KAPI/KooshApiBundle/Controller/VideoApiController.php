<?php
/**
 * https://github.com/liuggio/symfony2-rest-api-the-best-2013-way/tree/master/app
 * https://github.com/nmpolo/Symfony2Rest/blob/master/src/Nmpolo/RestBundle/Controller/UserController.php
 */

namespace KAPI\KooshApiBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\Controller\Annotations\NamePrefix;
use FOS\RestBundle\Controller\Annotations\Prefix;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Patch;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Request\ParamFetcherInterface;

use Symfony\Component\Form\FormTypeInterface;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use KAPI\KooshApi\CalculatorBundle\Exception\InvalidFormException;
use KAPI\KooshApiBundle\Form\PageType;
use KAPI\KooshApiBundle\Model\ClientInterface;

use FFMpeg\Coordinate\Dimension;
use FFMpeg\Filters\Video\ResizeFilter;
use FFMpeg\Format\Video\X264;
use FFMpeg\Coordinate\FrameRate;
use FFMpeg\Coordinate\TimeCode;


use Symfony\Component\Security\Core\Util\SecureRandom;

/**
 * @NamePrefix("api_rest_")
 * @Prefix("/api")
 * @RouteResource("videoApi")
 * @View
 */
class VideoApiController extends FOSRestController
{

/**
* Uploads the edited videos for the koosh.
*
* @ApiDoc(
* resource = true,
* description = "Uploads the edited videos for the koosh.",
* output = "",
* statusCodes = {
* 200 = "Returned when successful",
* 404 = "Returned when the page is not found"
* }
* )
*
* @param \Symfony\Component\HttpFoundation\Request $request
* @return mixed
* @Post("/upload_edited_video")
* 
* @View
*/
public function uploadEditedVideoAction(Request $request) {

    // Get Params
    $userId = $request->get('user_id');
    $videoId = $request->get('video_id');
    $videoData = $request->get('video_data');
    
    $videoUploadedFile = $request->files->get(str_replace('.', '_', $videoData));
    
    // Validation
    $errors = array();
    if(empty($userId)) $errors[] = 'Empty `user_id` param.'; 
    if(empty($videoId)) $errors[] = 'Empty `video_id` param.'; 
    if(empty($videoData)) $errors[] = 'Empty `video_data` param.';
    
    // Unique email and username validation
    $em = $this->getDoctrine()->getManager();
    
    // Video file format validation
    if(is_object($videoUploadedFile)) {
        $extension = strtolower($videoUploadedFile->guessExtension());
        if($extension != 'mp4') $errors[] = 'Video should be in ´.mp4´ format.';
    }
    
    $kooshItem = $em->getRepository('KAPIKooshApiBundle:KooshItem')->findOneBy(array('id' => $videoId));

    if(!is_object($kooshItem) || !is_object($kooshItem->getVideo()) ) {
        $errors[] = 'There is no item with id:' . $videoId . '.';
    } else {
        $video = $kooshItem->getVideo();
        if(!is_object($video)) {
            $errors[] = 'There is no video with id:' . $videoId . '.';
        }
    }
    
    if(sizeof($errors)) { 
       return array(
                'status' => 'error', 
                'message' => 'Validation error.', 
                'validation' => $errors
            );
       
    } else {
        // Try to upload and update video
        try { 
            $video->setVideoFile($videoUploadedFile);

            $em = $this->getDoctrine()->getManager();
            $em->persist($video);
            $em->flush();
            
            // Store final video freezframe
            $ffmpeg = $this->container->get('dubture_ffmpeg.ffmpeg');
            $freezeImageName = sha1(uniqid(mt_rand(), true)) . '.jpg';
            $freezeImageFilePath = $video->getAbsolutePath() . $freezeImageName;
            try {
                $videoF = $ffmpeg->open($video->getVideoFileAbsolutePath());
                $videoF->frame(TimeCode::fromSeconds(5))
                           ->save($freezeImageFilePath);
            } catch (Exception $e) {
                $this->container->get('api.logger')->addError('FFMPEG get video freeze image error: '.$e->getMessage());  
            }    
            $video->setImageFilePath($freezeImageName);
            $video->setVideoFile(null);
            $em->persist($video);
            $em->flush();
            

            
            return array(
                'status' => 'ok', 
                'message' => 'Video has been successfully updated.', 
            );
        } catch (Exception $e) {
            return array(
                'status' => 'error', 
                'message' => 'Unable to upload video data - error: ' . $e->getMessage(), 
            );
        }
    }
}

}