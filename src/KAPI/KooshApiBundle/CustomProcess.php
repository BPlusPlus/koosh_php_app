<?php

namespace KAPI\KooshApiBundle;

use Psr\Log\LoggerInterface;
use Symfony\Component\Process\Process;

class CustomProcess extends Process
{

    /**
     * @var LoggerInterface
     */
    protected static $logger;
    
    protected $kinfo_cmdline;
    protected $kinfo_runtime_start;
    
    public static function setLogger(LoggerInterface $logger)
    {
        static::$logger = $logger;
    }

    public function __construct($commandline, $cwd = null, array $env = null, $input = null, $timeout = 60, array $options = array())
    {
        $this->kinfo_cmdline = $commandline;
        parent::__construct($commandline, $cwd, $env, $input, $timeout, $options);
    }

    public function run($callback = null)
    {
        $logger = static::$logger;
        /** @var $logger LoggerInterface|null */
        if($logger) {
            $logger->info("Running process", ['commandline' => $this->kinfo_cmdline]);
            $this->kinfo_runtime_start = time();
        }
        $retval = parent::run($callback);
        if($logger) {
            $args = [
                'exit_code' => $retval,
                'runtime_seconds' => time() - $this->kinfo_runtime_start
            ];
            if($retval !== 0) {
                $logger->warning('Process finished with non-zero exit code', $args);
            } else {
                $logger->info('Process finished', $args);
            }
        }
        return $retval;
    }


}