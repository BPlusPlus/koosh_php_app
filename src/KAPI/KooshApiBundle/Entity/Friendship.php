<?php

namespace KAPI\KooshApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(repositoryClass="KAPI\KooshApiBundle\Repository\FriendshipRepository")
 * @ORM\Table(name="friendship")
 * @ORM\HasLifecycleCallbacks()
 * @Serializer\ExclusionPolicy("all")
 */
class Friendship
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\Column(type="integer")
     * 
     * @Assert\NotBlank()
     */
    protected $userIdFrom;

    /**
     * @ORM\Column(type="integer")
     * 
     * @Assert\NotBlank()
     */
    protected $userIdTo;

    /**
     * @ORM\Column(type="integer", options={"default":0})
     * 
     */
    protected $status;
    
    /**
     * @ORM\Column(type="datetime")
     */
    protected $created;
    
    /**
     * @ORM\Column(type="datetime")
     */
    protected $updated;
 
    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="friendshipFrom")
     * @ORM\JoinColumn(name="userIdFrom", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $userFrom;
    
    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="friendshipTo")
     * @ORM\JoinColumn(name="userIdTo", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $userTo;
    
    public function toString()
    {
        return $this->id;
    }

    
    /** @ORM\PrePersist */
    public function doStuffOnPrePersist()
    {
        $this->created = new \DateTime();
        $this->updated = new \DateTime();       
        
        $this->status = 0;
    }
    
    /** @ORM\PreUpdate */
    public function doStuffOnPreUpdate()
    {
        $this->updated = new \DateTime();
    }
   

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userIdFrom
     *
     * @param integer $userIdFrom
     * @return Friendship
     */
    public function setUserIdFrom($userIdFrom)
    {
        $this->userIdFrom = $userIdFrom;

        return $this;
    }

    /**
     * Get userIdFrom
     *
     * @return integer 
     */
    public function getUserIdFrom()
    {
        return $this->userIdFrom;
    }

    /**
     * Set userIdTo
     *
     * @param integer $userIdTo
     * @return Friendship
     */
    public function setUserIdTo($userIdTo)
    {
        $this->userIdTo = $userIdTo;

        return $this;
    }

    /**
     * Get userIdTo
     *
     * @return integer 
     */
    public function getUserIdTo()
    {
        return $this->userIdTo;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Friendship
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set userFrom
     *
     * @param \KAPI\KooshApiBundle\Entity\User $userFrom
     * @return Friendship
     */
    public function setUserFrom(\KAPI\KooshApiBundle\Entity\User $userFrom = null)
    {
        $this->userFrom = $userFrom;

        return $this;
    }

    /**
     * Get userFrom
     *
     * @return \KAPI\KooshApiBundle\Entity\User 
     */
    public function getUserFrom()
    {
        return $this->userFrom;
    }

    /**
     * Set userTo
     *
     * @param \KAPI\KooshApiBundle\Entity\User $userTo
     * @return Friendship
     */
    public function setUserTo(\KAPI\KooshApiBundle\Entity\User $userTo = null)
    {
        $this->userTo = $userTo;

        return $this;
    }

    /**
     * Get userTo
     *
     * @return \KAPI\KooshApiBundle\Entity\User 
     */
    public function getUserTo()
    {
        return $this->userTo;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return Friendship
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Friendship
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }
}
