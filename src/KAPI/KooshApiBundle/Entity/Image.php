<?php

namespace KAPI\KooshApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @ORM\Entity
 * @ORM\Table(name="image")
 * @ORM\HasLifecycleCallbacks()
 * @Serializer\ExclusionPolicy("all")
 */
class Image
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\Column(type="string")
     * 
     * @Assert\Length(min="1", max=255)
     */
    protected $title;
    
    /**
     * @ORM\Column(type="string", nullable=true)
     * 
     */
    protected $caption;

    /**
     * @ORM\Column(type="string", nullable=true)
     * 
     */
    protected $imageFile;
    
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $imageFilePath;
    
    /**
     * @ORM\Column(type="integer")
     * 
     */
    protected $userId;
    
     /**
     * @ORM\Column(type="integer", nullable=true)
     * 
     */
    protected $systemAudioId;
    
    /**
     * @ORM\Column(type="integer")
     * 
     */
    protected $kooshId;
    
    
    /**
     * @ORM\Column(type="smallint", options={"default":0})
     * 
     */
    protected $nudged;
    
    /**
     * @ORM\Column(type="integer", options={"default":0})
     * 
     */
    protected $status;
    
    /**
     * @ORM\Column(type="datetime")
     */
    protected $created;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $updated;
    
    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="images")
     * @ORM\JoinColumn(name="userId", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $user;
    
    /**
     * @ORM\ManyToOne(targetEntity="SystemAudio", inversedBy="images")
     * @ORM\JoinColumn(name="systemAudioId", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $systemAudio;
    
    /**
     * @ORM\ManyToOne(targetEntity="Koosh", inversedBy="images")
     * @ORM\JoinColumn(name="kooshId", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $koosh;
    
    
    /** @ORM\PrePersist */
    public function doStuffOnPrePersist()
    {
        $this->created = new \DateTime();
        $this->updated = new \DateTime();
        
        $this->nudged = 0;
        $this->status = 0;
    }
    
    /** @ORM\PreUpdate */
    public function doStuffOnPreUpdate()
    {
        $this->updated = new \DateTime();
    }
    
    public function toString()
    {
        return $this->title;
    }

    public function __toString() 
    {
        return $this->title;
    }
     
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->videoLikes = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Video
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Get videoFile
     *
     * @return string 
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     * @return Video
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer 
     */
    public function getUserId()
    {
        return $this->userId;
    }
    
     /**
     * Set systemAudioId
     *
     * @param integer $systemAudioId
     * @return Image
     */
    public function setSystemAudioId($systemAudioId)
    {
        $this->systemAudioId = $systemAudioId;

        return $this;
    }

    /**
     * Get systemAudioId
     *
     * @return integer 
     */
    public function getSystemAudioId()
    {
        return $this->systemAudioId;
    }

    /**
     * Set kooshId
     *
     * @param integer $kooshId
     * @return Video
     */
    public function setKooshId($kooshId)
    {
        $this->kooshId = $kooshId;

        return $this;
    }

    /**
     * Get kooshId
     *
     * @return integer 
     */
    public function getKooshId()
    {
        return $this->kooshId;
    }

    /**
     * Set nudged
     *
     * @param integer $nudged
     * @return Video
     */
    public function setNudged($nudged)
    {
        $this->nudged = $nudged;

        return $this;
    }

    /**
     * Get nudged
     *
     * @return integer 
     */
    public function getNudged()
    {
        return $this->nudged;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Video
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return Video
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set user
     *
     * @param \KAPI\KooshApiBundle\Entity\User $user
     * @return Video
     */
    public function setUser(\KAPI\KooshApiBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \KAPI\KooshApiBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
    
    /**
     * Set systemAudio
     *
     * @param \KAPI\KooshApiBundle\Entity\SystemAudio $systemAudio
     * @return Image
     */
    public function setSystemAudio(\KAPI\KooshApiBundle\Entity\SystemAudio $systemAudio = null)
    {
        $this->systemAudio = $systemAudio;

        return $this;
    }

    /**
     * Get systemAudio
     *
     * @return \KAPI\KooshApiBundle\Entity\SystemAudio 
     */
    public function getSystemAudio()
    {
        return $this->systemAudio;
    }

    public function getSystemAudioHelperPath()
    {
        return $this->koosh->getHelpersAbsolutePath() . $this->systemAudio->getAudioFilePath();
    }

    /**
     * Set koosh
     *
     * @param \KAPI\KooshApiBundle\Entity\Koosh $koosh
     * @return Video
     */
    public function setKoosh(\KAPI\KooshApiBundle\Entity\Koosh $koosh = null)
    {
        $this->koosh = $koosh;

        return $this;
    }

    /**
     * Get koosh
     *
     * @return \KAPI\KooshApiBundle\Entity\Koosh 
     */
    public function getKoosh()
    {
        return $this->koosh;
    }

    
    // YourNS/YourBundle/Entity/Image.php
    const SERVER_PATH_TO_IMAGE_FOLDER = 'uploads/image';


    public function getAbsolutePath()
    {
        return $this->getUploadRootDir().'/';
    }
    
    public function getImageFileAbsolutePath()
    {
        return null === $this->imageFilePath
            ? null
            : $this->getUploadRootDir().'/'.$this->imageFilePath;
    }

    public function getImageFileHelperAbsolutePath()
    {
        return null === $this->imageFilePath
            ? null
            : $this->koosh->getHelpersAbsolutePath().$this->imageFilePath;
    }

    public function getImageFileWebPath()
    {
        return null === $this->imageFilePath
            ? null
            : $this->koosh->getUploadDir().'/'.$this->getUploadDir().'/'.$this->imageFilePath;
    }
    
    public function getImageWebPath()
    {
        return null === $this->imageFilePath
            ? null
            : $this->getUploadDir().'/'.$this->imageFilePath;
    }

    protected function getUploadRootDir()
    {   
        $uploadRootDir = $this->koosh->getAbsolutePath() . $this->getUploadDir();
        
        // check existing path
        if (!file_exists($uploadRootDir)) {
            mkdir($uploadRootDir, 0755, true);
        }
        
        // the absolute directory path where uploaded
        // documents should be saved
        return $uploadRootDir;
    }
    
    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'images';
    }
    
    
   private $imageFileTemp;

    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setImageFile(UploadedFile $file = null)
    {
        $this->imageFile = $file;
        // check if we have an old image imageFilePath
        if (isset($this->imageFilePath)) {
            // store the old name to delete after the update
            $this->imageFileTemp = $this->imageFilePath;
            $this->imageFilePath = null;
        } else {
            $this->imageFilePath = 'initial';
        }
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        
        if (null !== $this->getImageFile()) {
            // do whatever you want to generate a unique name
            $filename = sha1(uniqid(mt_rand(), true));
            $this->imageFilePath = $filename.'.'.$this->getImageFile()->guessExtension();
        } else {
            $this->imageFilePath = $this->imageFileTemp;
        }
        
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if (null === $this->getImageFile()) {
            return;
        }
        
        // if there is an error when moving the file, an exception will
        // be automatically thrown by move(). This will properly prevent
        // the entity from being persisted to the database on error
        $this->getImageFile()->move($this->getUploadRootDir(), $this->imageFilePath);

        // check if we have an old image
        if (isset($this->imageFileTemp)) {
            // delete the old image
            unlink($this->getUploadRootDir().'/'.$this->imageFileTemp);
            // clear the imageFileTemp image imageFilePath
            $this->imageFileTemp = null;
        }
        //$this->imageFile = null;
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        if (($file = $this->getImageFileAbsolutePath()) && is_file($file)) {
            unlink($file);
        }
    }


    /**
     * Set path
     *
     * @param string $path
     * @return User
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string 
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set imageFilePath
     *
     * @param string $imageFilePath
     * @return Video
     */
    public function setImageFilePath($imageFilePath)
    {
        $this->imageFilePath = $imageFilePath;

        return $this;
    }

    /**
     * Get imageFilePath
     *
     * @return string 
     */
    public function getImageFilePath()
    {
        return $this->imageFilePath;
    }
    
    public function getActionField() {
        return '<a href="">EDIT</a>';
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Video
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set caption
     *
     * @param string $caption
     * @return Video
     */
    public function setCaption($caption)
    {
        $this->caption = $caption;

        return $this;
    }

    /**
     * Get caption
     *
     * @return string 
     */
    public function getCaption()
    {
        return $this->caption;
    }

}
