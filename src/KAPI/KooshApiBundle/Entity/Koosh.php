<?php

namespace KAPI\KooshApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @ORM\Entity(repositoryClass="KAPI\KooshApiBundle\Repository\KooshRepository")
 * @ORM\Table(name="koosh")
 * @ORM\HasLifecycleCallbacks()
 * @Serializer\ExclusionPolicy("all")
 * 
 */
class Koosh
{

    const QUALITY_DEFAULT = 1;
    const QUALITY_2 = 2;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\Column(type="string")
     * 
     * @Assert\NotBlank()
     * @Assert\Length(min="1", max=255)
     */
    protected $title;
    
    /**
     * @ORM\Column(type="string", nullable=true)
     * 
     */
    protected $notificationMessage;
    
    /**
     * @ORM\Column(type="string")
     * 
     * @Assert\NotBlank()
     * @Assert\Length(min="1", max=255)
     */
    protected $genre;
    
    /**
     * @ORM\Column(type="string")
     * 
     * @Assert\NotBlank()
     * @Assert\Length(min="1", max=255)
     */
    protected $type;
    
    /**
     * @ORM\Column(type="string", nullable=true)
     * 
     */
    protected $length;

    /**
     * @ORM\Column(type="string", nullable=true)
     * 
     */
    protected $uniqueHash;
    
    /**
     * @ORM\Column(type="string", nullable=true)
     * 
     */
    protected $image;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $fadeType;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $imagePath;
    
    /**
     * @ORM\Column(type="string", nullable=true)
     * 
     */
    protected $videoFile;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $videoFilePath;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $videoFilePath2;
    
    /**
     * @ORM\Column(type="string", nullable=true)
     * 
     */
    protected $addAudio;
    
    /**
     * @ORM\Column(type="string", nullable=true)
     * 
     */
    protected $audioFile;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $audioFilePath;
    
    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $systemAudioId;
    
    /**
     * @ORM\Column(type="integer")
     */
    protected $userId;

    /**
     * @ORM\Column(type="integer")
     */
    protected $status;
    
    /**
     * @ORM\Column(type="smallint", nullable=true, options={"default":0})
     * 
     */
    protected $disableNotifications;
    
    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $rabbitmqCounter;

    /**
     * @ORM\Column(type="string")
     */
    protected $publicStatus;
    
    /**
     * @ORM\Column(type="datetime")
     */
    protected $created;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $updated;
    
    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $processStarted;
    
    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $processEnded;
    
    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $rabbitQueueAdded;
    
    /**
     * @ORM\Column(type="text", nullable=true)
     * 
     */
    protected $errorMessage;

    /**
     * @ORM\Column(type="text", nullable=true)
     *
     */
    protected $ramProcessDirPath;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $processingInstructions;
    
    /**
     * @ORM\Column(type="integer", nullable=true)
     * 
     */
    protected $fameValue;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     */
    protected $dropletUrl;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="kooshes")
     * @ORM\JoinColumn(name="userId", referencedColumnName="id")
     */
    protected $user;
    
    /**
     * @ORM\ManyToOne(targetEntity="SystemAudio", inversedBy="kooshes")
     * @ORM\JoinColumn(name="systemAudioId", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $systemAudio;
    
    /**
     * @ORM\OneToMany(targetEntity="KooshComment", mappedBy="koosh", cascade={"all"}, orphanRemoval=true)
     */
    protected $comments;
    
    /**
     * @ORM\OneToMany(targetEntity="Notification", mappedBy="koosh", cascade={"all"})
     */
    protected $notifications;
    
    /**
     * @ORM\OneToMany(targetEntity="Video", mappedBy="koosh", cascade={"all"}, orphanRemoval=true)
     */
    protected $videos;
    
    /**
     * @ORM\OneToMany(targetEntity="Image", mappedBy="koosh", cascade={"all"}, orphanRemoval=true)
     */
    protected $images;
    
    /**
     * @ORM\OneToMany(targetEntity="KooshItem", mappedBy="koosh", cascade={"all"}, orphanRemoval=true)
     */
    protected $kooshItems;
    
    /**
     * @ORM\OneToMany(targetEntity="KooshLike", mappedBy="koosh")
     */
    protected $kooshLikes;
    
    
    /** @ORM\PrePersist */
    public function doStuffOnPrePersist()
    {
        $this->created = new \DateTime();
        $this->updated = new \DateTime();
        
        $this->status = 0;
        $this->uniqueHash = md5(uniqid(rand()));
    }
    
    /** @ORM\PreUpdate */
    public function doStuffOnPreUpdate()
    {
        $this->updated = new \DateTime();
    }
    
    public function toString()
    {
        return $this->title;
    }

    public function __toString()
    {
        return $this->title;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Koosh
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }


    /**
     * Set dropletUrl
     *
     * @param string $dropletUrl
     * @return Koosh
     */
    public function setDropletUrl($dropletUrl)
    {
        $this->dropletUrl = $dropletUrl;

        return $this;
    }

    /**
     * Get dropletUrl
     *
     * @return string
     */
    public function getDropletUrl()
    {
        return $this->dropletUrl;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     * @return Koosh
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer 
     */
    public function getUserId()
    {
        return $this->userId;
    }
    
    /**
     * Set systemAudioId
     *
     * @param integer $systemAudioId
     * @return Koosh
     */
    public function setSystemAudioId($systemAudioId)
    {
        $this->systemAudioId = $systemAudioId;

        return $this;
    }

    /**
     * Get systemAudioId
     *
     * @return integer 
     */
    public function getSystemAudioId()
    {
        return $this->systemAudioId;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Koosh
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }
    
    /**
     * Set status
     *
     * @param integer $disableNotifications
     * @return Koosh
     */
    public function setDisableNotifications($disableNotifications)
    {
        $this->disableNotifications = $disableNotifications;

        return $this;
    }

    /**
     * Get disableNotifications
     *
     * @return integer 
     */
    public function getDisableNotifications()
    {
        return $this->disableNotifications;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Koosh
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return Koosh
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set user
     *
     * @param \KAPI\KooshApiBundle\Entity\User $user
     * @return Koosh
     */
    public function setUser(\KAPI\KooshApiBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \KAPI\KooshApiBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
    
    /**
     * Set SystemAudio
     *
     * @param \KAPI\KooshApiBundle\Entity\SystemAudio $systemAudio
     * @return Koosh
     */
    public function setSystemAudio(\KAPI\KooshApiBundle\Entity\SystemAudio $systemAudio = null)
    {
        $this->systemAudio = $systemAudio;

        return $this;
    }

    /**
     * Get SystemAudio
     *
     * @return \KAPI\KooshApiBundle\Entity\SystemAudio 
     */
    public function getSystemAudio()
    {
        return $this->systemAudio;
    }

    public function getSystemAudioHelperPath()
    {
        return $this->getHelpersAbsolutePath() . $this->systemAudio->getAudioFilePath();
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->comments = new \Doctrine\Common\Collections\ArrayCollection();
        $this->videos = new \Doctrine\Common\Collections\ArrayCollection();
        $this->kooshLikes = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get comments
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Add kooshLikes
     *
     * @param \KAPI\KooshApiBundle\Entity\KooshLike $kooshLikes
     * @return Koosh
     */
    public function addKooshLike(\KAPI\KooshApiBundle\Entity\KooshLike $kooshLikes)
    {
        $this->kooshLikes[] = $kooshLikes;

        return $this;
    }

    /**
     * Remove kooshLikes
     *
     * @param \KAPI\KooshApiBundle\Entity\KooshLike $kooshLikes
     */
    public function removeKooshLike(\KAPI\KooshApiBundle\Entity\KooshLike $kooshLikes)
    {
        $this->kooshLikes->removeElement($kooshLikes);
    }

    /**
     * Get kooshLikes
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getKooshLikes()
    {
        return $this->kooshLikes;
    }

    public function getHelpersAbsolutePath($useRamDir = true)
    {
        if($useRamDir) {
            $helpersPath = $this->getUploadRamProcessDir();
        } else {
            $helpersPath = $this->getUploadRootDir() . '/helpers';
        }

        // check existing path
        if (!file_exists($helpersPath)) {
            mkdir($helpersPath, 0755, true);
        }

        return $helpersPath.'/';
    }

    public function getAbsolutePath()
    {
        return $this->getUploadRootDir().'/';
    }
    
    public function getHashAbsolutePath()
    {
        return $this->getAbsolutePath() . $this->getCreated()->format('n_Y') . '/' . $this->getUniqueHash . '/';
    }
    
    public function getImageAbsolutePath()
    {
        return null === $this->imagePath
            ? null
            : $this->getUploadRootDir().'/'.$this->imagePath;
    }
    
    public function getVideoFileAbsolutePath()
    {
        return null === $this->videoFilePath
            ? null
            : $this->getUploadRootDir().'/'.$this->videoFilePath;
    }
    
    public function getVideoFileAbsolutePathByQuality($quality)
    {
        $propname = $this->getQualityPropname($quality);
        
        return null === $this->$propname
            ? null 
            : $this->getUploadRootDir().'/'.$this->$propname;
    }
    
    public function getAudioFileAbsolutePath()
    {
        return null === $this->audioFilePath
            ? null
            : $this->getUploadRootDir().'/'.$this->audioFilePath;
    }

    public function getAudioFileHelperAbsolutePath()
    {
        return null === $this->audioFilePath
            ? null
            : $this->getHelpersAbsolutePath().$this->audioFilePath;
    }

    public function getFontHelperAbsolutePath() {
        return $this->getHelpersAbsolutePath() . 'Helvetica_Reg.ttf';
    }

    public function getImageWebPath()
    {
        return null === $this->imagePath
            ? null
            : $this->getUploadDir().'/'.$this->imagePath;
    }

    protected function getQualityPropname($quality)
    {
        if($quality === null)
            $quality = static::QUALITY_DEFAULT;

        if($quality == static::QUALITY_DEFAULT) {
            $propname = 'videoFilePath';
        } elseif($quality == static::QUALITY_2) {
            $propname = 'videoFilePath2';
        } else {
            throw new \LogicException();
        }
        return $propname;
    }
    
    public function getVideoFileWebPath($quality = null)
    {
        $propname = $this->getQualityPropname($quality);

        return null === $this->$propname
            ? null
            : $this->getUploadDir().'/'.$this->$propname;
    }
    
    public function getAudioFileWebPath()
    {
        return null === $this->audioFilePath
            ? null
            : $this->getUploadDir().'/'.$this->audioFilePath;
    }

    protected function getUploadRootDir()
    {
        $uploadRootDir = __DIR__.'/../../../../web/'.$this->getUploadDir();
        
        // check existing path
        if (!file_exists($uploadRootDir)) {
            mkdir($uploadRootDir, 0755, true);
        }
        
        // the absolute directory path where uploaded
        // documents should be saved
        return $uploadRootDir;
    }
    
    public function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'uploads/koosh/' . $this->getCreated()->format('n_Y') . '/' . $this->getUniqueHash() ;
    }

    public function getUploadRamProcessDir()
    {
        $ramProcessDir = $this->getRamProcessDirPath() . '/' . $this->getUniqueHash();

        // check existing path
        if (!file_exists($ramProcessDir)) {
            mkdir($ramProcessDir, 0755, true);
        }

        return $ramProcessDir;
    }

    
    public function getWatermarkAbsolutePath() 
    {
       return __DIR__.'/../../../../web/uploads/watermark/logo_admin.png';
    }


    private $videoFileTemp;
    private $imageTemp;
    private $audioFileTemp;

    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setVideoFile(UploadedFile $file = null)
    {
        $this->videoFile = $file;
        // check if we have an old image path
        if (isset($this->videoFilePath)) {
            // store the old name to delete after the update
            $this->videoFileTemp = $this->videoFilePath;
            $this->videoFilePath = null;
        } else {
            $this->videoFilePath = 'initial';
        }
    }
    
    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setAudioFile(UploadedFile $file = null)
    {
        $this->audioFile = $file;
        // check if we have an old image path
        if (isset($this->audioFilePath)) {
            // store the old name to delete after the update
            $this->audioFileTemp = $this->audioFilePath;
            $this->audioFilePath = null;
        } else {
            $this->audioFilePath = 'initial';
        }
    }
    
    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setImage(UploadedFile $file = null)
    {
        $this->image = $file;
        // check if we have an old image path
        if (isset($this->imagePath)) {
            // store the old name to delete after the update
            $this->imageTemp = $this->imagePath;
            $this->imagePath = null;
        } else {
            $this->imagePath = 'initial';
        }
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        if (null !== $this->getVideoFile()) {
            // do whatever you want to generate a unique name
            $filename = sha1(uniqid(mt_rand(), true));
            $this->videoFilePath = $filename.'.'.$this->getVideoFile()->guessExtension();
        } else {
            $this->videoFilePath = $this->videoFileTemp;
        }
        
        #var_dump($this->getImage()); die();
        if ((null !== $this->getImage()) && is_object($this->getImage())) {
            // do whatever you want to generate a unique name
            $filename = sha1(uniqid(mt_rand(), true));
            $this->imagePath = $filename.'.'.$this->getImage()->guessExtension();
        } else {
            $this->imagePath = $this->imageTemp;
        }
        
       if ((null !== $this->getAudioFile()) && is_object($this->getAudioFile())) {
            // do whatever you want to generate a unique name
            $filename = sha1(uniqid(mt_rand(), true));
            $this->audioFilePath = $filename.'.mp3';
        } else {
            $this->audioFilePath = $this->audioFileTemp;
        }
        
        
        
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function uploadVideoFile()
    {
        if (null === $this->getVideoFile()) {
            return;
        }
        
        // if there is an error when moving the file, an exception will
        // be automatically thrown by move(). This will properly prevent
        // the entity from being persisted to the database on error
        $this->getVideoFile()->move($this->getUploadRootDir(), $this->videoFilePath);

        // check if we have an old image
        if (isset($this->videoFileTemp) && ($this->videoFileTemp != 'initial')) {
            // delete the old image
            unlink($this->getUploadRootDir().'/'.$this->videoFileTemp);
            // clear the temp image path
            $this->videoFileTemp = null;
        }
        $this->videoFile = null;
    }
    
    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function uploadImage()
    {
        if ((null === $this->getImage()) || !is_object($this->getImage())) {
            return;
        }
        
        // if there is an error when moving the file, an exception will
        // be automatically thrown by move(). This will properly prevent
        // the entity from being persisted to the database on error
        $this->getImage()->move($this->getUploadRootDir(), $this->imagePath);

        // check if we have an old image
        if (isset($this->imageTemp) && ($this->imageTemp != 'initial')) {
            // delete the old image
            unlink($this->getUploadRootDir().'/'.$this->imageTemp);
            // clear the temp image path
            $this->imageTemp = null;
        }
        $this->image = null;
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function uploadAudioFile()
    {
        if (null === $this->getAudioFile()) {
            return;
        }
        
        // if there is an error when moving the file, an exception will
        // be automatically thrown by move(). This will properly prevent
        // the entity from being persisted to the database on error
        $this->getAudioFile()->move($this->getUploadRootDir(), $this->audioFilePath);

        // check if we have an old image
        if (isset($this->audioFileTemp) && ($this->audioFileTemp != 'initial')) {
            // delete the old image
            if(is_file($this->getUploadRootDir().'/'.$this->audioFileTemp)) {
                unlink($this->getUploadRootDir().'/'.$this->audioFileTemp);
            }
            // clear the temp image path
            $this->audioFileTemp = null;
        }
        $this->audioFile = null;
    }
    
    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        if(($imageFile = $this->getImageAbsolutePath()) && is_file($imageFile)) {
            unlink($imageFile);
        }
        
        if(($videoFile = $this->getVideoFileAbsolutePath()) && is_file($videoFile)) {
            unlink($videoFile);
        }
        
        if(($audioFile = $this->getAudioFileAbsolutePath()) && is_file($audioFile)) {
            unlink($audioFile);
        }
        
        if(($videoFile1 = $this->getVideoFileAbsolutePathByQuality(Koosh::QUALITY_2)) && is_file($videoFile1)) {
            unlink($videoFile1);
        }
        
    }


    /**
     * Set imagePath
     *
     * @param string $path
     * @return Koosh
     */
    public function setImagePath($path)
    {
        $this->imagePath = $path;

        return $this;
    }

    /**
     * Get imagePath
     *
     * @return string 
     */
    public function getImagePath()
    {
        return $this->imagePath;
    }
    
    /**
     * Set imagePath
     *
     * @param string $path
     * @return Koosh
     */
    public function setVideoFilePath($path)
    {
        $this->imagePath = $path;

        return $this;
    }

    /**
     * Get videoFilePath
     *
     * @return string 
     */
    public function getVideoFilePath()
    {
        return $this->videoFilePath;
    }
    
    /**
     * Get videoFilePath2
     *
     * @return string 
     */
    public function getVideoFilePath2()
    {
        return $this->videoFilePath2;
    }

    /**
     * Get videoFile
     *
     * @return string 
     */
    public function getVideoFile()
    {
        return $this->videoFile;
    }
    
    public function addComment(KooshComment $kooshComment)
    {
        if(!is_array($this->comments) && !$this->comments instanceof \Traversable)
            $this->comments = [];
        $this->comments[] = $kooshComment;
        $kooshComment->setKoosh($this);
    }

    public function removeComment(KooshComment $kooshComment)
    {
        $this->comments->removeElement($kooshComment);
        $kooshComment->setKoosh(null);
    }
    
    public function addVideo(Video $video)
    {
        if(!is_array($this->videos) && !$this->videos instanceof \Traversable)
            $this->videos = [];
        $this->videos[] = $video;
        $video->setKoosh($this);
    }

    public function removeVideo(Video $video)
    {
        $this->videos->removeElement($video);
        $video->setKoosh(null);
    }

    /**
     * Get videos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getVideos()
    {
        return $this->videos;
    }
    
    
    public function setVideoFileTemp($videoFileTemp)
    {
        $this->videoFileTemp = $videoFileTemp;
    }

    public function setVideoFileDirectly($value, $quality=null)
    {
        $propname = $this->getQualityPropname($quality);
        $this->$propname = $value;
    }
    
    public function setImageTemp($imageTemp)
    {
        $this->imageTemp = $imageTemp;
    }

    /**
     * Set notificationMessage
     *
     * @param string $notificationMessage
     * @return Koosh
     */
    public function setNotificationMessage($notificationMessage)
    {
        $this->notificationMessage = $notificationMessage;

        return $this;
    }

    /**
     * Get notificationMessage
     *
     * @return string 
     */
    public function getNotificationMessage()
    {
        return $this->notificationMessage;
    }

    /**
     * Set genre
     *
     * @param string $genre
     * @return Koosh
     */
    public function setGenre($genre)
    {
        $this->genre = $genre;

        return $this;
    }

    /**
     * Get genre
     *
     * @return string 
     */
    public function getGenre()
    {
        return $this->genre;
    }

    /**
     * Add images
     *
     * @param \KAPI\KooshApiBundle\Entity\Image $images
     * @return Koosh
     */
    public function addImage(\KAPI\KooshApiBundle\Entity\Image $images)
    {
        $this->images[] = $images;

        return $this;
    }

    /**
     * Remove images
     *
     * @param \KAPI\KooshApiBundle\Entity\Image $images
     */
    public function removeImage(\KAPI\KooshApiBundle\Entity\Image $images)
    {
        $this->images->removeElement($images);
    }

    /**
     * Get images
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Koosh
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set length
     *
     * @param string $length
     * @return Koosh
     */
    public function setLength($length)
    {
        $this->length = $length;

        return $this;
    }

    /**
     * Get length
     *
     * @return string 
     */
    public function getLength()
    {
        return $this->length;
    }
    
    /**
     * Set uniqueHash
     *
     * @param string $uniqueHash
     * @return Koosh
     */
    public function setUniqueHash($uniqueHash)
    {
        $this->uniqueHash = $uniqueHash;

        return $this;
    }

    /**
     * Get uniqueHash
     *
     * @return string 
     */
    public function getUniqueHash()
    {
        return $this->uniqueHash;
    }

    /**
     * Set errorMessage
     *
     * @param string $errorMessage
     * @return Koosh
     */
    public function setErrorMessage($errorMessage)
    {
        $this->errorMessage = $errorMessage;

        return $this;
    }

    /**
     * Get errorMessage
     *
     * @return string 
     */
    public function getErrorMessage()
    {
        return $this->errorMessage;
    }

    /**
     * Set ramProcessDirPath
     *
     * @param string $ramProcessDirPath
     * @return Koosh
     */
    public function setRamProcessDirPath($ramProcessDirPath)
    {
        $this->ramProcessDirPath = $ramProcessDirPath;

        return $this;
    }

    /**
     * Get ramProcessDirPath
     *
     * @return string
     */
    public function getRamProcessDirPath()
    {
        return $this->ramProcessDirPath;
    }

    /**
     * Add notifications
     *
     * @param \KAPI\KooshApiBundle\Entity\Notification $notifications
     * @return Koosh
     */
    public function addNotification(\KAPI\KooshApiBundle\Entity\Notification $notifications)
    {
        $this->notifications[] = $notifications;

        return $this;
    }

    /**
     * Remove notifications
     *
     * @param \KAPI\KooshApiBundle\Entity\Notification $notifications
     */
    public function removeNotification(\KAPI\KooshApiBundle\Entity\Notification $notifications)
    {
        $this->notifications->removeElement($notifications);
    }

    /**
     * Get notifications
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getNotifications()
    {
        return $this->notifications;
    }

    /**
     * Set publicStatus
     *
     * @param string $publicStatus
     * @return Koosh
     */
    public function setPublicStatus($publicStatus)
    {
        $this->publicStatus = $publicStatus;

        return $this;
    }

    /**
     * Get publicStatus
     *
     * @return string 
     */
    public function getPublicStatus()
    {
        return $this->publicStatus;
    }

    /**
     * @return mixed
     */
    public function getRabbitmqCounter()
    {
        return $this->rabbitmqCounter;
    }

    /**
     * @param mixed $rabbitmqCounter
     */
    public function setRabbitmqCounter($rabbitmqCounter)
    {
        $this->rabbitmqCounter = $rabbitmqCounter;
    }

    /**
     * @return mixed
     */
    public function getProcessingInstructions()
    {
        return $this->processingInstructions;
    }

    /**
     * @param mixed $processingInstructions
     */
    public function setProcessingInstructions($processingInstructions)
    {
        $this->processingInstructions = $processingInstructions;
    }
    
    /**
     * @return integer
     */
    public function getFameValue()
    {
        return $this->fameValue;
    }

    /**
     * @param integer $fameValue
     */
    public function setFameValue($fameValue)
    {
        $this->fameValue = $fameValue;
    }
    
    /**
     * Set fadeType
     *
     * @param string $fadeType
     * @return Koosh
     */
    public function setFadeType($fadeType)
    {
        $this->fadeType = $fadeType;

        return $this;
    }

    /**
     * Get fadeType
     *
     * @return string 
     */
    public function getFadeType()
    {
        return $this->fadeType;
    }
    
    /**
     * Set processStarted
     *
     * @param string $processStarted
     * @return Koosh
     */
    public function setProcessStarted($processStarted)
    {
        $this->processStarted = $processStarted;

        return $this;
    }

    /**
     * Get processStarted
     *
     * @return string 
     */
    public function getProcessStarted()
    {
        return $this->processStarted;
    }
    
    /**
     * Set processEnded
     *
     * @param string $processEnded
     * @return Koosh
     */
    public function setProcessEnded($processEnded)
    {
        $this->processEnded = $processEnded;

        return $this;
    }

    /**
     * Get procesEnded
     *
     * @return string 
     */
    public function getProcessEnded()
    {
        return $this->processEnded;
    }
    
    /**
     * Set rabbitQueueAdded
     *
     * @param string $rabbitQueueAdded
     * @return Koosh
     */
    public function setRabbitQueueAdded($rabbitQueueAdded)
    {
        $this->rabbitQueueAdded = $rabbitQueueAdded;

        return $this;
    }

    /**
     * Get rabbitQueueAdded
     *
     * @return string 
     */
    public function getRabbitQueueAdded()
    {
        return $this->rabbitQueueAdded;
    }
    
    /**
     * Set addAudio
     *
     * @param string $addAudio
     * @return Video
     */
    public function setAddAudio($addAudio)
    {
        $this->addAudio = $addAudio;

        return $this;
    }

    /**
     * Get addAudio
     *
     * @return integer 
     */
    public function getAddAudio()
    {
        return $this->addAudio;
    }

    /**
     * Get audioFile
     *
     * @return string 
     */
    public function getAudioFile()
    {
        return $this->audioFile;
    }

    /**
     * Set audioFilePath
     *
     * @param string $audioFilePath
     * @return Video
     */
    public function setAudioFilePath($audioFilePath)
    {
        $this->audioFilePath = $audioFilePath;

        return $this;
    }

    /**
     * Get audioFilePath
     *
     * @return string 
     */
    public function getAudioFilePath()
    {
        return $this->audioFilePath;
    }


    
}
