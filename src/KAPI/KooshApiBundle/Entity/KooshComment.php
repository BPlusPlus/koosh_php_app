<?php

namespace KAPI\KooshApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity
 * @ORM\Table(name="kooshComment")
 * @ORM\HasLifecycleCallbacks()
 * @Serializer\ExclusionPolicy("all")
 */
class KooshComment
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\Column(type="integer")
     * 
     */
    protected $kooshId;

    /**
     * @ORM\Column(type="integer")
     * 
     */
    protected $userId;
    
    /**
     * @ORM\Column(type="string")
     * 
     * @Assert\NotBlank()
     */
    protected $text;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $created;
    
    /**
     * @ORM\Column(type="datetime")
     */
    protected $updated;
    
    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="comments")
     * @ORM\JoinColumn(name="userId", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $user;
    
    /**
     * @ORM\ManyToOne(targetEntity="Koosh", inversedBy="comments")
     * @ORM\JoinColumn(name="kooshId", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $koosh;
    
    /**
     * @ORM\OneToMany(targetEntity="KooshCommentTag", mappedBy="kooshComment")
     */
    protected $tags;
    
    /** @ORM\PrePersist */
    public function doStuffOnPrePersist()
    {
        $this->created = new \DateTime();
        $this->updated = new \DateTime();
    }
    
    /** @ORM\PreUpdate */
    public function doStuffOnPreUpdate()
    {
        $this->updated = new \DateTime();
    }
    
    
    public function toString()
    {
        return $this->id;
    }
 
    public function __toString()
    {
        $userName = is_object($this->user) ? $this->user->getUsername() : 'unknown';
        return $userName . ' - ' . $this->text;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set kooshId
     *
     * @param integer $kooshId
     * @return KooshComment
     */
    public function setKooshId($kooshId)
    {
        $this->kooshId = $kooshId;

        return $this;
    }

    /**
     * Get kooshId
     *
     * @return integer 
     */
    public function getKooshId()
    {
        return $this->kooshId;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     * @return KooshComment
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer 
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set text
     *
     * @param string $text
     * @return KooshComment
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string 
     */
    public function getText()
    {
        return $this->text;
    }


    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set user
     *
     * @param \KAPI\KooshApiBundle\Entity\User $user
     * @return KooshComment
     */
    public function setUser(\KAPI\KooshApiBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \KAPI\KooshApiBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set koosh
     *
     * @param \KAPI\KooshApiBundle\Entity\Koosh $koosh
     * @return KooshComment
     */
    public function setKoosh(\KAPI\KooshApiBundle\Entity\Koosh $koosh = null)
    {
        $this->koosh = $koosh;

        return $this;
    }

    /**
     * Get koosh
     *
     * @return \KAPI\KooshApiBundle\Entity\Koosh 
     */
    public function getKoosh()
    {
        return $this->koosh;
    }


    /**
     * Get updated
     *
     * @return \DateTime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return KooshComment
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return KooshComment
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->tags = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add tags
     *
     * @param \KAPI\KooshApiBundle\Entity\KooshCommentTag $tags
     * @return KooshComment
     */
    public function addTag(\KAPI\KooshApiBundle\Entity\KooshCommentTag $tags)
    {
        $this->tags[] = $tags;

        return $this;
    }

    /**
     * Remove tags
     *
     * @param \KAPI\KooshApiBundle\Entity\KooshCommentTag $tags
     */
    public function removeTag(\KAPI\KooshApiBundle\Entity\KooshCommentTag $tags)
    {
        $this->tags->removeElement($tags);
    }

    /**
     * Get tags
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTags()
    {
        return $this->tags;
    }
}
