<?php

namespace KAPI\KooshApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity
 * @ORM\Table(name="kooshCommentTag")
 * @ORM\HasLifecycleCallbacks()
 * @Serializer\ExclusionPolicy("all")
 */
class KooshCommentTag
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\Column(type="integer")
     * 
     */
    protected $kooshCommentId;

    /**
     * @ORM\Column(type="integer")
     * 
     */
    protected $userId;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $created;
    
    /**
     * @ORM\Column(type="datetime")
     */
    protected $updated;
    
    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="tags")
     * @ORM\JoinColumn(name="userId", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $user;
    
    /**
     * @ORM\ManyToOne(targetEntity="KooshComment", inversedBy="tags")
     * @ORM\JoinColumn(name="kooshCommentId", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $kooshComment;
    
    /** @ORM\PrePersist */
    public function doStuffOnPrePersist()
    {
        $this->created = new \DateTime();
        $this->updated = new \DateTime();
    }
    
    /** @ORM\PreUpdate */
    public function doStuffOnPreUpdate()
    {
        $this->updated = new \DateTime();
    }
    
    
    public function toString()
    {
        return $this->id;
    }
 
    public function __toString()
    {
        $userName = is_object($this->user) ? $this->user->getUsername() : 'unknown';
        return $userName . ' - ' . $this->text;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set kooshCommentId
     *
     * @param integer $kooshCommentId
     * @return KooshCommentTag
     */
    public function setKooshCommentId($kooshCommentId)
    {
        $this->kooshCommentId = $kooshCommentId;

        return $this;
    }

    /**
     * Get kooshCommentId
     *
     * @return integer 
     */
    public function getKooshCommentId()
    {
        return $this->kooshCommentId;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     * @return KooshComment
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer 
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set user
     *
     * @param \KAPI\KooshApiBundle\Entity\User $user
     * @return KooshComment
     */
    public function setUser(\KAPI\KooshApiBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \KAPI\KooshApiBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set kooshComment
     *
     * @param \KAPI\KooshApiBundle\Entity\KooshComment $kooshComment
     * @return KooshCommentTag
     */
    public function setKooshComment(\KAPI\KooshApiBundle\Entity\KooshComment $kooshComment = null)
    {
        $this->kooshComment = $kooshComment;

        return $this;
    }

    /**
     * Get kooshComment
     *
     * @return \KAPI\KooshApiBundle\Entity\KooshComment 
     */
    public function getKooshComment()
    {
        return $this->kooshComment;
    }


    /**
     * Get updated
     *
     * @return \DateTime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return KooshComment
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return KooshComment
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }
}
