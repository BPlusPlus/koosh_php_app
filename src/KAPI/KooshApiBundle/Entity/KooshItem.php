<?php

namespace KAPI\KooshApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @ORM\Entity
 * @ORM\Table(name="kooshItem")
 * @ORM\HasLifecycleCallbacks()
 * @Serializer\ExclusionPolicy("all")
 */
class KooshItem
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\Column(type="string")
     * 
     * @Assert\Length(min="1", max=255)
     */
    protected $sourceType;
    
    
    /**
     * @ORM\Column(type="integer")
     * 
     */
    protected $userId;
    
    /**
     * @ORM\Column(type="integer")
     * 
     */
    protected $kooshId;
    
    /**
     * @ORM\Column(type="integer", nullable=true)
     * 
     */
    protected $videoId;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * 
     */
    protected $imageId;
    
    
    /**
     * @ORM\Column(type="datetime")
     */
    protected $created;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $updated;
    
    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="videos")
     * @ORM\JoinColumn(name="userId", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $user;
    
    /**
     * @ORM\ManyToOne(targetEntity="Koosh", inversedBy="videos")
     * @ORM\JoinColumn(name="kooshId", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $koosh;
    
    /**
     * @ORM\ManyToOne(targetEntity="Video", inversedBy="kooshItems")
     * @ORM\JoinColumn(name="videoId", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $video;
    
    /**
     * @ORM\ManyToOne(targetEntity="Image", inversedBy="kooshItems")
     * @ORM\JoinColumn(name="imageId", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $image;
    
    
    /** @ORM\PrePersist */
    public function doStuffOnPrePersist()
    {
        $this->created = new \DateTime();
        $this->updated = new \DateTime();
    }
    
    /** @ORM\PreUpdate */
    public function doStuffOnPreUpdate()
    {
        $this->updated = new \DateTime();
    }
    
     
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->videoLikes = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Set sourceType
     *
     * @param string $sourceType
     * @return KooshItem
     */
    public function setSourceType($sourceType)
    {
        $this->sourceType = $sourceType;

        return $this;
    }

    /**
     * Get sourceType
     *
     * @return string 
     */
    public function getSourceType()
    {
        return $this->sourceType;
    }
    
    /**
     * Set userId
     *
     * @param integer $userId
     * @return KooshItem
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer 
     */
    public function getUserId()
    {
        return $this->userId;
    }
    

    /**
     * Set kooshId
     *
     * @param integer $kooshId
     * @return KooshItem
     */
    public function setKooshId($kooshId)
    {
        $this->kooshId = $kooshId;

        return $this;
    }

    /**
     * Get kooshId
     *
     * @return integer 
     */
    public function getKooshId()
    {
        return $this->kooshId;
    }

    /**
     * Set videoId
     *
     * @param integer $videoId
     * @return KooshItem
     */
    public function setVideoId($videoId)
    {
        $this->videoId = $videoId;

        return $this;
    }

    /**
     * Get videoId
     *
     * @return integer 
     */
    public function getVideoId()
    {
        return $this->videoId;
    }
    
    /**
     * Set imageId
     *
     * @param integer $imageId
     * @return KooshItem
     */
    public function setImageId($imageId)
    {
        $this->imageId = $imageId;

        return $this;
    }

    /**
     * Get imageId
     *
     * @return integer 
     */
    public function getImageId()
    {
        return $this->imageId;
    }
    
    /**
     * Set video
     *
     * @param \KAPI\KooshApiBundle\Entity\Video $video
     * @return KooshItem
     */
    public function setVideo(\KAPI\KooshApiBundle\Entity\Video $video = null)
    {
        $this->video = $video;

        return $this;
    }

    /**
     * Get video
     *
     * @return \KAPI\KooshApiBundle\Entity\Video 
     */
    public function getVideo()
    {
        return $this->video;
    }
    
    /**
     * Set image
     *
     * @param \KAPI\KooshApiBundle\Entity\Image $image
     * @return KooshItem
     */
    public function setImage(\KAPI\KooshApiBundle\Entity\Image $image = null)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return \KAPI\KooshApiBundle\Entity\Image
     */
    public function getImage()
    {
        return $this->image;
    }
    
    /**
     * Set image
     *
     * @param \KAPI\KooshApiBundle\Entity\User $image
     * @return KooshItem
     */
    public function setUser(\KAPI\KooshApiBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \KAPI\KooshApiBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
    
    /**
     * Set koosh
     *
     * @param \KAPI\KooshApiBundle\Entity\Koosh $koosh
     * @return KooshItem
     */
    public function setKoosh(\KAPI\KooshApiBundle\Entity\Koosh $koosh = null)
    {
        $this->koosh = $koosh;

        return $this;
    }

    /**
     * Get koosh
     *
     * @return \KAPI\KooshApiBundle\Entity\Koosh
     */
    public function getKoosh()
    {
        return $this->koosh;
    }
}
