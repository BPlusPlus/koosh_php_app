<?php

namespace KAPI\KooshApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity
 * @ORM\Table(name="notification")
 * @ORM\HasLifecycleCallbacks()
 * @Serializer\ExclusionPolicy("all")
 */
class Notification
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\Column(type="integer", nullable=true)
     * 
     * @Assert\NotBlank()
     */
    protected $kooshId;
    
    /**
     * @ORM\Column(type="string", nullable=true)
     * 
     */
    protected $noteMessage;
    
    /**
     * @ORM\Column(type="integer", nullable=true)
     * 
     */
    protected $permanent;
    
    /**
     * @ORM\Column(type="integer")
     * 
     * @Assert\NotBlank()
     */
    protected $userIdFrom;

    /**
     * @ORM\Column(type="integer")
     * 
     * @Assert\NotBlank()
     */
    protected $userIdTo;
    
    /**
     * @ORM\Column(type="integer", nullable=true)
     * 
     */
    protected $requestId;
    
    /**
     * @ORM\Column(type="string")
     */
    protected $type;
    
    /**
     * @ORM\Column(type="integer", nullable=true)
     * 
     */
    protected $amount;
    
    /**
     * @ORM\Column(type="datetime")
     */
    protected $created;
 
    /**
     * @ORM\ManyToOne(targetEntity="Koosh", inversedBy="notifications")
     * @ORM\JoinColumn(name="kooshId", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $koosh;
    
    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="friendshipFrom")
     * @ORM\JoinColumn(name="userIdFrom", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $userFrom;
    
    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="friendshipTo")
     * @ORM\JoinColumn(name="userIdTo", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $userTo;
    
    public function toString()
    {
        return $this->id;
    }
    
    public function __toString()
    {
        return (string)$this->id;
    }
    
    /** @ORM\PrePersist */
    public function doStuffOnPrePersist()
    {
        $this->created = new \DateTime();
    }
    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set kooshId
     *
     * @param integer $kooshId
     * @return Notification
     */
    public function setKooshId($kooshId)
    {
        $this->kooshId = $kooshId;

        return $this;
    }

    /**
     * Get kooshId
     *
     * @return integer 
     */
    public function getKooshId()
    {
        return $this->kooshId;
    }

    /**
     * Set userIdFrom
     *
     * @param integer $userIdFrom
     * @return Notification
     */
    public function setUserIdFrom($userIdFrom)
    {
        $this->userIdFrom = $userIdFrom;

        return $this;
    }

    /**
     * Get userIdFrom
     *
     * @return integer 
     */
    public function getUserIdFrom()
    {
        return $this->userIdFrom;
    }

    /**
     * Set userIdTo
     *
     * @param integer $userIdTo
     * @return Notification
     */
    public function setUserIdTo($userIdTo)
    {
        $this->userIdTo = $userIdTo;

        return $this;
    }

    /**
     * Get userIdTo
     *
     * @return integer 
     */
    public function getUserIdTo()
    {
        return $this->userIdTo;
    }

    /**
     * Set type
     *
     * @param integer $type
     * @return Notification
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Notification
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set koosh
     *
     * @param \KAPI\KooshApiBundle\Entity\Koosh $koosh
     * @return Notification
     */
    public function setKoosh(\KAPI\KooshApiBundle\Entity\Koosh $koosh = null)
    {
        $this->koosh = $koosh;

        return $this;
    }

    /**
     * Get koosh
     *
     * @return \KAPI\KooshApiBundle\Entity\Koosh 
     */
    public function getKoosh()
    {
        return $this->koosh;
    }

    /**
     * Set userFrom
     *
     * @param \KAPI\KooshApiBundle\Entity\User $userFrom
     * @return Notification
     */
    public function setUserFrom(\KAPI\KooshApiBundle\Entity\User $userFrom = null)
    {
        $this->userFrom = $userFrom;

        return $this;
    }

    /**
     * Get userFrom
     *
     * @return \KAPI\KooshApiBundle\Entity\User 
     */
    public function getUserFrom()
    {
        return $this->userFrom;
    }

    /**
     * Set userTo
     *
     * @param \KAPI\KooshApiBundle\Entity\User $userTo
     * @return Notification
     */
    public function setUserTo(\KAPI\KooshApiBundle\Entity\User $userTo = null)
    {
        $this->userTo = $userTo;

        return $this;
    }

    /**
     * Get userTo
     *
     * @return \KAPI\KooshApiBundle\Entity\User 
     */
    public function getUserTo()
    {
        return $this->userTo;
    }

    /**
     * Set noteMessage
     *
     * @param string $noteMessage
     * @return Notification
     */
    public function setNoteMessage($noteMessage)
    {
        $this->noteMessage = $noteMessage;

        return $this;
    }

    /**
     * Get noteMessage
     *
     * @return string 
     */
    public function getNoteMessage()
    {
        return $this->noteMessage;
    }

    /**
     * Set requestId
     *
     * @param integer $requestId
     * @return Notification
     */
    public function setRequestId($requestId)
    {
        $this->requestId = $requestId;

        return $this;
    }

    /**
     * Get requestId
     *
     * @return integer 
     */
    public function getRequestId()
    {
        return $this->requestId;
    }

    /**
     * Set permanent
     *
     * @param integer $permanent
     * @return Notification
     */
    public function setPermanent($permanent)
    {
        $this->permanent = $permanent;

        return $this;
    }

    /**
     * Get permanent
     *
     * @return integer 
     */
    public function getPermanent()
    {
        return $this->permanent;
    }
    
    /**
     * Set amount
     *
     * @param integer $amount
     * @return Notification
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return integer 
     */
    public function getAmount()
    {
        return $this->amount;
    }
}
