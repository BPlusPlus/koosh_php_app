<?php

namespace KAPI\KooshApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;


/**
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="KAPI\KooshApiBundle\Repository\PrivateMessageRepository")
 * @ORM\Table(name="privateMessage")
 * @ORM\HasLifecycleCallbacks()
 * @Serializer\ExclusionPolicy("all")
 */
class PrivateMessage
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\Column(type="text", nullable=true)
     * 
     */
    protected $message;
    
    /**
     * @ORM\Column(type="integer")
     * 
     */
    protected $userIdFrom;

    /**
     * @ORM\Column(type="integer")
     * 
     */
    protected $userIdTo;
    
    /**
     * @ORM\Column(type="datetime")
     */
    protected $created;
    
    /**
     * @ORM\Column(type="datetime")
     */
    protected $updated;
    
    /**
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="User", inversedBy="friendshipFrom")
     * @ORM\JoinColumn(name="userIdFrom", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $userFrom;
    
    /**
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="User", inversedBy="friendshipTo")
     * @ORM\JoinColumn(name="userIdTo", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $userTo;
    
    public function toString()
    {
        return $this->id;
    }
    
    public function __toString()
    {
        return (string)$this->id;
    }
    
    /** @ORM\PrePersist */
    public function doStuffOnPrePersist()
    {
        $this->created = new \DateTime();
        $this->updated = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set message
     *
     * @param string $message
     * @return PrivateMessage
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string 
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set userIdFrom
     *
     * @param integer $userIdFrom
     * @return PrivateMessage
     */
    public function setUserIdFrom($userIdFrom)
    {
        $this->userIdFrom = $userIdFrom;

        return $this;
    }

    /**
     * Get userIdFrom
     *
     * @return integer 
     */
    public function getUserIdFrom()
    {
        return $this->userIdFrom;
    }

    /**
     * Set userIdTo
     *
     * @param integer $userIdTo
     * @return PrivateMessage
     */
    public function setUserIdTo($userIdTo)
    {
        $this->userIdTo = $userIdTo;

        return $this;
    }

    /**
     * Get userIdTo
     *
     * @return integer 
     */
    public function getUserIdTo()
    {
        return $this->userIdTo;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return PrivateMessage
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set userFrom
     *
     * @param \KAPI\KooshApiBundle\Entity\User $userFrom
     * @return PrivateMessage
     */
    public function setUserFrom(\KAPI\KooshApiBundle\Entity\User $userFrom = null)
    {
        $this->userFrom = $userFrom;

        return $this;
    }

    /**
     * Get userFrom
     *
     * @return \KAPI\KooshApiBundle\Entity\User 
     */
    public function getUserFrom()
    {
        return $this->userFrom;
    }

    /**
     * Set userTo
     *
     * @param \KAPI\KooshApiBundle\Entity\User $userTo
     * @return PrivateMessage
     */
    public function setUserTo(\KAPI\KooshApiBundle\Entity\User $userTo = null)
    {
        $this->userTo = $userTo;

        return $this;
    }

    /**
     * Get userTo
     *
     * @return \KAPI\KooshApiBundle\Entity\User 
     */
    public function getUserTo()
    {
        return $this->userTo;
    }
    
    /**
     * Get updated
     *
     * @return \DateTime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }
    

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return PrivateMessage
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }
}
