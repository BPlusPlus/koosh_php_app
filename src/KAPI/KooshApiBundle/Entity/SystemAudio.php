<?php

namespace KAPI\KooshApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @ORM\Entity
 * @ORM\Table(name="systemAudio")
 * @ORM\HasLifecycleCallbacks()
 * @Serializer\ExclusionPolicy("all")
 */
class SystemAudio
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\Column(type="string")
     * 
     * @Assert\Length(min="1", max=255)
     */
    protected $title;

    /**
     * @ORM\Column(type="string", nullable=true)
     * 
     */
    protected $audioFile;
    
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $audioFilePath;
    
    /**
     * @ORM\Column(type="integer", options={"default":0})
     * 
     */
    protected $status;
    
    /**
     * @ORM\Column(type="datetime")
     */
    protected $created;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $updated;
    
    /**
     * @ORM\OneToMany(targetEntity="Koosh", mappedBy="systemAudio")
     */
    protected $kooshes;
    
    /**
     * @ORM\OneToMany(targetEntity="Video", mappedBy="systemAudio")
     */
    protected $videos;
    
    /**
     * @ORM\OneToMany(targetEntity="Image", mappedBy="systemAudio")
     */
    protected $images;
    
    /** @ORM\PrePersist */
    public function doStuffOnPrePersist()
    {
        $this->created = new \DateTime();
        $this->updated = new \DateTime();
    }
    
    /** @ORM\PreUpdate */
    public function doStuffOnPreUpdate()
    {
        $this->updated = new \DateTime();
    }
    
    public function toString()
    {
        return $this->title;
    }

    public function __toString() 
    {
        return $this->title;
    }
     
    /**
     * Constructor
     */
    public function __construct()
    {
        
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Video
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }


    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Video
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return Video
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    
    // YourNS/YourBundle/Entity/Image.php
    const SERVER_PATH_TO_IMAGE_FOLDER = 'uploads/system_audio';


    public function getAbsolutePath()
    {
        return $this->getUploadRootDir().'/';
    }
    
    public function getAudioAbsolutePath()
    {
        return $this->getAudioUploadRootDir().'/';
    }
    
    public function getAudioFileAbsolutePath()
    {
        return null === $this->audioFilePath
            ? null
            : $this->getAudioUploadRootDir().'/'.$this->audioFilePath;
    }

    public function getAudioFileHelperAbsolutePath($koosh)
    {
        return null === $this->audioFilePath
            ? null
            : $koosh->getHelpersAbsolutePath().$this->audioFilePath;
    }
    
    public function getAudioFileWebPath()
    {
        return null === $this->audioFilePath
            ? null
            : $this->getAudioUploadDir().'/'.$this->audioFilePath;
    }
    
    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__.'/../../../../web/'.$this->getUploadDir();
    }
    
    protected function getAudioUploadRootDir() {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__.'/../../../../web/'.$this->getAudioUploadDir();
    }
    
    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'uploads/video';
    }
    
    protected function getAudioUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'uploads/system_audio';
    }
    
    private $audioFileTemp;


    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setAudioFile(UploadedFile $file = null)
    {
        $this->audioFile = $file;
        // check if we have an old image videoFilePath
        if (isset($this->audioFilePath)) {
            // store the old name to delete after the update
            $this->audioFileTemp = $this->audioFilePath;
            $this->audioFilePath = null;
        } else {
            $this->audioFilePath = 'initial';
        }
    }
    
    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        
        if (null !== $this->getAudioFile() && (is_object($this->getAudioFile()))) {
            // do whatever you want to generate a unique name
            $filename = sha1(uniqid(mt_rand(), true));      
            $this->audioFilePath = $filename.'.mp3';
        } else {
            $this->audioFilePath = $this->audioFileTemp;
        }        
    }


    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function uploadAudio()
    {
        if ((null === $this->getAudioFile()) || !is_object($this->getAudioFile())) {
            return;
        }
        
        // if there is an error when moving the file, an exception will
        // be automatically thrown by move(). This will properly prevent
        // the entity from being persisted to the database on error
        $this->getAudioFile()->move($this->getAudioUploadRootDir(), $this->audioFilePath);

        // check if we have an old image
        if (isset($this->audioFileTemp)) {
            // delete the old image
            unlink($this->getAudioUploadRootDir().'/'.$this->audioFileTemp);
            // clear the videoFileTemp image videoFilePath
            $this->audioFileTemp = null;
        }
        //$this->audioFile = null;
    }
    
    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        
        if(($audioFile = $this->getAudioFileAbsolutePath()) && is_file($audioFile)) {
            unlink($audioFile);
        }
        
    }


    /**
     * Set path
     *
     * @param string $path
     * @return User
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string 
     */
    public function getPath()
    {
        return $this->path;
    }

    
    public function getActionField() {
        return '<a href="">EDIT</a>';
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Video
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    
    /**
     * Set addAudio
     *
     * @param integer $addAudio
     * @return Video
     */
    public function setAddAudio($addAudio)
    {
        $this->addAudio = $addAudio;

        return $this;
    }

    /**
     * Get addAudio
     *
     * @return integer 
     */
    public function getAddAudio()
    {
        return $this->addAudio;
    }

  

    /**
     * Get audioFile
     *
     * @return string 
     */
    public function getAudioFile()
    {
        return $this->audioFile;
    }

    /**
     * Set audioFilePath
     *
     * @param string $audioFilePath
     * @return Video
     */
    public function setAudioFilePath($audioFilePath)
    {
        $this->audioFilePath = $audioFilePath;

        return $this;
    }

    /**
     * Get audioFilePath
     *
     * @return string 
     */
    public function getAudioFilePath()
    {
        return $this->audioFilePath;
    }
}
