<?php

namespace KAPI\KooshApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @ORM\Entity(repositoryClass="KAPI\KooshApiBundle\Repository\KooshRepository")
 * @ORM\Table(name="transition")
 * @ORM\HasLifecycleCallbacks()
 * @Serializer\ExclusionPolicy("all")
 * 
 */
class Transition
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\Column(type="string")
     * 
     * @Assert\NotBlank()
     * @Assert\Length(min="1", max=255)
     */
    protected $title;
    
    
    /**
     * @ORM\Column(type="string")
     * 
     * @Assert\NotBlank()
     * @Assert\Length(min="1", max=255)
     */
    protected $code;
    
    /**
     * @ORM\Column(type="string", nullable=true)
     * 
     */
    protected $description;

    /**
     * @ORM\Column(type="string", nullable=true)
     * 
     */
    protected $maskImage;

    /**
     * @ORM\Column(type="integer")
     */
    protected $status;
    
    /**
     * @ORM\Column(type="datetime")
     */
    protected $created;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $updated;
    
    
    /** @ORM\PrePersist */
    public function doStuffOnPrePersist()
    {
        $this->created = new \DateTime();
        $this->updated = new \DateTime();
        
        $this->status = 0;
    }
    
    /** @ORM\PreUpdate */
    public function doStuffOnPreUpdate()
    {
        $this->updated = new \DateTime();
    }
    
    public function toString()
    {
        return $this->title;
    }

    public function __toString()
    {
        return $this->title;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param mixed $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getMaskImage()
    {
        return $this->maskImage;
    }

    /**
     * @param mixed $maskImage
     */
    public function setMaskImage($maskImage)
    {
        $this->maskImage = $maskImage;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param mixed $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return mixed
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param mixed $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }


    public function getAudioFileWebPath()
    {
        return null === $this->audioFilePath
            ? null
            : $this->getUploadDir().'/'.$this->audioFilePath;
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__.'/../../../../web/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'uploads/transitions';
    }

    public function getTransitionMaskFilePath()
    {
        return __DIR__.'/../../../../web/'.$this->getUploadDir();;
    }
    
    public function getTransitionMaskFileAbsolutePath()
    {
        return $this->getTransitionMaskFilePath().'/'.$this->maskImage;
    }

    public function getTransitionMaskFileHelperAbsolutePath($koosh)
    {
        return $koosh->getHelpersAbsolutePath() .$this->maskImage;
    }

    public function getMaskImageFileWebPath() {
        return null === $this->maskImage
            ? null
            : $this->getUploadDir().'/'.$this->maskImage;
    }

    public function getMaskImagePreviewWebPath() {
        return null === $this->maskImage
            ? null
            : $this->getUploadDir().'/preview/'.str_replace('.pgm', '.jpg', $this->maskImage);
    }
}
