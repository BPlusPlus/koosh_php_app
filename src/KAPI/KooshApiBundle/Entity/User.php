<?php

namespace KAPI\KooshApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\HttpFoundation\File\UploadedFile;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="KAPI\KooshApiBundle\Repository\UserRepository")
 * @ORM\Table(name="user")
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity(fields={"username", "email"})
 * @Serializer\ExclusionPolicy("all")
 */
class User
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\Column(type="string")
     * 
     * @Assert\NotBlank()
     * @Assert\Length(min="1", max=255)
     */
    protected $firstName;

    /**
     * @ORM\Column(type="string", unique=true)
     * 
     * @Assert\NotBlank()
     * @Assert\Length(min="1", max=255)
     */
    protected $username;
    
    /**
     * @ORM\Column(type="string")
     * 
     * @Assert\NotBlank()
     * @Assert\Length(min="1", max=255)
     */
    protected $lastName;

    /**
     * @ORM\Column(type="string", unique=true)
     * 
     * @Assert\NotBlank()
     * @Assert\Email(
     *     message = "The email '{{ value }}' is not a valid email.",
     *     checkMX = true
     * )
     */
    protected $email;
    
    /**
     * @ORM\Column(type="string")
     * 
     */
    protected $password;
    
    /**
     * @ORM\Column(type="string")
     * 
     */
    protected $salt;
    
    /**
     * @ORM\Column(type="datetime")
     */
    protected $dob;
    
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $pushId;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $image;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $path;
    
    /**
     * @ORM\Column(type="float", nullable=true)
     * 
     */
    protected $latitude;
    
    /**
     * @ORM\Column(type="float", nullable=true)
     * 
     */
    protected $longitude;
    
    /**
     * @ORM\Column(type="integer", nullable=true)
     * 
     */
    protected $fameValue;
    
    /**
     * @ORM\Column(type="string", nullable=true)
     * 
     */
    protected $facebookId;
    
    /**
     * @ORM\Column(type="string", nullable=true)
     * 
     */
    protected $twitterId;
    
    /**
     * @ORM\Column(type="smallint", nullable=true, options={"default":0})
     * 
     */
    protected $nudged;
    
    /**
     * @ORM\Column(type="datetime")
     */
    protected $created;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $updated;
 
    /**
     * @ORM\OneToMany(targetEntity="Koosh", mappedBy="user", cascade={"all"})
     */
    protected $kooshes;
    
    /**
     * @ORM\OneToMany(targetEntity="Friendship", mappedBy="userFrom")
     */
    protected $friendshipFrom;
    
    /**
     * @ORM\OneToMany(targetEntity="Friendship", mappedBy="userTo")
     */
    protected $friendshipTo;
    
    /**
     * @ORM\OneToMany(targetEntity="KooshComment", mappedBy="user")
     */
    protected $comments;
    
    /**
     * @ORM\OneToMany(targetEntity="KooshCommentTag", mappedBy="user")
     */
    protected $tags;
    
    /**
     * @ORM\OneToMany(targetEntity="Video", mappedBy="user")
     */
    protected $videos;
    
    /**
     * @ORM\OneToMany(targetEntity="Image", mappedBy="user")
     */
    protected $images;
    
    /**
     * @ORM\OneToMany(targetEntity="KooshItem", mappedBy="user")
     */
    protected $kooshItems;
    
    /**
     * @ORM\OneToMany(targetEntity="KooshLike", mappedBy="user")
     */
    protected $kooshLikes;
    
    /** @ORM\PrePersist */
    public function doStuffOnPrePersist()
    {
        $this->created = new \DateTime();
        $this->updated = new \DateTime();
        
        $this->nudged = 0;

    }
    
    /** @ORM\PreUpdate */
    public function doStuffOnPreUpdate()
    {
        $this->updated = new \DateTime();
    }
    
    public function toString()
    {
        return $this->id;
    }
    
    public function __toString() 
    {
        return (string)$this->username;
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     * @return User
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string 
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set username
     *
     * @param string $username
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string 
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string 
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set companyName
     *
     * @param string $companyName
     * @return User
     */
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;

        return $this;
    }

    /**
     * Get companyName
     *
     * @return string 
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * Set dob
     *
     * @param \DateTime $dob
     * @return User
     */
    public function setDob($dob)
    {
        $this->dob = $dob;

        return $this;
    }

    /**
     * Get dob
     *
     * @return \DateTime 
     */
    public function getDob()
    {
        return $this->dob;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return User
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return User
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->kooshes = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add kooshes
     *
     * @param \KAPI\KooshApiBundle\Entity\Koosh $kooshes
     * @return User
     */
    public function addKoosh(\KAPI\KooshApiBundle\Entity\Koosh $kooshes)
    {
        $this->kooshes[] = $kooshes;

        return $this;
    }

    /**
     * Remove kooshes
     *
     * @param \KAPI\KooshApiBundle\Entity\Koosh $kooshes
     */
    public function removeKoosh(\KAPI\KooshApiBundle\Entity\Koosh $kooshes)
    {
        $this->kooshes->removeElement($kooshes);
    }

    /**
     * Get kooshes
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getKooshes()
    {
        return $this->kooshes;
    }

    /**
     * Add friendshipFrom
     *
     * @param \KAPI\KooshApiBundle\Entity\Friendship $friendshipFrom
     * @return User
     */
    public function addFriendshipFrom(\KAPI\KooshApiBundle\Entity\Friendship $friendshipFrom)
    {
        $this->friendshipFrom[] = $friendshipFrom;

        return $this;
    }

    /**
     * Remove friendshipFrom
     *
     * @param \KAPI\KooshApiBundle\Entity\Friendship $friendshipFrom
     */
    public function removeFriendshipFrom(\KAPI\KooshApiBundle\Entity\Friendship $friendshipFrom)
    {
        $this->friendshipFrom->removeElement($friendshipFrom);
    }

    /**
     * Get friendshipFrom
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFriendshipFrom()
    {
        return $this->friendshipFrom;
    }

    /**
     * Add friendshipTo
     *
     * @param \KAPI\KooshApiBundle\Entity\Friendship $friendshipTo
     * @return User
     */
    public function addFriendshipTo(\KAPI\KooshApiBundle\Entity\Friendship $friendshipTo)
    {
        $this->friendshipTo[] = $friendshipTo;

        return $this;
    }

    /**
     * Remove friendshipTo
     *
     * @param \KAPI\KooshApiBundle\Entity\Friendship $friendshipTo
     */
    public function removeFriendshipTo(\KAPI\KooshApiBundle\Entity\Friendship $friendshipTo)
    {
        $this->friendshipTo->removeElement($friendshipTo);
    }

    /**
     * Get friendshipTo
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFriendshipTo()
    {
        return $this->friendshipTo;
    }

    /**
     * Add comments
     *
     * @param \KAPI\KooshApiBundle\Entity\KooshComment $comments
     * @return User
     */
    public function addComment(\KAPI\KooshApiBundle\Entity\KooshComment $comments)
    {
        $this->comments[] = $comments;

        return $this;
    }

    /**
     * Remove comments
     *
     * @param \KAPI\KooshApiBundle\Entity\KooshComment $comments
     */
    public function removeComment(\KAPI\KooshApiBundle\Entity\KooshComment $comments)
    {
        $this->comments->removeElement($comments);
    }

    /**
     * Get comments
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Add kooshLikes
     *
     * @param \KAPI\KooshApiBundle\Entity\KooshLike $kooshLikes
     * @return User
     */
    public function addKooshLike(\KAPI\KooshApiBundle\Entity\KooshLike $kooshLikes)
    {
        $this->kooshLikes[] = $kooshLikes;

        return $this;
    }

    /**
     * Remove kooshLikes
     *
     * @param \KAPI\KooshApiBundle\Entity\KooshLike $kooshLikes
     */
    public function removeKooshLike(\KAPI\KooshApiBundle\Entity\KooshLike $kooshLikes)
    {
        $this->kooshLikes->removeElement($kooshLikes);
    }

    /**
     * Get kooshLikes
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getKooshLikes()
    {
        return $this->kooshLikes;
    }
    
    // YourNS/YourBundle/Entity/Image.php
    const SERVER_PATH_TO_IMAGE_FOLDER = 'uploads/user';

    
    public function geAlphaMaskAbsolutePath() 
    {
       return __DIR__.'/../../../../web/uploads/alpha_mask/profile_alpha_mask.png';
    }

    public function getAbsolutePath()
    {
        return null === $this->path
            ? null
            : $this->getUploadRootDir().'/'.$this->id.'.'.$this->path;
    }

    public function getImageWebPath()
    {
        return null === $this->path
            ? null
            : $this->getUploadDir().'/'.$this->path;
    }
    
    public function getWebPath()
    {
        return null === $this->path
            ? null
            : $this->getUploadDir().'/'.$this->path;
    }

    public function getImageAbsolutePath()
    {
         return null === $this->path
            ? null
            : $this->getUploadRootDir().'/'.$this->path;
    }

    public function getRoundCorneredImageAbsolutePath()
    {
         return null === $this->path
            ? null
            : $this->getUploadRootDir().'/rc_'.$this->path.'.png';
    }
    
    public function getNameImageAbsolutePath()
    {
         return null === $this->path
            ? null
            : $this->getUploadRootDir().'/ni_'.$this->path.'.png';
    }

    public function getRoundCorneredWatermarkImageHelperPath($koosh)
    {
        return null === $this->path
            ? null
            : $koosh->getHelpersAbsolutePath().'rc_'.$this->path.'.png';
    }

    public function getNameWatermarkImageHelperPath($koosh) {
        return null === $this->path
            ? null
            : $koosh->getHelpersAbsolutePath().'ni_'.$this->path.'.png';
    }
    
    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__.'/../../../../web/'.$this->getUploadDir();
    }
    
    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'uploads/user';
    }
    
    
   private $temp;

    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setImage(UploadedFile $file = null)
    {
        $this->image = $file;
        // check if we have an old image path
        if (isset($this->path)) {
            // store the old name to delete after the update
            $this->temp = $this->path;
            $this->path = null;
        } else {
            $this->path = 'initial';
        }
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        if (null !== $this->getImage()) {
            // do whatever you want to generate a unique name
            $filename = sha1(uniqid(mt_rand(), true));
            $this->path = $filename.'.'.$this->getImage()->guessExtension();
        } else {
            $this->path = $this->temp;
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if (null === $this->getImage()) {
            return;
        }

        // if there is an error when moving the file, an exception will
        // be automatically thrown by move(). This will properly prevent
        // the entity from being persisted to the database on error
        $this->getImage()->move($this->getUploadRootDir(), $this->path);

        // check if we have an old image
        if (isset($this->temp)) {
            // delete the old image
            unlink($this->getUploadRootDir().'/'.$this->temp);
            // clear the temp image path
            $this->temp = null;
        }
        $this->image = null;
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        if ($file = $this->getAbsolutePath()) {
            if(is_file($file)) {
                unlink($file);
            }
        }
    }


    /**
     * Set path
     *
     * @param string $path
     * @return User
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string 
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Add videos
     *
     * @param \KAPI\KooshApiBundle\Entity\Video $videos
     * @return User
     */
    public function addVideo(\KAPI\KooshApiBundle\Entity\Video $videos)
    {
        $this->videos[] = $videos;

        return $this;
    }

    /**
     * Remove videos
     *
     * @param \KAPI\KooshApiBundle\Entity\Video $videos
     */
    public function removeVideo(\KAPI\KooshApiBundle\Entity\Video $videos)
    {
        $this->videos->removeElement($videos);
    }

    /**
     * Get videos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getVideos()
    {
        return $this->videos;
    }

    /**
     * Set password with auto generated salt
     *
     * @param string $rawPassword
     * @return User
     */
    public function setPassword($rawPassword)
    {
        if(!empty($rawPassword)) {
            $salt = sha1(time().rand().$this->id.$this->username);
            $password = sha1($rawPassword.$salt);
            $this->password = $password;
            $this->setSalt($salt);
        }
        
        return $this;
    }
    
    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set salt
     *
     * @param string $salt
     * @return User
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;

        return $this;
    }

    /**
     * Get salt
     *
     * @return string 
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * Set facebookId
     *
     * @param string $facebookId
     * @return User
     */
    public function setFacebookId($facebookId)
    {
        $this->facebookId = $facebookId;

        return $this;
    }

    /**
     * Get facebookId
     *
     * @return string 
     */
    public function getFacebookId()
    {
        return $this->facebookId;
    }

    /**
     * Set twitterId
     *
     * @param string $twitterId
     * @return User
     */
    public function setTwitterId($twitterId)
    {
        $this->twitterId = $twitterId;

        return $this;
    }

    /**
     * Get twitterId
     *
     * @return string 
     */
    public function getTwitterId()
    {
        return $this->twitterId;
    }

    /**
     * Set nudged
     *
     * @param integer $nudged
     * @return User
     */
    public function setNudged($nudged)
    {
        $this->nudged = $nudged;

        return $this;
    }

    /**
     * Get nudged
     *
     * @return integer 
     */
    public function getNudged()
    {
        return $this->nudged;
    }

    /**
     * Set pushId
     *
     * @param string $pushId
     * @return User
     */
    public function setPushId($pushId)
    {
        $this->pushId = $pushId;

        return $this;
    }

    /**
     * Get pushId
     *
     * @return string 
     */
    public function getPushId()
    {
        return $this->pushId;
    }

    /**
     * Add images
     *
     * @param \KAPI\KooshApiBundle\Entity\Image $images
     * @return User
     */
    public function addImage(\KAPI\KooshApiBundle\Entity\Image $images)
    {
        $this->images[] = $images;

        return $this;
    }

    /**
     * Remove images
     *
     * @param \KAPI\KooshApiBundle\Entity\Image $images
     */
    public function removeImage(\KAPI\KooshApiBundle\Entity\Image $images)
    {
        $this->images->removeElement($images);
    }

    /**
     * Get images
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getImages()
    {
        return $this->images;
    }
    
    /**
     * Set latitude
     *
     * @param integer $latitude
     * @return User
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude
     *
     * @return integer 
     */
    public function getLatitude()
    {
        return $this->latitude;
    }
    
    /**
     * Set longitude
     *
     * @param integer $longitude
     * @return User
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get longitude
     *
     * @return integer 
     */
    public function getLongitude()
    {
        return $this->longitude;
    }
    
    /**
     * @return integer
     */
    public function getFameValue()
    {
        return $this->fameValue;
    }

    /**
     * @param integer $fameValue
     */
    public function setFameValue($fameValue)
    {
        $this->fameValue = $fameValue;
    }

    /**
     * Add tags
     *
     * @param \KAPI\KooshApiBundle\Entity\KooshCommentTag $tags
     * @return User
     */
    public function addTag(\KAPI\KooshApiBundle\Entity\KooshCommentTag $tags)
    {
        $this->tags[] = $tags;

        return $this;
    }

    /**
     * Remove tags
     *
     * @param \KAPI\KooshApiBundle\Entity\KooshCommentTag $tags
     */
    public function removeTag(\KAPI\KooshApiBundle\Entity\KooshCommentTag $tags)
    {
        $this->tags->removeElement($tags);
    }

    /**
     * Get tags
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTags()
    {
        return $this->tags;
    }
}
