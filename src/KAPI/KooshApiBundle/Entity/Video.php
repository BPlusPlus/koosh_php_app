<?php

namespace KAPI\KooshApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @ORM\Entity
 * @ORM\Table(name="video")
 * @ORM\HasLifecycleCallbacks()
 * @Serializer\ExclusionPolicy("all")
 */
class Video
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\Column(type="string")
     * 
     * @Assert\Length(min="1", max=255)
     */
    protected $title;
    
    /**
     * @ORM\Column(type="string", nullable=true)
     * 
     */
    protected $caption;
    
    /**
     * @ORM\Column(type="string", nullable=true)
     * 
     */
    protected $uniqueHash;

    /**
     * @ORM\Column(type="string", nullable=true)
     * 
     */
    protected $videoFile;
    
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $videoFilePath;
    
    /**
     * @ORM\Column(type="string", nullable=true)
     * 
     */
    protected $imageFile;
    
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $imageFilePath;
    
    /**
     * @ORM\Column(type="string", nullable=true)
     * 
     */
    protected $audioFile;
    
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $audioFilePath;
    
    /**
     * @ORM\Column(type="smallint", options={"default":0}, nullable=true)
     * 
     */
    protected $addAudio;
    
    /**
     * @ORM\Column(type="integer")
     * 
     */
    protected $userId;
    
    /**
     * @ORM\Column(type="integer", nullable=true)
     * 
     */
    protected $systemAudioId;

    /**
     * @ORM\Column(type="integer")
     * 
     */
    protected $kooshId;
    
    /**
     * @ORM\Column(type="smallint", options={"default":0})
     * 
     */
    protected $muted;
    
    /**
     * @ORM\Column(type="smallint", options={"default":0})
     * 
     */
    protected $nudged;
    
    /**
     * @ORM\Column(type="integer", options={"default":0})
     * 
     */
    protected $status;
    
    /**
     * @ORM\Column(type="datetime")
     */
    protected $created;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $updated;
    
    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="videos")
     * @ORM\JoinColumn(name="userId", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $user;
    
    /**
     * @ORM\ManyToOne(targetEntity="SystemAudio", inversedBy="videos")
     * @ORM\JoinColumn(name="systemAudioId", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $systemAudio;
    
    /**
     * @ORM\ManyToOne(targetEntity="Koosh", inversedBy="videos")
     * @ORM\JoinColumn(name="kooshId", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $koosh;
    
    /**
     * @ORM\OneToMany(targetEntity="VideoLike", mappedBy="video")
     */
    protected $videoLikes;
    
        /** @ORM\PrePersist */
    public function doStuffOnPrePersist()
    {
        $this->created = new \DateTime();
        $this->updated = new \DateTime();
        
        $this->nudged = 0;
        $this->status = 0;
        $this->muted = 0;
        
        $this->uniqueHash = md5(uniqid(rand()));
    }
    
    /** @ORM\PreUpdate */
    public function doStuffOnPreUpdate()
    {
        $this->updated = new \DateTime();
    }
    
    public function toString()
    {
        return $this->title;
    }

    public function __toString() 
    {
        return $this->title;
    }
     
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->videoLikes = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Video
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Get videoFile
     *
     * @return string 
     */
    public function getVideoFile()
    {
        return $this->videoFile;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     * @return Video
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer 
     */
    public function getUserId()
    {
        return $this->userId;
    }
    
    /**
     * Set systemAudioId
     *
     * @param integer $systemAudioId
     * @return Video
     */
    public function setSystemAudioId($systemAudioId)
    {
        $this->systemAudioId = $systemAudioId;

        return $this;
    }

    /**
     * Get systemAudioId
     *
     * @return integer 
     */
    public function getSystemAudioId()
    {
        return $this->systemAudioId;
    }

    /**
     * Set kooshId
     *
     * @param integer $kooshId
     * @return Video
     */
    public function setKooshId($kooshId)
    {
        $this->kooshId = $kooshId;

        return $this;
    }

    /**
     * Get kooshId
     *
     * @return integer 
     */
    public function getKooshId()
    {
        return $this->kooshId;
    }

    /**
     * Set nudged
     *
     * @param integer $nudged
     * @return Video
     */
    public function setNudged($nudged)
    {
        $this->nudged = $nudged;

        return $this;
    }

    /**
     * Get nudged
     *
     * @return integer 
     */
    public function getNudged()
    {
        return $this->nudged;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Video
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return Video
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set user
     *
     * @param \KAPI\KooshApiBundle\Entity\User $user
     * @return Video
     */
    public function setUser(\KAPI\KooshApiBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get systemAudio
     *
     * @return \KAPI\KooshApiBundle\Entity\SystemAudio 
     */
    public function getSystemAudio()
    {
        return $this->systemAudio;
    }

    public function getSystemAudioHelperPath()
    {
        return $this->koosh->getHelpersAbsolutePath() . $this->systemAudio->getAudioFilePath();
    }

    /**
     * Set systemAudio
     *
     * @param \KAPI\KooshApiBundle\Entity\SystemAudio $systemAudio
     * @return SystemAudio
     */
    public function setSystemAudio(\KAPI\KooshApiBundle\Entity\SystemAudio $systemAudio = null)
    {
        $this->systemAudio = $systemAudio;

        return $this;
    }

    /**
     * Get user
     *
     * @return \KAPI\KooshApiBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
    
    /**
     * Set koosh
     *
     * @param \KAPI\KooshApiBundle\Entity\Koosh $koosh
     * @return Video
     */
    public function setKoosh(\KAPI\KooshApiBundle\Entity\Koosh $koosh = null)
    {
        $this->koosh = $koosh;

        return $this;
    }

    /**
     * Get koosh
     *
     * @return \KAPI\KooshApiBundle\Entity\Koosh 
     */
    public function getKoosh()
    {
        return $this->koosh;
    }

    /**
     * Add videoLikes
     *
     * @param \KAPI\KooshApiBundle\Entity\VideoLike $videoLikes
     * @return Video
     */
    public function addVideoLike(\KAPI\KooshApiBundle\Entity\VideoLike $videoLikes)
    {
        $this->videoLikes[] = $videoLikes;

        return $this;
    }

    /**
     * Remove videoLikes
     *
     * @param \KAPI\KooshApiBundle\Entity\VideoLike $videoLikes
     */
    public function removeVideoLike(\KAPI\KooshApiBundle\Entity\VideoLike $videoLikes)
    {
        $this->videoLikes->removeElement($videoLikes);
    }

    /**
     * Get videoLikes
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getVideoLikes()
    {
        return $this->videoLikes;
    }
    
    // YourNS/YourBundle/Entity/Image.php
    const SERVER_PATH_TO_IMAGE_FOLDER = 'uploads/video';


    public function getAbsolutePath()
    {
        return $this->getUploadRootDir().'/';
    }
    
    public function getAudioAbsolutePath()
    {
        return $this->getAudioUploadRootDir().'/';
    }
    
    
    public function getVideoFileAbsolutePath()
    {
        return null === $this->videoFilePath
            ? null
            : $this->getUploadRootDir().'/'.$this->videoFilePath;
    }

    public function getVideoFileHelperAbsolutePath()
    {
        return null === $this->videoFilePath
            ? null
            : $this->koosh->getHelpersAbsolutePath().$this->videoFilePath;
    }
    
    public function getAudioFileAbsolutePath()
    {
        return null === $this->audioFilePath
            ? null
            : $this->getAudioUploadRootDir().'/'.$this->audioFilePath;
    }

    public function getAudioFileHelperAbsolutePath()
    {
        return null === $this->audioFilePath
            ? null
            : $this->koosh->getHelpersAbsolutePath().$this->audioFilePath;
    }
    
    public function getImageFileAbsolutePath()
    {
        return null === $this->imageFilePath
            ? null
            : $this->getUploadRootDir().'/'.$this->imageFilePath;
    }

    public function getVideoFileWebPath()
    {
        return null === $this->videoFilePath
            ? null
            : $this->koosh->getUploadDir().'/'.$this->getUploadDir().'/'.$this->videoFilePath;
    }

    public function getImageFileWebPath()
    {
        return null === $this->imageFilePath
            ? null
            : $this->koosh->getUploadDir().'/'.$this->getUploadDir().'/'.$this->imageFilePath;
    }
    
    public function getAudioFileWebPath()
    {
        return null === $this->audioFilePath
            ? null
            : $this->koosh->getUploadDir().'/'.$this->getAudioUploadDir().'/'.$this->audioFilePath;
    }
    
    protected function getUploadRootDir()
    {
        $uploadRootDir = $this->koosh->getAbsolutePath() . $this->getUploadDir();
        
        // check existing path
        if (!file_exists($uploadRootDir)) {
            mkdir($uploadRootDir, 0755, true);
        }
        
        // the absolute directory path where uploaded
        // documents should be saved
        return $uploadRootDir;
    }
    
    protected function getAudioUploadRootDir() {
        $uploadRootDir = $this->koosh->getAbsolutePath() . $this->getAudioUploadDir();
        
        // check existing path
        if (!file_exists($uploadRootDir)) {
            mkdir($uploadRootDir, 0755, true);
        }
        
        // the absolute directory path where uploaded
        // documents should be saved
        return $uploadRootDir;
    }
    
    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'videos/'.$this->uniqueHash;
    }
    
    protected function getAudioUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'videos/'.$this->uniqueHash;
    }
    
    
   private $videoFileTemp;
   private $audioFileTemp;

    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setVideoFile(UploadedFile $file = null)
    {
        $this->videoFile = $file;
        // check if we have an old image videoFilePath
        if (isset($this->videoFilePath)) {
            // store the old name to delete after the update
            $this->videoFileTemp = $this->videoFilePath;
            $this->videoFilePath = null;
        } else {
            $this->videoFilePath = 'initial';
        }
    }

    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setAudioFile(UploadedFile $file = null)
    {
        $this->audioFile = $file;
        // check if we have an old image videoFilePath
        if (isset($this->audioFilePath)) {
            // store the old name to delete after the update
            $this->audioFileTemp = $this->audioFilePath;
            $this->audioFilePath = null;
        } else {
            $this->audioFilePath = 'initial';
        }
    }
    
    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        
        if (null !== $this->getVideoFile()) {
            // do whatever you want to generate a unique name
            $filename = sha1(uniqid(mt_rand(), true));
            $this->videoFilePath = $filename.'.'.$this->getVideoFile()->guessExtension();
        } else {
            $this->videoFilePath = $this->videoFileTemp;
        }
        
        if (null !== $this->getAudioFile() && (is_object($this->getAudioFile()))) {
            // do whatever you want to generate a unique name
            $filename = sha1(uniqid(mt_rand(), true));      
            $this->audioFilePath = $filename.'.mp3';
        } else {
            $this->audioFilePath = $this->audioFileTemp;
        }
        
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function uploadVideo()
    {
        if (null === $this->getVideoFile()) {
            return;
        }
        
        // if there is an error when moving the file, an exception will
        // be automatically thrown by move(). This will properly prevent
        // the entity from being persisted to the database on error
        $this->getVideoFile()->move($this->getUploadRootDir(), $this->videoFilePath);

        // check if we have an old image
        if (isset($this->videoFileTemp)) {
            // delete the old image
            unlink($this->getUploadRootDir().'/'.$this->videoFileTemp);
            // clear the videoFileTemp image videoFilePath
            $this->videoFileTemp = null;
        }
        //$this->videoFile = null;
        
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function uploadAudio()
    {
        if ((null === $this->getAudioFile()) || !is_object($this->getAudioFile())) {
            return;
        }
        
        
        // if there is an error when moving the file, an exception will
        // be automatically thrown by move(). This will properly prevent
        // the entity from being persisted to the database on error
        $this->getAudioFile()->move($this->getAudioUploadRootDir(), $this->audioFilePath);

        // check if we have an old image
        if (isset($this->audioFileTemp)) {
            // delete the old image
            unlink($this->getAudioUploadRootDir().'/'.$this->audioFileTemp);
            // clear the videoFileTemp image videoFilePath
            $this->audioFileTemp = null;
        }
        //$this->audioFile = null;
    }
    
    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        if(($imageFile = $this->getImageFileAbsolutePath()) && is_file($imageFile)) {
            unlink($imageFile);
        }
        
        if(($videoFile = $this->getVideoFileAbsolutePath()) && is_file($videoFile)) {
            unlink($videoFile);
        }
        
        if(($videoFile1 = $this->getVideoFileAbsolutePath().'original.mp4') && is_file($videoFile1)) {
            unlink($videoFile1);
        }
        
        if(($audioFile = $this->getAudioFileAbsolutePath()) && is_file($audioFile)) {
            unlink($audioFile);
        }
        
    }


    /**
     * Set path
     *
     * @param string $path
     * @return User
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string 
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set videoFilePath
     *
     * @param string $videoFilePath
     * @return Video
     */
    public function setVideoFilePath($videoFilePath)
    {
        $this->videoFilePath = $videoFilePath;

        return $this;
    }

    /**
     * Get videoFilePath
     *
     * @return string 
     */
    public function getVideoFilePath()
    {
        return $this->videoFilePath;
    }
    
    public function getActionField() {
        return '<a href="">EDIT</a>';
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Video
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set caption
     *
     * @param string $caption
     * @return Video
     */
    public function setCaption($caption)
    {
        $this->caption = $caption;

        return $this;
    }

    /**
     * Get caption
     *
     * @return string 
     */
    public function getCaption()
    {
        return $this->caption;
    }
    
    /**
     * Set uniqueHash
     *
     * @param string $uniqueHash
     * @return Video
     */
    public function setUniqueHash($uniqueHash)
    {
        $this->uniqueHash = $uniqueHash;

        return $this;
    }

    /**
     * Get uniqueHash
     *
     * @return string 
     */
    public function getUniqueHash()
    {
        return $this->uniqueHash;
    }

    /**
     * Set muted
     *
     * @param integer $muted
     * @return Video
     */
    public function setMuted($muted)
    {
        $this->muted = $muted;

        return $this;
    }

    /**
     * Get muted
     *
     * @return integer 
     */
    public function getMuted()
    {
        return $this->muted;
    }

    /**
     * Set imageFile
     *
     * @param string $imageFile
     * @return Video
     */
    public function setImageFile($imageFile)
    {
        $this->imageFile = $imageFile;

        return $this;
    }

    /**
     * Get imageFile
     *
     * @return string 
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * Set imageFilePath
     *
     * @param string $imageFilePath
     * @return Video
     */
    public function setImageFilePath($imageFilePath)
    {
        $this->imageFilePath = $imageFilePath;

        return $this;
    }

    /**
     * Get imageFilePath
     *
     * @return string 
     */
    public function getImageFilePath()
    {
        return $this->imageFilePath;
    }
    
    /**
     * Set addAudio
     *
     * @param integer $addAudio
     * @return Video
     */
    public function setAddAudio($addAudio)
    {
        $this->addAudio = $addAudio;

        return $this;
    }

    /**
     * Get addAudio
     *
     * @return integer 
     */
    public function getAddAudio()
    {
        return $this->addAudio;
    }

  

    /**
     * Get audioFile
     *
     * @return string 
     */
    public function getAudioFile()
    {
        return $this->audioFile;
    }

    /**
     * Set audioFilePath
     *
     * @param string $audioFilePath
     * @return Video
     */
    public function setAudioFilePath($audioFilePath)
    {
        $this->audioFilePath = $audioFilePath;

        return $this;
    }

    /**
     * Get audioFilePath
     *
     * @return string 
     */
    public function getAudioFilePath()
    {
        return $this->audioFilePath;
    }
}
