<?php

namespace KAPI\KooshApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity
 * @ORM\Table(name="videoLike")
 * @Serializer\ExclusionPolicy("all")
 */
class VideoLike
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\Column(type="integer")
     * 
     * @Assert\NotBlank()
     */
    protected $videoId;

    /**
     * @ORM\Column(type="integer")
     * 
     * @Assert\NotBlank()
     */
    protected $userId;
    

    /**
     * @ORM\Column(type="datetime")
     */
    protected $created;
    
    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="kooshLikes")
     * @ORM\JoinColumn(name="userId", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $user;
    
    /**
     * @ORM\ManyToOne(targetEntity="Video", inversedBy="videoLikes")
     * @ORM\JoinColumn(name="videoId", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $video;
    
    public function toString()
    {
        return $this->id;
    }

    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set videoId
     *
     * @param integer $videoId
     * @return VideoLike
     */
    public function setVideoId($videoId)
    {
        $this->videoId = $videoId;

        return $this;
    }

    /**
     * Get videoId
     *
     * @return integer 
     */
    public function getVideoId()
    {
        return $this->videoId;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     * @return VideoLike
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer 
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return VideoLike
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set user
     *
     * @param \KAPI\KooshApiBundle\Entity\User $user
     * @return VideoLike
     */
    public function setUser(\KAPI\KooshApiBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \KAPI\KooshApiBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set video
     *
     * @param \KAPI\KooshApiBundle\Entity\Video $video
     * @return VideoLike
     */
    public function setVideo(\KAPI\KooshApiBundle\Entity\Video $video = null)
    {
        $this->video = $video;

        return $this;
    }

    /**
     * Get video
     *
     * @return \KAPI\KooshApiBundle\Entity\Video 
     */
    public function getVideo()
    {
        return $this->video;
    }
}
