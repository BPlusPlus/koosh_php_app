<?php 
// http://symfony.com/doc/current/cookbook/form/create_custom_field_type.html

namespace KAPI\KooshApiBundle\Form\Type;

use Symfony\Component\Form\ButtonTypeInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\Extension\Core\Type\BaseType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\Options;

use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface; 

use Sonata\AdminBundle\Admin\FieldDescriptionInterface;

class ImagePreviewType extends AbstractType implements ButtonTypeInterface 
{
    
    protected $router;
    
    public function __construct()
    {
    }
    
    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return 'button';
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $attr = function(Options $options, $value){ 
            return array(                
                'class'=>'btn btn-warning', 
                'button_name' => $options['button_name'], 
                'image_path' => $options['image_path'] 
            ); 
        };
        
        
        $resolver->setDefaults(array(
            'attr' => $attr,
            'mapped' => false, 
            'params' => array(),
            'label_render' => '', 
            'sonata_field_description' => '', 
            'button_name' => '', 
            'read_only' => false, 
            'required' => false, 
            'image_path' => ''
        ))->setRequired(array(
            'image_path',
        ))
        ->setNormalizers(array(
            'attr' => $attr, 
        ));
    }

    public function getName()
    {
        return 'imagePreview';
    }
    
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $sonataAdmin = array(
            'name'              => null,
            'admin'             => null,
            'value'             => null,
            'edit'              => 'standard',
            'inline'            => 'natural',
            'field_description' => null,
            'block_name'        => false
        );

        $builder->setAttribute('sonata_admin_enabled', false);

        if ($options['sonata_field_description'] instanceof FieldDescriptionInterface) {
            $fieldDescription = $options['sonata_field_description'];

            $sonataAdmin['admin']             = $fieldDescription->getAdmin();
            $sonataAdmin['field_description'] = $fieldDescription;
            $sonataAdmin['name']              = $fieldDescription->getName();
            $sonataAdmin['edit']              = $fieldDescription->getOption('edit', 'standard');
            $sonataAdmin['inline']            = $fieldDescription->getOption('inline', 'natural');
            $sonataAdmin['block_name']        = $fieldDescription->getOption('block_name', false);
            //$sonataAdmin['class']             = $this->getClass($builder);

            $builder->setAttribute('sonata_admin_enabled', true);
        }

        $builder->setAttribute('sonata_admin', $sonataAdmin);
    }
    
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        parent::buildView($view, $form, $options);

        $view->vars['required'] = false;
        $view->vars['sonata_admin'] = $form->getConfig()->getAttribute('sonata_admin');
        $view->vars['errors'] = false;
    }
    
}