<?php  
  
namespace KAPI\KooshApiBundle\Listener;  
  
use KAPI\KooshApiBundle\Entity\Invite;  
  
class PostPersistInvite  
{  
    private $container;  
      
    public function __construct($container)  
    {  
        $this->container = $container;  
    }  
  
    public function postPersist(LifecycleEventArgs $args){  
        $entity = $args->getEntity();  
        if($entity instanceof Invite) {  
            $message = serialize(array('invite_id' => $entity->getId()));  
            $this->container->get('old_sound_rabbit_mq.add_video_task_producer')  
                            ->publish($message);  
        }  
    }  
}  