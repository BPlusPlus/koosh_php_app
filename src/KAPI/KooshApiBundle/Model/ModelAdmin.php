<?php 

class ModelAdmin extends Admin
{
    // ...
 
    public function validate(ErrorElement $errorElement, $object)
    {
        // find object with the same uniqueField-value
        $other = $this->modelManager->findOneBy($this->getClass(), array('uniqueField' => $object->getUniqueSlug()));
 
        if (null !== $other && !$other->equals($object)) {
            $errorElement
                ->with('uniqueField')
                ->addViolation('The unique field must be unique!')
                ->end();
        }
    }
}