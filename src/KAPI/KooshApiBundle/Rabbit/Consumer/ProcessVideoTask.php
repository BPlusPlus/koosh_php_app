<?php

namespace KAPI\KooshApiBundle\Rabbit\Consumer;

use Doctrine\ORM\EntityManager;
use KAPI\KooshApiBundle\Entity\Koosh;
use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

use FFMpeg\Coordinate\Dimension;
use FFMpeg\Filters\Video\ResizeFilter;
use FFMpeg\Format\Video\X264;
use FFMpeg\Coordinate\FrameRate;
use FFMpeg\Coordinate\TimeCode;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Process\Exception\ProcessFailedException;
use KAPI\KooshApiBundle\CustomProcess as Process;

use RMS\PushNotificationsBundle\Message\iOSMessage;
use RMS\PushNotificationsBundle\Message\AndroidMessage;

class ProcessVideoTask implements ConsumerInterface
{
    private $container;
    private $logger;
    private $em;
    private $videoCodecSetting;
    private $threadsSetting;
    private $meltThreadsSetting;

    public function __construct(ContainerInterface $container, EntityManager $em)
    {
        $this->em = $em;
        $this->container = $container;
        $this->logger = $container->get('rabbitmq.logger');
        Process::setLogger($this->logger);
        if (extension_loaded('newrelic')) {
            newrelic_disable_autorum();
            newrelic_background_job();
            newrelic_end_transaction(false);
        }

        $this->videoCodecSetting = $this->getVideoCodecSetting();
        $this->threadsSetting = 3;
        $this->meltThreadsSetting = 4;
    }

    public function execute(AMQPMessage $msg)
    {
        try {
            if (extension_loaded('newrelic')) {
                newrelic_start_transaction(ini_get('newrelic.appname')?ini_get('newrelic.appname'):'Koosh');
                newrelic_background_job();
                newrelic_name_transaction("process-koosh");
            }
            $this->logger->addInfo('Start executing video task.');
            $message = unserialize($msg->body);
            if (extension_loaded('newrelic')) {
                if(isset($message['kooshId'])) {
                    newrelic_add_custom_parameter('kooshId', $message['kooshId']);
                }
                newrelic_add_custom_parameter('kooshMessage', print_r($message, true));
            }
            $this->logger->addInfo('Decoded message', ['message' => $message]);

            $koosh = $this->em->getRepository('KAPIKooshApiBundle:Koosh')->findOneBy(array('id' => $message['kooshId']));
            // kill unsuccessfull koosh (in process)
            if(is_object($koosh) && ($koosh->getStatus() == '2')) {
                $this->markAsUnfinished($koosh->getId(), 'Unfinished koosh found by RabbitMQ consumer.');
                if (extension_loaded('newrelic')) {
                    newrelic_end_transaction(false);
                }
                return true;
            }

            $this->logger->addInfo('Check koosh id: ' . $message['kooshId']);

            // check if actual droplet is free
            $dropletServerWebPath = ($this->container->getParameter('droplet_server_web_path') != '') ? $this->container->getParameter('droplet_server_web_path') : false;
            $kooshInProcessOnDroplet = $this->em->getRepository('KAPIKooshApiBundle:Koosh')->findOneBy(
                array(
                    'dropletUrl' => $dropletServerWebPath,
                    'status' => '2'
                )
            );
            if(is_object($kooshInProcessOnDroplet)){
                $this->logger->addInfo('Found existing koosh in process on droplet: ' . $dropletServerWebPath);

                // return koosh to the queue
                $this->logger->addInfo('Returning koosh id: ' . $message['kooshId'] . ' back to que.');
                $this->container->get('add_video_task')->process(array('kooshId' => $message['kooshId'], 'items' => $message['items'], 'fadeType' => $message['fadeType']));

                if (extension_loaded('newrelic')) {
                    newrelic_end_transaction(false);
                }
            } else {
                switch($message['type']) {
                    CASE 'process-koosh':
                        // Process Koosh
                        $koosh = $this->em->getRepository('KAPIKooshApiBundle:Koosh')->findOneBy(array('id' => $message['kooshId']));
                        if(!is_object($koosh)) {
                            $this->logger->addInfo('Nonexisting item in proccess -  koosh id: ' . $message['kooshId'] . '.');
                        } else {
                            $this->logger->addInfo('Processing koosh id: ' . $message['kooshId'] . ' videos/images.');
                            $this->processKooshItems($message['kooshId'], $message['items'], $message['fadeType']);
                        }
                        break;
                    DEFAULT:
                        break;
                }


                $this->logger->addInfo('End executing video task.');
                if (extension_loaded('newrelic')) {
                    newrelic_end_transaction(false);
                }
            }

        }
        catch(\Exception $e){
            $this->logger->addError($e->getMessage());
            if (extension_loaded('newrelic')) {
                newrelic_notice_error($e->getMessage(), $e);
                newrelic_end_transaction(false);
            }
        }
    }

    protected function copyDirectory($source, $dest) {
        mkdir($dest, 0755);
        foreach (
            $iterator = new \RecursiveIteratorIterator(
                new \RecursiveDirectoryIterator($source, \RecursiveDirectoryIterator::SKIP_DOTS),
                \RecursiveIteratorIterator::SELF_FIRST) as $item
        ) {
            if ($item->isDir()) {
                mkdir($dest . DIRECTORY_SEPARATOR . $iterator->getSubPathName());
            } else {
                copy($item, $dest . DIRECTORY_SEPARATOR . $iterator->getSubPathName());
            }
        }
    }

    protected function copyKooshFilesToRamFolder($koosh, $ramProcessFolderPath) {
        $source = $koosh->getUploadDir();
        $dest = $ramProcessFolderPath . '/' . $koosh->getUniqueHash();
        $this->copyDirectory($source, $dest);
    }

    protected function getVideoCodecSetting() {
        return "-vcodec mpeg2video -qscale:v 2 ";
    }

    protected function createFinalQualityVersions($ffmpegServerPath, $source_path, $hd_output_path, $sd_output_path, $koosh) {

        // get logo watermark path
        $logo_path = $koosh->getWatermarkAbsolutePath();

        // get final video duration
        $duration = $this->getVideoDuration($ffmpegServerPath, $source_path);

        // check original audio
        $found_audio = $this->checkVideoAudio($ffmpegServerPath, $source_path);

        // check audio setting
        $add_audio = false;
        if(($koosh->getAddAudio() == 'koosh') && is_file($koosh->getAudioFileAbsolutePath())) {
            $add_audio = true;
            $audio_path = $koosh->getAudioFileAbsolutePath();
        } elseif(($koosh->getAddAudio() == 'system_audio_koosh') && is_object($koosh->getSystemAudio()) && is_file($koosh->getSystemAudio()->getAudioFileAbsolutePath())) {
            $add_audio = true;
            $audio_path = $koosh->getSystemAudio()->getAudioFileAbsolutePath();
        }

        // generate ffmpeg command
        $cmd =
            "${ffmpegServerPath}ffmpeg -y"
            ." -i $source_path"
            ." -framerate ntsc"
            ." -i $logo_path"
            .(($add_audio) ? " -i $audio_path" : "")
            ." -filter_complex"
            ." \""
            ."[0:v][1:v]overlay=(main_w-overlay_w-60):10,format=yuv420p[v]"
//            ." [0:v]scale=640:-2[sdbg];"
//            ." [sdbg][1:v]overlay=(main_w-overlay_w-40):10,format=yuv420p[sd]"

            .(($add_audio) ? "; [2:a]aformat=sample_rates=44100:channel_layouts=mono,aloop=-1:size=2147480000,atrim=duration=".($duration).",afade=t=in:ss=0:d=0.5,afade=t=out:st=".($duration - 0.5).":d=0.5[a]" : "")
            ."\""

            .((!$add_audio) ? " -map \"[v]\" -map 0:a -c:v libx264 -preset medium -crf 23 -c:a aac -strict -2 -shortest -movflags +faststart $hd_output_path" : "")
            .(($add_audio) ? " -map \"[v]\" -map \"[a]\" -c:v libx264 -preset medium -crf 23 -c:a aac -strict -2 -ac 1 -shortest -movflags +faststart $hd_output_path" : "");

        $process = new Process($cmd, null, null, null, null);
        $process->setTimeout(3600);
        $process->mustRun();

    }

    protected function createRoundCorneredProfileImage($source_path, $dest_path, $width, $height)
    {
        copy($source_path, $dest_path);

        $image = new \Imagick($source_path);
        $image->cropThumbnailImage($width, $height);
        $image->setImageFormat("png");

        $image->roundCorners($width, $height);
        $image->writeImage($dest_path);
    }

    protected function createUserNameImage($dest_path, $text, $koosh)
    {
        //$fontPath = str_replace('C:', '', str_replace('\\', '/', __DIR__)).'/../../../../../web/fonts/Helvetica_Reg.ttf';
        $fontPath = $koosh->getFontHelperAbsolutePath();

        /* Create Imagick objects */
        $image = new \Imagick();
        $draw = new \ImagickDraw();
        $background = new \ImagickPixel('#ffffff');// Transparent

        /* Font properties */
        $draw->setFont($fontPath);
        $draw->setFontSize(18);
        $draw->setFillColor('#4ea6f2');
        $draw->setStrokeAntialias(true);
        $draw->setTextAntialias(true);

        /* Get font metrics */
        $metrics = $image->queryFontMetrics($draw, $text);

        /* Create text */
        $draw->annotation(15, $metrics['ascender']+10, $text);

        /* Create image */
        $image->newImage($metrics['textWidth']+30, $metrics['textHeight']+20, $background);
        //$image->setImageOpacity(0.6);
        $image->setImageFormat('png');
        $image->drawImage($draw);
        $image->roundCorners(4,4);
        $image->writeImage($dest_path);

        return array('width' => $metrics['textWidth']+10, 'height' => $metrics['textHeight']+10);

    }

    protected function generateProfilePictureWatermark($user)
    {
        $watermarkAbsolutePath = $user->getRoundCorneredImageAbsolutePath();

        if(!is_file($watermarkAbsolutePath) && class_exists("\Imagick")) {
            $userImageAbsPath = $user->getImageAbsolutePath();
            if(!empty($userImageAbsPath) && is_file($userImageAbsPath) && filesize($userImageAbsPath)) {
                $this->createRoundCorneredProfileImage($user->getImageAbsolutePath(), $user->getRoundCorneredImageAbsolutePath(), 100, 100);
            }
        }

        return $watermarkAbsolutePath;
    }

    protected function generateProfileNameWatermark($user, $koosh)
    {
        $watermarkAbsolutePath = $user->getNameImageAbsolutePath();

        if(!is_file($watermarkAbsolutePath) && class_exists("\Imagick")) {
            $this->createUserNameImage($user->getNameImageAbsolutePath(), $user->getFirstName().' '.$user->getLastName(), $koosh);
        }

        return $watermarkAbsolutePath;
    }


    protected function prepareCaptionCmd($sourceObj, $width)
    {
        $captionCmd = '';
        $fontPath = $sourceObj->getKoosh()->getFontHelperAbsolutePath();
        $caption = $sourceObj->getCaption();
        if(!empty($caption)) {
            $text = !empty($caption) ? str_replace("'", "", $caption) : "";
            $allowedStringSize = floor($width / 16);
            $text = !empty($text) ? wordwrap($text, $allowedStringSize, "\f") : '';
            $lines = substr_count($text, "\f");
            $text = escapeshellarg($text);
            $search = array(
                ':',
                '\\'
            );
            $replace = array(
                '\\:',
                '\\\\'
            );
            $text = str_replace($search, $replace, $text);
            $text = str_replace(',', '\,', $text);
            $this->logger->addInfo('DRAWTEXT video file width:'.$width);

            $boxHeight = ($lines > 0) ? (($lines * 28) + 85) : 90;

            $captionCmd = ",drawbox=y=ih/PHI:color=black@0.4:width=iw:height=" . $boxHeight . ":t=ih, drawtext=fontfile='" . $fontPath . "': text='" . $text . "':fontcolor=white:fontsize=28:x=(w-tw)/2:y=(h/PHI)+35";
        }

        return $captionCmd;
    }


    /**
     * Merge videos and images togeather
     *
     * @param type $kooshId
     * @param type $items
     * @param type $fadeType
     * @return boolean
     */
    public function processKooshItems($kooshId, $items, $fadeType) {
        // mark koosh as in processing
        $koosh = $this->markKooshProcessing($kooshId, $items, $fadeType);

        // ffmpeg bundle
        $ffmpeg = $this->container->get('dubture_ffmpeg.ffmpeg');
        $ffprobe = $this->container->get('dubture_ffmpeg.ffprobe');

        $finalVideoUUID = sha1(uniqid(mt_rand(), true));
        $finalVideoName = $finalVideoUUID . '.mp4';
        $finalVideoNameQ2 = $finalVideoUUID . '.lq.mp4';
        $finalVideoFileOriginPath = $koosh->getAbsolutePath() . $finalVideoName;
        $finalVideoFileOriginPathQ2 = $koosh->getAbsolutePath() . $finalVideoNameQ2;
        $finalVideoFilePath = $koosh->getHelpersAbsolutePath() . $finalVideoName;
        $finalVideoFilePathQ2 = $koosh->getHelpersAbsolutePath() . $finalVideoNameQ2;
        $finalVideoMeltFilePath = $koosh->getHelpersAbsolutePath() . $finalVideoName . "_melt.mkv";
        $imageHelpName = sha1(uniqid(mt_rand(), true));
        $imageHelpFilePath = $koosh->getHelpersAbsolutePath() . $imageHelpName;
        $freezeImageName = '';


        // Get all koosh items
        $kooshItems = $this->storeKooshItemsSettings($items);

        // Copy all files to heler folder
        $this->copySourceFilesToHelpersDir($kooshItems, $koosh, $fadeType, true);

        // Check the highest framerate
        list($allItemsInPortrait, $highestFrameRate, $convertFrameRate, $highestSampleRate, $convertSampleRate, $videoInfo) = $this->findHighestFramerateAndSamplerate($kooshItems, $ffprobe);

        // Convert all videos to highest framerate if necessary(different frame rates)
        //$this->convertItemsToHighestFrameRate($kooshId, $convertFrameRate, $highestFrameRate, $kooshItems, $ffmpeg);


        $ffmpegServerPath = ($this->container->getParameter('ffmpeg_server_path') != '') ? $this->container->getParameter('ffmpeg_server_path') . '/' : '';
        $meltServerPath = ($this->container->getParameter('melt_server_path') != '') ? $this->container->getParameter('melt_server_path') : 'mlt-melt';
        $isDropletServer = ($this->container->getParameter('is_droplet_server') != '') ? $this->container->getParameter('is_droplet_server') : false;

        $fadeInTime = 1;
        $fadeOutTime = 1;
        $totalVideoDuration = 0;
        $firstVideoDuration = 0;
        $singleVideoDuration = 5;
        $finalMergeVideoList = array();
        $deleteFilesList = array();

        if($allItemsInPortrait) {
            $resizeWidth = 960;
            $resizeHeight = 1280;
        } else {
            $resizeWidth = 1280;
            $resizeHeight = 960;
        }


        // Apply all filters, audio, ...
        foreach($kooshItems AS $key => $item) {
            if($item['source_type'] == 'video') {
                list($totalVideoDuration, $freezeImageName, $highestSampleRate, $finalMergeVideoList) = $this->compileVideoItem($kooshId, $fadeType, $item, $videoInfo, $key, $totalVideoDuration, $freezeImageName, $koosh, $ffmpeg, $ffmpegServerPath, $ffprobe, $highestSampleRate, $fadeInTime, $fadeOutTime, $resizeWidth, $resizeHeight, $highestFrameRate, $finalMergeVideoList);
            } else {
                $image = $item['object'];
                list($highestSampleRate, $highestFrameRate, $freezeImageName, $finalMergeVideoList, $totalVideoDuration, $deleteFilesList) = $this->compileImageItem($kooshId, $fadeType, $highestSampleRate, $highestFrameRate, $image, $key, $freezeImageName, $koosh, $imageHelpFilePath, $resizeWidth, $resizeHeight, $ffmpegServerPath, $singleVideoDuration, $fadeInTime, $fadeOutTime, $finalMergeVideoList, $totalVideoDuration, $deleteFilesList);

            }
        }


        // Concat (merge) videos from list
        $this->concatItemsWithMelt($koosh, $fadeType, $finalMergeVideoList, $finalVideoMeltFilePath, $resizeWidth, $resizeHeight, $highestFrameRate, $meltServerPath);

        $fadeInTime = 3;
        $fadeOutTime = 3;


        // Generate Final Quality Versions - SD and HD
        try {
            $this->createFinalQualityVersions($ffmpegServerPath, $finalVideoMeltFilePath, $finalVideoFilePath, $finalVideoFilePathQ2, $koosh);

        } catch (ProcessFailedException $e) {
            $this->logger->addError('FFMPEG generate SD and HD versions error: '.$e->getMessage());
            $this->markAsUnfinished($kooshId, $e->getMessage());
            return false;
        }


        // copy freeze image from helpers folder
        if(is_file($koosh->getHelpersAbsolutePath() . $freezeImageName)) {
            copy($koosh->getHelpersAbsolutePath() . $freezeImageName, $koosh->getAbsolutePath() . $freezeImageName);
        }


        // copy main video files from helpers folder
        if(is_file($finalVideoFilePath)) {
            copy($finalVideoFilePath, $finalVideoFileOriginPath);
        }
        if(is_file($finalVideoFilePathQ2)) {
            copy($finalVideoFilePathQ2, $finalVideoFileOriginPathQ2);
        }


        // delete heleprs folder
        $process = new Process("rm -R " . $koosh->getHelpersAbsolutePath());
        $process->mustRun();

        // Delete old file
        if(is_file($koosh->getVideoFileAbsolutePath())) {
            unlink($koosh->getVideoFileAbsolutePath());
        }


        // Persist final video to db
        $koosh->setVideoFile(null);
        $koosh->setImage(null);
        $koosh->setImageTemp($freezeImageName);
        $koosh->setVideoFileTemp($finalVideoName);
        $koosh->setVideoFileDirectly($finalVideoNameQ2, Koosh::QUALITY_2);
        $koosh->setStatus(3); // set CONVERTED state
        $koosh->setProcessEnded(new \DateTime());
        $koosh->setAudioFile(null);

        $this->em->persist($koosh);
        $this->em->flush();

        // call remote server download script
        $mainServerWebPath = ($this->container->getParameter('main_server_web_path') != '') ? $this->container->getParameter('main_server_web_path') : '';
        file_get_contents($mainServerWebPath . 'droplet/finished/'.$koosh->getId().'/');

        // Send notification to all involved users
        $this->sendPushNotificationsToInvolvedUsers($koosh);

        // Delete koosh files
        if($isDropletServer) {
            if(is_file($koosh->getImageAbsolutePath())) {
                unlink($koosh->getImageAbsolutePath());
            }

            if(is_file($koosh->getVideoFileAbsolutePath())) {
                unlink($koosh->getVideoFileAbsolutePath());
            }
        }
    }


    public function markAsUnfinished($kooshId, $errorMsg) {
        $koosh = $this->em->getRepository('KAPIKooshApiBundle:Koosh')->findOneBy(array('id' => $kooshId));
        // Change status
        $koosh->setVideoFile(null);
        $koosh->setImage(null);
        $koosh->setErrorMessage($errorMsg);
        $koosh->setStatus(4); // set UNFINISHED ERROR state
        $koosh->setAudioFile(null);

        $this->em->persist($koosh);
        $this->em->flush();
    }

    protected function markKooshProcessing($kooshId, $items, $fadeType)
    {
        $koosh = $this->em->getRepository('KAPIKooshApiBundle:Koosh')->findOneBy(array('id' => $kooshId));
        $koosh->setStatus(2); // set PROCESS state
        $koosh->setProcessStarted(new \DateTime());
        $koosh->setRabbitmqCounter(0);
        $koosh->setErrorMessage('');
        $koosh->setProcessingInstructions(serialize($items));
        $koosh->setFadeType($fadeType);
        $koosh->setAudioFile(null);

        $dropletServerWebPath = ($this->container->getParameter('droplet_server_web_path') != '') ? $this->container->getParameter('droplet_server_web_path') : '';
        $koosh->setDropletUrl($dropletServerWebPath);

        //$this->em->persist($koosh);
        $this->container->get('kapi_kooshService')->update($koosh);
        return $koosh;
    }

    protected function storeKooshItemsSettings($items)
    {
        $kooshItems = array();
        foreach ($items AS $key => $itemData) {
            $item = $this->em->getRepository('KAPIKooshApiBundle:KooshItem')->findOneBy(array('id' => $itemData['unique_id']));
            if ($item->getSourceType() == 'video') {
                $video = $item->getVideo();
                if (is_object($video)) {

                    $systemAudio = isset($itemData['system_audio_id']) ? $this->em->getRepository('KAPIKooshApiBundle:SystemAudio')->findOneBy(array('id' => $itemData['system_audio_id'])) : null;
                    if (is_object($systemAudio)) {
                        $video->setSystemAudio($systemAudio);
                    }

                    $video->setMuted(isset($itemData['muted_bool']) ? $itemData['muted_bool'] : false);
                    $video->setCaption(isset($itemData['caption']) ? $itemData['caption'] : '');
                    $video->setAddAudio(isset($itemData['add_audio']) ? $itemData['add_audio'] : '');
                    $video->setVideoFile(null);
                    $video->setAudioFile(null);
                    $this->em->persist($video);
                    $this->em->flush();

                    $kooshItems[$key] = array(
                        'source_type' => 'video',
                        'object' => $video
                    );
                }
            } else {
                /*
                 * Images
                 */
                $image = $item->getImage();
                if (is_object($image)) {

                    $image->setCaption(isset($itemData['caption']) ? $itemData['caption'] : '');
                    $image->setImageFile(null);
                    $this->em->persist($image);
                    $this->em->flush();

                    $kooshItems[$key] = array(
                        'source_type' => 'image',
                        'object' => $image
                    );
                }
            }
        }
        return $kooshItems;
    }

    protected function findHighestFramerateAndSamplerate($kooshItems, $ffprobe)
    {
        $allItemsInPortrait = true;
        $allVideosHave1AudioChannel = true;
        $highestFrameRate = 0;
        $convertFrameRate = false;
        $highestSampleRate = 0;
        $convertSampleRate = false;

        $videoInfo = array();
        foreach ($kooshItems AS $key => $item) {
            if ($item['source_type'] == 'video') {
                $info = $ffprobe->format($item['object']->getVideoFileHelperAbsolutePath());
                $streams = $ffprobe->streams($item['object']->getVideoFileHelperAbsolutePath());

                foreach ($streams AS $stream) {
                    if ($stream->get('codec_type') == 'video') {

                        $this->logger->addInfo('VIDEO frame RATE:' . $stream->get('r_frame_rate'));

                        if ($stream->get('r_frame_rate') > $highestFrameRate) {
                            $highestFrameRate = $stream->get('r_frame_rate');
                        }
                        if ($stream->get('r_frame_rate') != $highestFrameRate) {
                            $convertFrameRate = true;
                        }
                        $tags = $stream->get('tags');

                        $videoInfo[$key]['nb_frames'] = $stream->get('nb_frames');
                        $videoInfo[$key]['duration'] = $stream->get('duration');
                        $videoInfo[$key]['width'] = $stream->get('width');
                        $videoInfo[$key]['height'] = $stream->get('height');
                        $videoInfo[$key]['rotate'] = isset($tags['rotate']) ? $tags['rotate'] : '';

                        $this->logger->addError('ROTATE:' . $videoInfo[$key]['rotate']);
                    } elseif ($stream->get('codec_type') == 'audio') {
                        $this->logger->addInfo('VIDEO sample RATE:' . $stream->get('sample_rate'));

                        if ($stream->get('sample_rate') > $highestSampleRate) {
                            $highestSampleRate = $stream->get('sample_rate');
                        }
                        if ($stream->get('sample_rate') != $highestSampleRate) {
                            $convertSampleRate = true;
                        }
                        $videoInfo[$key]['channels'] = $stream->get('channels');
                        $videoInfo[$key]['channel_layout'] = $stream->get('channel_layout');
                        $videoInfo[$key]['sample_rate'] = $stream->get('sample_rate');
                    }
                }

                if ($allVideosHave1AudioChannel && isset($videoInfo[$key]['channels']) && ($videoInfo[$key]['channels'] != '1')) {
                    $allVideosHave1AudioChannel = false;
                }

                if ($allItemsInPortrait && ((($videoInfo[$key]['width'] > $videoInfo[$key]['height']) && empty($videoInfo[$key]['rotate'])))) {
                    $allItemsInPortrait = false;
                }
            } else {
                // check image resolution
                list($imgWidth, $imgHeight, $imgType, $imgAttr) = getimagesize($item['object']->getImageFileHelperAbsolutePath());
                if ($allItemsInPortrait && (empty($imgWidth) || empty($imgHeight) || ($imgWidth > $imgHeight))) {
                    $allItemsInPortrait = false;
                }
            }
        }
        return array($allItemsInPortrait, $highestFrameRate, $convertFrameRate, $highestSampleRate, $convertSampleRate, $videoInfo);
    }

    protected function convertItemsToHighestFrameRate($kooshId, $convertFrameRate, $highestFrameRate, $kooshItems, $ffmpeg)
        {
        if ($convertFrameRate) {
            $frameRate = new FrameRate($highestFrameRate);
            $gop = 250;
            $format = new X264('libvo_aacenc');
            $format->setKiloBitrate(1000);

            foreach ($kooshItems AS $item) {
                if ($item['source_type'] == 'video') {
                    $video = $item['object'];
                    $videoFilePath = $video->getVideoFileHelperAbsolutePath();

                    try {
                        $video = $ffmpeg->open($videoFilePath);
                        $video->filters()->framerate($frameRate, $gop);
                        $video->save($format, $videoFilePath . 'converted.mp4');
                    } catch (Exception $e) {
                        $this->logger->addError('FFMPEG highest framerate converting: ' . $e->getMessage());
                        $this->markAsUnfinished($kooshId, $e->getMessage());
                    }

                    // delete old file
                    if (is_file($videoFilePath)) {
                        copy($videoFilePath, $videoFilePath . 'original.mp4');
                        unlink($videoFilePath);
                    }
                    // rename new file
                    rename($videoFilePath . 'converted.mp4', $videoFilePath);
                    // allow access
                    if (is_file($videoFilePath)) {
                        chmod($videoFilePath, 0777);
                    }

                    $this->logger->addInfo('Converting video:' . $videoFilePath . ' to highest frameRate:' . $highestFrameRate);
                }
            }
        }
    }

    protected function copyFileFromRemoteServer($sourcePath, $targetPath) {
        try {
            $mainServerWebPath = ($this->container->getParameter('main_server_web_path') != '') ? $this->container->getParameter('main_server_web_path') : '';
            $this->logger->addInfo('COPY file from REMOTE server:' . $mainServerWebPath . $sourcePath);

            // Create a curl handle to a non-existing location
            $ch = curl_init($mainServerWebPath . $sourcePath);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $remoteFile = '';
            if( ($remoteFile = curl_exec($ch) ) === false)
            {
                $this->logger->addError('COPY file from REMOTE: ' . curl_error($ch));
            }
            else
            {
                file_put_contents($targetPath, $remoteFile);
            }

            // Close handle
            curl_close($ch);

            //$remoteFile = file_get_contents($mainServerWebPath . $sourcePath);

        } catch (Exception $e) {
            $this->logger->addError('COPY file from REMOTE: ' . $e->getMessage());
            //$this->markAsUnfinished($kooshId, $e->getMessage());
        }
    }

    protected function copySourceFilesToHelpersDir($kooshItems, $koosh, $fadeType, $copyFromRemote = true)
    {
        // check transition
        $transition = $this->em->getRepository('KAPIKooshApiBundle:Transition')->findOneBy(array('code' => $fadeType));
        if(is_object($transition)) {
            $maskPath = $transition->getMaskImageFileWebPath();
            if(!empty($maskPath)) {
                if($copyFromRemote) {
                    $this->copyFileFromRemoteServer($transition->getMaskImageFileWebPath(), $transition->getTransitionMaskFileHelperAbsolutePath($koosh));
                } elseif(is_file($transition->getTransitionMaskFileAbsolutePath())) {
                    copy($transition->getTransitionMaskFileAbsolutePath(), $transition->getTransitionMaskFileHelperAbsolutePath($koosh));
                }

            }
        }

        // copy font
        $fontPath = str_replace('C:', '', str_replace('\\', '/', __DIR__)).'/../../../../../web/fonts/Helvetica_Reg.ttf';
        if($copyFromRemote) {
            $this->copyFileFromRemoteServer('fonts/Helvetica_Reg.ttf', $koosh->getFontHelperAbsolutePath());
        } elseif(is_file($fontPath)) {
            copy($fontPath, $koosh->getFontHelperAbsolutePath());
        }

        foreach ($kooshItems AS $item) {
            if ($item['source_type'] == 'video') {
                // copy video object
                $video = $item['object'];
                if($copyFromRemote) {
                    $this->copyFileFromRemoteServer($video->getVideoFileWebPath(), $video->getVideoFileHelperAbsolutePath());
                } else {
                    copy($video->getVideoFileAbsolutePath(), $video->getVideoFileHelperAbsolutePath());
                }


                // copy video audio
                $audioFilePath = $video->getAudioFileAbsolutePath();
                if(!empty($audioFilePath)) {
                    if($copyFromRemote) {
                        $this->copyFileFromRemoteServer($video->getAudioFileWebPath(), $video->getAudioFileHelperAbsolutePath());
                    } elseif(is_file($audioFilePath)) {
                        copy($video->getAudioFileAbsolutePath(), $video->getAudioFileHelperAbsolutePath());
                    }
                }


                // copy system audio
                $systemAudio = $video->getSystemAudio();
                if(is_object($systemAudio)) {
                    $systemAudioPath = $systemAudio->getAudioFileAbsolutePath();
                    if(!empty($systemAudioPath)) {
                        if($copyFromRemote) {
                            $this->copyFileFromRemoteServer($systemAudio->getAudioFileWebPath(), $video->getSystemAudioHelperPath());
                        } elseif(is_file($systemAudioPath)) {
                            copy($systemAudioPath, $video->getSystemAudioHelperPath());
                        }
                    }
                }

                // make watermarks
                $userImageWebPath = $video->getUser()->getImageWebPath();
                if(!empty($userImageWebPath)) {
                    $this->copyFileFromRemoteServer($video->getUser()->getImageWebPath(), $video->getUser()->getImageAbsolutePath());
                }

                $watermark_path_1 = $this->generateProfileNameWatermark($video->getUser(), $koosh);
                if(is_file($watermark_path_1)) {
                    copy($watermark_path_1, $video->getUser()->getNameWatermarkImageHelperPath($koosh));
                }
                $watermark_path_2 = $this->generateProfilePictureWatermark($video->getUser());
                if(is_file($watermark_path_2)) {
                    copy($watermark_path_2, $video->getUser()->getRoundCorneredWatermarkImageHelperPath($koosh));
                }

            } else {
                $image = $item['object'];
                if($copyFromRemote) {
                    $this->copyFileFromRemoteServer($image->getImageFileWebPath(), $image->getImageFileHelperAbsolutePath());
                } else {
                    copy($image->getImageFileAbsolutePath(), $image->getImageFileHelperAbsolutePath());
                }

                // copy system audio
                $systemAudio = $image->getSystemAudio();
                if(is_object($systemAudio)) {
                    $systemAudioPath = $systemAudio->getAudioFileAbsolutePath();
                    if(!empty($systemAudioPath)) {
                        if($copyFromRemote) {
                            $this->copyFileFromRemoteServer($systemAudio->getAudioFileWebPath(), $image->getSystemAudioHelperPath());
                        } else {
                            if(is_file($systemAudioPath)) {
                                copy($systemAudioPath, $image->getSystemAudioHelperPath());
                            }
                        }

                    }
                }

                // make watermarks
                $userImageWebPath = $image->getUser()->getImageWebPath();
                if(!empty($userImageWebPath)) {
                    $this->copyFileFromRemoteServer($image->getUser()->getImageWebPath(), $image->getUser()->getImageAbsolutePath());
                }

                $watermark_path_1 = $this->generateProfileNameWatermark($image->getUser(), $koosh);
                if(is_file($watermark_path_1)) {
                    copy($watermark_path_1, $image->getUser()->getNameWatermarkImageHelperPath($koosh));
                }
                $watermark_path_2 = $this->generateProfilePictureWatermark($image->getUser());
                if(is_file($watermark_path_2)) {
                    copy($watermark_path_2, $image->getUser()->getRoundCorneredWatermarkImageHelperPath($koosh));
                }
            }

            // copy main audio - uploaded audio
            $audioFilePath = $koosh->getAudioFileAbsolutePath();
            if(!empty($audioFilePath)) {
                if($copyFromRemote) {
                    $this->copyFileFromRemoteServer($koosh->getAudioFileWebPath(), $koosh->getAudioFileHelperAbsolutePath());
                } else {
                    if(is_file($audioFilePath)) {
                        copy($koosh->getAudioFileAbsolutePath(), $koosh->getAudioFileHelperAbsolutePath());
                    }
                }
            }

            // copy main audio - system audio
            $systemAudio = $koosh->getSystemAudio();
            if(is_object($systemAudio)) {
                $systemAudioPath = $systemAudio->getAudioFileAbsolutePath();
                if(!empty($systemAudioPath)) {
                    if($copyFromRemote) {
                        $this->copyFileFromRemoteServer($systemAudio->getAudioFileWebPath(), $koosh->getSystemAudioHelperPath());
                    } else {
                        if(is_file($systemAudioPath)) {
                            copy($systemAudioPath, $koosh->getSystemAudioHelperPath());
                        }
                    }
                }
            }
        }
        return;
    }

    protected function compileVideoItem($kooshId, $fadeType, $item, $videoInfo, $key, $totalVideoDuration, $freezeImageName, $koosh, $ffmpeg, $ffmpegServerPath, $ffprobe, $highestSampleRate, $fadeInTime, $fadeOutTime, $resizeWidth, $resizeHeight, $highestFrameRate, $finalMergeVideoList)
    {
        $video = $item['object'];
        try {
            $totalVideoDuration += $videoInfo[$key]['duration'];
            if (empty($key)) {
                $firstVideoDuration = $videoInfo[$key]['duration'];
            }

            // Rotate video if needed
            //$videoInfo = $this->rotateVideo($videoInfo, $key, $ffmpegServerPath, $video);

            if (empty($key) || empty($freezeImageName)) {
                // Generate video freeze frame
                $freezeImageName = $this->generateVideoFreezeFrame($kooshId, $koosh, $ffmpeg, $video, $firstVideoDuration, "");
            }

            $watermark_path_1 = $video->getUser()->getNameWatermarkImageHelperPath($koosh);
            $watermark_path_2 = $video->getUser()->getRoundCorneredWatermarkImageHelperPath($koosh);
            $output_path = $video->getVideoFileHelperAbsolutePath()."_final.mkv";
            $this->generateSingleClipVideo($ffmpegServerPath, $video->getVideoFileHelperAbsolutePath()."", $output_path, $video, $resizeWidth, $resizeHeight, $watermark_path_1, $watermark_path_2, $highestSampleRate, $highestFrameRate);


            // Add video to final merge list
            $finalMergeVideoList[] = $output_path;

        } catch (ProcessFailedException $e) {
            $this->logger->addError('FFMPEG  make single clip video error: ' . $e->getMessage());
            $this->markAsUnfinished($kooshId, $e->getMessage());
        }

        return array($totalVideoDuration, $freezeImageName, $highestSampleRate, $finalMergeVideoList);
    }

    protected function compileImageItem($kooshId, $fadeType, $highestSampleRate, $highestFrameRate, $image, $key, $freezeImageName, $koosh, $imageHelpFilePath, $resizeWidth, $resizeHeight, $ffmpegServerPath, $singleVideoDuration, $fadeInTime, $fadeOutTime, $finalMergeVideoList, $totalVideoDuration, $deleteFilesList)
    {
        $highestSampleRate = !empty($highestSampleRate) ? $highestSampleRate : 44100;
        $highestFrameRate = !empty($highestFrameRate) ? $highestFrameRate : 30;
        $this->logger->addInfo('IMAGE highest frameRate:' . $highestFrameRate);
        $this->logger->addInfo('IMAGE highest sampleRate:' . $highestSampleRate);

        if (is_object($image)) {

            // Create helper image
            $parmanentHelperFilePath = $this->createImageHelperFile($image, $koosh);

            if (empty($key) || (empty($freezeImageName))) {
                // Store final video freezframe
                $freezeImageName = sha1(uniqid(mt_rand(), true)) . '.jpg';
                $imageFilePath = $koosh->getHelpersAbsolutePath() . $freezeImageName;
                try {
                    copy($parmanentHelperFilePath, $imageFilePath);
                } catch (Exception $e) {
                    $this->logger->addError('FFMPEG get freeze image error: ' . $e->getMessage());
                    $this->markAsUnfinished($kooshId, $e->getMessage());
                }
            }

            // Convert helper image to png (http://www.imagemagick.org/Usage/resize/#resize)
            $imageHelpFilePathPad = $this->convertImageToPng($kooshId, $key, $imageHelpFilePath, $parmanentHelperFilePath);

            try {

                // generate single image video
                $singleVideoFilePath = $imageHelpFilePathPad . "_single.mkv";
                $watermark_path_1 = $image->getUser()->getNameWatermarkImageHelperPath($koosh);
                $watermark_path_2 = $image->getUser()->getRoundCorneredWatermarkImageHelperPath($koosh);
                $this->generateSingleImageVideo($ffmpegServerPath, $imageHelpFilePathPad, $singleVideoFilePath, $image, $resizeWidth, $resizeHeight, $watermark_path_1, $watermark_path_2);

            } catch (ProcessFailedException $e) {
                $this->logger->addError('FFMPEG make single image video error: ' . $e->getMessage());
                $this->markAsUnfinished($kooshId, $e->getMessage());
            }


            // Add video to final merge list
            $finalMergeVideoList[] = $singleVideoFilePath;

            $totalVideoDuration += $singleVideoDuration;

        }
        return array($highestSampleRate, $highestFrameRate, $freezeImageName, $finalMergeVideoList, $totalVideoDuration, $deleteFilesList);
    }

    /**
     * @param $kooshId
     * @param $freezeImageName
     * @param $koosh
     * @param $ffmpeg
     * @param $video
     * @param $firstVideoDuration
     * @return array
     */
    protected function generateVideoFreezeFrame($kooshId, $koosh, $ffmpeg, $video, $firstVideoDuration, $suffix='')
    {
        $freezeImageName = sha1(uniqid(mt_rand(), true)) . '.jpg';
        $imageFilePath = $koosh->getHelpersAbsolutePath() . $freezeImageName;
        try {
            $finalVideo = $ffmpeg->open($video->getVideoFileHelperAbsolutePath() . $suffix);
            $finalVideo->frame(TimeCode::fromSeconds($firstVideoDuration > 4 ? 4 : 1))
                ->save($imageFilePath, true);
        } catch (Exception $e) {
            $this->logger->addError('FFMPEG get freeze image error: ' . $e->getMessage());
            $this->markAsUnfinished($kooshId, $e->getMessage());
        }
        return $freezeImageName;
    }

    protected function rotateVideo($videoInfo, $key, $ffmpegServerPath, $video)
    {
        $rotateFlag = (isset($videoInfo[$key]['rotate']) && !empty($videoInfo[$key]['rotate'])) ? $videoInfo[$key]['rotate'] : '';
        $rotation = '';
        if ($rotateFlag == '90') {
            $rotation = 'transpose=1 -metadata:s:v:0 rotate=0';
        } elseif (($rotateFlag == '270') || ($rotateFlag == '-90')) {
            $rotation = 'transpose=2 -metadata:s:v:0 rotate=0';
        } elseif($rotateFlag == '180') {
            $rotation = '"transpose=2,transpose=2"';
        }

        if (!empty($rotation)) {
            $rotationCmd = $ffmpegServerPath . "ffmpeg -i " . $video->getVideoFileHelperAbsolutePath() . " -vf " . $rotation . " " . $this->videoCodecSetting . $video->getVideoFileHelperAbsolutePath() . "_rotation.mp4";
            $this->logger->addInfo('ROTATION PROCESS: ' . $rotationCmd);
            $process = new Process($rotationCmd);
            $process->setTimeout(3600);
            $process->mustRun();

            $metaHeight = $videoInfo[$key]['height'];
            $videoInfo[$key]['height'] = $videoInfo[$key]['width'];
            $videoInfo[$key]['width'] = $metaHeight;
        }

        copy($video->getVideoFileHelperAbsolutePath() . (!empty($rotation) ? '_rotation.mp4' : ''), $video->getVideoFileHelperAbsolutePath() . "_rotated.mp4");
        return $videoInfo;
    }


    protected function unifyAudioChannels($ffmpegServerPath, $highestSampleRate, $convertSampleRate, $video, $addAudio)
    {
        $convertSampleRateString = ($convertSampleRate) ? " -ar " . $highestSampleRate : "";
        $process = new Process($ffmpegServerPath . "ffmpeg -i " . $video->getVideoFileHelperAbsolutePath() . (($addAudio) ? "_add_audio" : "_caption") . ".mp4 -codec:v copy -af pan=\"mono: c0=FL\" " . $convertSampleRateString . " " . $this->videoCodecSetting . $video->getVideoFileHelperAbsolutePath() . "_mono.mp4");
        $process->setTimeout(3600);
        $process->mustRun();
        $monoConversion = true;

        $this->logger->addInfo('Mono conversion process....');
        return $monoConversion;
    }

    protected function convertSampleRate($ffmpegServerPath, $highestSampleRate, $video, $addAudio)
    {
        $process = new Process($ffmpegServerPath . "ffmpeg -i " . $video->getVideoFileHelperAbsolutePath() . (($addAudio) ? "_add_audio" : "_caption") . ".mp4 -codec:v copy -ar " . $highestSampleRate . " " . $this->videoCodecSetting . $video->getVideoFileHelperAbsolutePath() . "_mono.mp4");
        $process->setTimeout(3600);
        $process->mustRun();
        $monoConversion = true;
        return $monoConversion;
    }

    protected function addAudioFade($videoInfo, $key, $ffmpegServerPath, $fadeInTime, $fadeOutTime, $video, $monoConversion)
    {
        $process = new Process($ffmpegServerPath . "ffmpeg -i " . $video->getVideoFileHelperAbsolutePath() . (($monoConversion) ? '_mono' : '_caption') . ".mp4 -af afade=t=in:ss=0:d=" . $fadeInTime . ",afade=t=out:st=" . ($videoInfo[$key]['duration'] - $fadeOutTime) . ":d=" . $fadeOutTime . " " . $this->videoCodecSetting . $video->getVideoFileHelperAbsolutePath() . "_fade.mp4");
        $process->setTimeout(3600);
        $process->mustRun();

        return $process;
    }

    protected function scaleVideo($ffmpegServerPath, $resizeWidth, $resizeHeight, $highestFrameRate, $video)
    {
        $process = new Process($ffmpegServerPath . "ffmpeg -i " . $video->getVideoFileHelperAbsolutePath() . "_fade.mp4 -vf: \"scale=trunc((iw*min($resizeWidth/iw\,$resizeHeight/ih))/2)*2:trunc((ih*min($resizeWidth/iw\,$resizeHeight/ih))/2)*2\" -preset:v ultrafast -r " . $highestFrameRate . " -maxrate 10M " . $this->videoCodecSetting . $video->getVideoFileHelperAbsolutePath() . "_fade_scaled.mp4");
        $process->setTimeout(3600);
        $process->mustRun();
        $scaleVideo = true;

        return $scaleVideo;
    }

    protected function fillBlackWithBlurredBackground($ffmpegServerPath, $resizeWidth, $resizeHeight, $highestFrameRate, $video, $scaleVideo)
    {
        $process = new Process($ffmpegServerPath . "ffmpeg -i " . $video->getVideoFileHelperAbsolutePath() . "_fade" . (($scaleVideo) ? '_scaled' : '') . ".mp4 -lavfi \"scale=$resizeWidth:$resizeHeight,boxblur=luma_radius=min(h\,w)/40:luma_power=3:chroma_radius=min(cw\,ch)/40:chroma_power=1[bg];[bg][0:v]overlay=(W-w)/2:(H-h)/2,setsar=1 \" -preset:v ultrafast -r " . $highestFrameRate . " -maxrate 10M " . $this->videoCodecSetting . $video->getVideoFileHelperAbsolutePath() . "_resized.mp4", null, null, null, null);
        $process->setTimeout(3600);
        $process->mustRun();
    }

    protected function createImageHelperFile($image, $koosh)
    {
        $parmanentHelperFilePath = $koosh->getHelpersAbsolutePath() . 'helper_' . $image->getImageFilePath();
        //copy($image->getImageFileHelperAbsolutePath(), $parmanentHelperFilePath);

        $image = new \Imagick($image->getImageFileHelperAbsolutePath());
        $this->autoRotateImage($image);
        $image->writeImage($parmanentHelperFilePath);


        return $parmanentHelperFilePath;
    }

    public function autoRotateImage($image) {
        $orientation = $image->getImageOrientation();
        $this->logger->addInfo('IMAGICK image orientation:' . $orientation);

        switch($orientation) {
            case \Imagick::ORIENTATION_BOTTOMRIGHT:
                $image->rotateimage("#000", 180); // rotate 180 degrees
                break;

            case \Imagick::ORIENTATION_RIGHTTOP:
                $image->rotateimage("#000", 90); // rotate 90 degrees CW
                break;

            case \Imagick::ORIENTATION_LEFTBOTTOM:
                $image->rotateimage("#000", -90); // rotate 90 degrees CCW
                break;
        }

        // Now that it's auto-rotated, make sure the EXIF data is correct in case the EXIF gets saved with the image!
        $image->setImageOrientation(\Imagick::ORIENTATION_TOPLEFT);
    }

    /**
     * @param $kooshId
     * @param $key
     * @param $imageHelpFilePath
     * @param $parmanentHelperFilePath
     * @return array
     */
    protected function convertImageToPng($kooshId, $key, $imageHelpFilePath, $parmanentHelperFilePath)
    {
        $imageHelpFilePathPad = $imageHelpFilePath . str_pad($key, 3, "0", STR_PAD_LEFT) . '.png';
        $process = new Process("convert " . $parmanentHelperFilePath . " " . $imageHelpFilePathPad);
        $process->setTimeout(3600);
        try {
            $process->mustRun();
        } catch (ProcessFailedException $e) {
            $this->logger->addError('CONVERT resize and convert image to png  error: ' . $e->getMessage());
            $this->markAsUnfinished($kooshId, $e->getMessage());
        }
        return $imageHelpFilePathPad;
    }

    protected function getImageAspectRatio($ffmpegServerPath, $source_path)
    {
        // ffprobe -loglevel error -show_entries stream=display_aspect_ratio -of default=nw=1:nk=1 images/78807ad3394c65aba2a5e95bdf3a52d86b642951.jpeg
        $cmd =
            "${ffmpegServerPath}ffprobe"
            ." -loglevel error"
            ." -show_entries stream=display_aspect_ratio"
            ." -of default=nw=1:nk=1"
            ." $source_path";
        $process = new Process($cmd, null, null, null, null);
        $process->setTimeout(3600);
        $process->mustRun();

        if($process->isSuccessful()) {
            return $process->getOutput();
        }

        return 0;
    }

    protected function getVideoDuration($ffmpegServerPath, $source_path)
    {
        // ffprobe -loglevel error -show_entries format=duration -of default=nw=1:nk=1 input.mp4
        $cmd =
            "${ffmpegServerPath}ffprobe"
            ." -loglevel error"
            ." -show_entries format=duration"
            ." -of default=nw=1:nk=1"
            ." $source_path";
        $process = new Process($cmd, null, null, null, null);
        $process->setTimeout(3600);
        $process->mustRun();

        if($process->isSuccessful()) {
            return $process->getOutput();
        }

        return 0;
    }

    protected function checkVideoAudio($ffmpegServerPath, $source_path)
    {
        // ffprobe -i INPUT -show_streams -select_streams a -loglevel error
        $cmd =
            "${ffmpegServerPath}ffprobe"
            ." -i $source_path"
            ." -loglevel error"
            ." -show_streams"
            ." -select_streams a";
        $process = new Process($cmd, null, null, null, null);
        $process->setTimeout(3600);
        $process->mustRun();

        if($process->isSuccessful()) {
            return $process->getOutput();
        }

        return '';
    }

    protected function scaleImage($resizeWidth, $resizeHeight, $imageHelpFilePathPad)
    {
        list($imgWidth, $imgHeight, $imgType, $imgAttr) = getimagesize($imageHelpFilePathPad);
        $scaleImage = "-vf: \"scale=trunc(in_w/2)*2:trunc(in_h/2)*2\"";
        if (($imgWidth > $resizeWidth) || ($imgHeight > $resizeHeight)) {
            $scaleImage = "-vf: \"scale=trunc((iw*min($resizeWidth/iw\,$resizeHeight/ih))/2)*2:trunc((ih*min($resizeWidth/iw\,$resizeHeight/ih))/2)*2\"";
            return $scaleImage;
        }
        return $scaleImage;
    }

    protected function generateSingleClipVideo($ffmpegServerPath, $source_path, $output_path, $image, $width, $height, $watermark_path_1, $watermark_path_2, $highestSampleRate, $highestFrameRate)
    {
        // get image aspect ratio
        $video_duration = trim($this->getVideoDuration($ffmpegServerPath, $source_path));

        // get user name watermark image size
        list($nameImageWidth, $nameImageHeight, $type, $attr) = getimagesize($watermark_path_1);

        // add audio
        $add_audio = false;
        $disableSound = ($image->getMuted() == '1') ? true : false;
        if (($image->getKoosh()->getAddAudio() == 'clips') && (empty($disableSound)) && ((is_object($image->getSystemAudio()) && is_file($image->getSystemAudio()->getAudioFileAbsolutePath())))) {
            $add_audio = true;
            $audio_path = $image->getSystemAudio()->getAudioFileHelperAbsolutePath();
        }

        // check video audio
        $found_audio = $this->checkVideoAudio($ffmpegServerPath, $source_path);

        // check final koosh audio
        $finalKooshAudio = false;
        if(($image->getKoosh()->getAddAudio() == 'koosh') || ($image->getKoosh()->getAddAudio() == 'system_audio_koosh')) {
            $finalKooshAudio = true;
        }

        $add_silent = false;
        if(!$add_audio && (empty($found_audio) || $disableSound || $finalKooshAudio)){
            $add_silent = true;
        }

        // add caption
        $caption_cmd = $this->prepareCaptionCmd($image, $width);

        $cmd =
            "${ffmpegServerPath}ffmpeg -y"
            ." -i $source_path"
            .(($add_silent) ? " -t $video_duration -f lavfi -i anullsrc=channel_layout=mono:sample_rate=44100" : "")
            ." -framerate 1 -i $watermark_path_1"
            ." -framerate 1 -i $watermark_path_2"
            .(($add_audio) ? " -i $audio_path" : "")
            ." -filter_complex"
            ." \""
            ." [0:v]fps=ntsc,scale=iw*max($width/iw\,$height/ih):ih*max($width/iw\,$height/ih),crop=$width:$height,boxblur=20,curves=preset=darker[bg];"
            ." [0:v]fps=ntsc,scale=iw*min($width/iw\,$height/ih):ih*min($width/iw\,$height/ih)[fg];"
            ." [bg][fg]overlay=(main_w-overlay_w)/2:(main_h-overlay_h)/2[bgfg];"
            ." [bgfg][".(($add_silent) ? "2" : "1").":v]overlay=main_w-overlay_w-140:main_h-overlay_h-50[ni];"
            ." [ni][".(($add_silent) ? "3" : "2").":v]overlay=main_w-overlay_w-20:main_h-overlay_h-20".$caption_cmd.",setsar=1,format=yuv420p,fps=".$highestFrameRate."[v]"
            .((!$add_audio && !empty($found_audio) && !$disableSound && !$finalKooshAudio) ? "; [0:a]aformat=sample_rates=44100:channel_layouts=mono,afade=t=in:ss=0:d=0.5,afade=t=out:st=".($video_duration-0.5).":d=0.5[a]" : "")
            .(($add_audio) ? "; [3:a]aformat=sample_rates=44100:channel_layouts=mono,aloop=-1:size=".$video_duration.",atrim=duration=".($video_duration-0.5)."[aa];" : "")
            .(($add_audio) ? " [0:a][aa]amerge=inputs=2,afade=t=in:ss=0:d=0.5,afade=t=out:st=".($video_duration-0.5).":d=0.5[a]" : "")
            ."\""
            ." -map \"[v]\" -map ".((!$add_silent) ? "\"[a]\"" : "1:a")." -c:v libx264 -crf 0 -preset ultrafast -c:a pcm_s16le -ac 1 -shortest"
            ." $output_path";

        $process = new Process($cmd, null, null, null, null);
        $process->setTimeout(3600);
        $process->mustRun();
    }

    protected function generateSingleImageVideo($ffmpegServerPath, $source_path, $output_path, $image, $width, $height, $watermark_path_1, $watermark_path_2)
    {
        // get image aspect ratio
        $aspectRatio = $this->getImageAspectRatio($ffmpegServerPath, $source_path);

        // get user name watermark image size
        list($nameImageWidth, $nameImageHeight, $type, $attr) = getimagesize($watermark_path_1);

        // add audio
        $add_audio = false;
        if (($image->getKoosh()->getAddAudio() == 'clips') && ((is_object($image->getSystemAudio()) && is_file($image->getSystemAudio()->getAudioFileHelperAbsolutePath())))) {
            $add_audio = true;
            $audio_path = $image->getSystemAudio()->getAudioFileHelperAbsolutePath();
        }

        // add caption
        $caption_cmd = $this->prepareCaptionCmd($image, $width);

        if($aspectRatio == '4:3') {
            $cmd =
                "${ffmpegServerPath}ffmpeg -y"
                ." -framerate ntsc -loop 1 -t 5 -i $source_path"
                .((!$add_audio) ? " -t 5 -f lavfi -i anullsrc=channel_layout=mono:sample_rate=44100" : "")
                ." -framerate ntsc -i $watermark_path_1"
                ." -framerate ntsc -i $watermark_path_2"
                .(($add_audio) ? " -i $audio_path" : "")
                ." -filter_complex"
                ." \""
                ."[0:v]scale=640:480[bg];"
                ." [bg][".(($add_audio) ? "1" : "2").":v]overlay=main_w-overlay_w-140:main_h-overlay_h-50[ni];"
                ." [ni][".(($add_audio) ? "2" : "3").":v]overlay=main_w-overlay_w-20:main_h-overlay_h-20,setsar=1,format=yuv420p[v]"
                .(($add_audio) ? "; [3:a]aformat=sample_rates=44100:channel_layouts=mono,aloop=-1:size=2147480000,atrim=duration=5,afade=t=in:ss=0:d=0.5,afade=t=out:st=4.5:d=0.5[a]" : "")
                ."\""
                ." -map \"[v]\" -map ".(($add_audio) ? "\"[a]\"" : "1:a")." -c:v libx264 -crf 0 -preset ultrafast -c:a pcm_s16le -shortest"
                ." $output_path";
        } else {
            $cmd =
                "${ffmpegServerPath}ffmpeg -y"
                ." -framerate ntsc -loop 1 -t 5 -i $source_path"
                .((!$add_audio) ? " -t 5 -f lavfi -i anullsrc=channel_layout=mono:sample_rate=44100" : "")
                ." -framerate ntsc -i $watermark_path_1"
                ." -framerate ntsc -i $watermark_path_2"
                .(($add_audio) ? " -i $audio_path" : "")
                ." -filter_complex"
                ." \""
                ."[0:v]scale=iw*max($width/iw\,$height/ih):ih*max($width/iw\,$height/ih),crop=$width:$height,boxblur=20,curves=preset=darker[bg];"
                ." [0:v]scale=iw*min($width/iw\,$height/ih):ih*min($width/iw\,$height/ih)[fg];"
                ." [bg][fg]overlay=(main_w-overlay_w)/2:(main_h-overlay_h)/2[bgfg];"
                ." [bgfg][".(($add_audio) ? "1" : "2").":v]overlay=main_w-overlay_w-140:main_h-overlay_h-50[ni];"
                ." [ni][".(($add_audio) ? "2" : "3").":v]overlay=main_w-overlay_w-20:main_h-overlay_h-20".$caption_cmd.",setsar=1,format=yuv420p[v]"
                //.(($add_audio) ? " [3:a]aformat=sample_rates=44100:channel_layouts=mono,aloop=-1:size=2147480000,atrim=duration=5,afade=t=in:ss=0:d=0.5,afade=t=out:st=4.5:d=0.5[a]" : "")
                .(($add_audio) ? "; [3:a]aformat=sample_rates=44100:channel_layouts=mono,aloop=-1:size=2147480000,atrim=duration=5,afade=t=in:ss=0:d=0.5,afade=t=out:st=4.5:d=0.5[a]" : "")
                ."\""
                ." -map \"[v]\" -map ".(($add_audio) ? "\"[a]\"" : "1:a")." -c:v libx264 -crf 0 -preset ultrafast -c:a pcm_s16le -shortest"
                ." $output_path";
        }

        $process = new Process($cmd, null, null, null, null);
        $process->setTimeout(3600);
        $process->mustRun();

    }

    protected function makeSingleImageVideo($ffmpegServerPath, $singleVideoDuration, $imageHelpFilePathPad, $scaleImage)
    {
        $singleVideoFilePath = $imageHelpFilePathPad . "_single.mp4";
        $process = new Process($ffmpegServerPath . "ffmpeg -loop 1 -i " . $imageHelpFilePathPad . " -c:v libx264 -preset ultrafast -t " . $singleVideoDuration . " " . $scaleImage . " -pix_fmt yuv420p " . $this->videoCodecSetting . $singleVideoFilePath . "_before_a.mp4");
        $process->setTimeout(3600);
        $process->mustRun();
        return $singleVideoFilePath;
    }

    protected function addMutedAudioChannel($highestSampleRate, $ffmpegServerPath, $singleVideoFilePath)
    {
        $process = new Process($ffmpegServerPath . "ffmpeg -f lavfi -i anullsrc=channel_layout=stereo:sample_rate=" . $highestSampleRate . " -i " . $singleVideoFilePath . "_before_a.mp4 -shortest -c:v copy -c:a libvo_aacenc " . $this->videoCodecSetting . $singleVideoFilePath . "_before.mp4");
        $process->setTimeout(3600);
        $process->mustRun();
    }

    protected function fillImageBlackWithBlurredBackground($highestFrameRate, $resizeWidth, $resizeHeight, $ffmpegServerPath, $singleVideoFilePath)
    {
        $process = new Process($ffmpegServerPath . "ffmpeg -i " . $singleVideoFilePath . "_before.mp4 -lavfi \"scale=$resizeWidth:$resizeHeight,boxblur=luma_radius=min(h\,w)/40:luma_power=3:chroma_radius=min(cw\,ch)/40:chroma_power=1[bg];[bg][0:v]overlay=(W-w)/2:(H-h)/2,setsar=1 \" -preset:v ultrafast -r " . $highestFrameRate . " -maxrate 10M " . $this->videoCodecSetting . $singleVideoFilePath);
        $process->setTimeout(3600);
        $process->mustRun();
    }

    protected function addImageAudioFade($ffmpegServerPath, $singleVideoDuration, $fadeInTime, $fadeOutTime, $singleVideoFilePath)
    {
        $process1 = new Process($ffmpegServerPath . "ffmpeg -y -i " . $singleVideoFilePath . " -af afade=t=in:ss=0:d=" . $fadeInTime . ",afade=t=out:st=" . ($singleVideoDuration - $fadeOutTime) . ":d=" . $fadeOutTime . " " . $this->videoCodecSetting . $singleVideoFilePath . "_fade.mp4");
        $process1->setTimeout(3600);
        $process1->mustRun();
    }

    /**
     * @param $kooshId
     * @param $fadeType
     * @param $finalMergeVideoList
     * @param $finalVideoFilePath
     * @param $resizeWidth
     * @param $resizeHeight
     * @param $highestFrameRate
     * @return array
     */
    protected function concatItemsWithMelt($koosh, $fadeType, $finalMergeVideoList, $finalVideoFilePath, $resizeWidth, $resizeHeight, $highestFrameRate, $meltServerPath)
    {

        try {
            // Prepare MLT MELT mix command
            $transition = $this->em->getRepository('KAPIKooshApiBundle:Transition')->findOneBy(array('code' => $fadeType));
            $transitionCmd = 'luma:' . (is_object($transition) ? $transition->getTransitionMaskFileHelperAbsolutePath($koosh) : '');
            $softness = '0.4';
            $transitionTime = 30;
            $mixingInputsCmd = '';
            if (sizeof($finalMergeVideoList)) {
                foreach ($finalMergeVideoList AS $key => $filePath) {
                    $mixingInputsCmd .= $filePath . ((!empty($key) && !empty($fadeType) && ($fadeType != 'none')) ? ' -mix ' . $transitionTime . ' -mixer ' . $transitionCmd . ' softness=' . $softness : '') . ' ';
                }
            }
            // Lou's script
            $mlt_cmd = $meltServerPath . " "
                . $mixingInputsCmd
                . " -silent"
                . " -mixer mix:-1"
                . " -consumer avformat:" . $finalVideoFilePath
                //. " vcodec=libx264"
                . " vcodec=ffv1"
                . " crf=0"
                . " acodec=pcm_s16le ac=1 ar=44100";


            $mlt_cmd = $meltServerPath . " "
                . $mixingInputsCmd
                . " -silent"
                . " -mixer mix:-1"
                . " -consumer avformat:" . $finalVideoFilePath
                . " vcodec=libx264"
                . " crf=0"
                . " acodec=libmp3lame ac=1 ar=44100"
                //. " real_time=-" . $this->meltThreadsSetting
                . " width=" . $resizeWidth
                . " height=" . $resizeHeight
                //. " frame_rate_num=" . $highestFrameRate
                . " ";


            $this->logger->addInfo('Mlt MELT process: ' . $mlt_cmd);

            $process = new Process($mlt_cmd, null, null, null, null);
            $process->setTimeout(3600);
            $process->mustRun();
            $this->logger->addInfo('Mlt MELT success');

        } catch (ProcessFailedException $e) {
            $this->logger->addError('FFMPEG concat videos error: ' . $e->getMessage());
            $this->markAsUnfinished($koosh->getId(), $e->getMessage());
        }
    }



    /**
     * @param $koosh
     */
    protected function sendPushNotificationsToInvolvedUsers($koosh)
    {
        $users = $this->em->getRepository('KAPIKooshApiBundle:User')->getAllUsersInvolvedByKoosh($koosh->getId(), $koosh->getType());
        if (sizeof($users)) {
            foreach ($users AS $userTo) {

                if ($this->container->getParameter('bool_send_push_notifications')) {
                    $notificationMessage = 'Koosh: ' . $koosh->getTitle() . ' has been completed.';

                    // Send PUSH notification - IOs
                    try {
                        $iosMessage = new iOSMessage();
                        $iosMessage->setMessage($notificationMessage);
                        $iosMessage->setDeviceIdentifier($userTo->getPushId());

                        $this->container->get('rms_push_notifications')->send($iosMessage);
                    } catch (Exception $ex) {
                        $this->logger->addError('Unable to send iOs PUSH notification: ' . $ex->getMessage());
                    }
                    // Send PUSH notification - Android
                    try {
                        $androidMessage = new AndroidMessage();
                        $androidMessage->setGCM(true);
                        $androidMessage->setMessage($notificationMessage);
                        $androidMessage->setDeviceIdentifier($userTo->getPushId());

                        $this->container->get('rms_push_notifications')->send($androidMessage);
                    } catch (Exception $ex) {
                        $this->logger->addError('Unable to send android PUSH notification ' . $ex->getMessage());
                    }
                }
            }
        }
    }


}  