<?php
 
namespace KAPI\KooshApiBundle\Rabbit\Producer;
 
class AddVideoTask
{
    private $producer;
    private $em;
 
    public function __construct($producer, $em)
    {
        $this->producer = $producer;
        $this->em = $em; 
    }
    
    public function process($params)
    {
        
        $koosh = $this->em->getRepository('KAPIKooshApiBundle:Koosh')->findOneBy(array('id' => $params['kooshId']));
        if(is_object($koosh)) {
            $koosh->setStatus(5); // set PENDING state (actually in the rabbitMq que)
            $koosh->setRabbitmqCounter(0);
            $koosh->setErrorMessage('');
            $koosh->setAudioFile(null);
            
            $this->em->persist($koosh);
            $this->em->flush();
        }
        
        $msg = serialize(array(
            'type' => 'process-koosh', 
            'kooshId' => $params['kooshId'], 
            'items' => $params['items'], 
            'fadeType' => $params['fadeType']
        ));  

        $this->producer->publish($msg);
    }
}