<?php
namespace KAPI\KooshApiBundle\Repository;

use Doctrine\ORM\EntityRepository;
use KAPI\KooshApiBundle\Entity\Friendship;

class FriendshipRepository extends EntityRepository
{
    public function checkExistingFriendship($userIdFrom, $userIdTo)
    {
        
        $query = $this->createQueryBuilder('f')
        ->where('(f.userIdFrom = :userIdFrom AND f.userIdTo = :userIdTo) OR (f.userIdFrom = :userIdTo AND f.userIdTo = :userIdFrom)')
        ->setParameter('userIdFrom', $userIdFrom)
        ->setParameter('userIdTo', $userIdTo)
        ->getQuery();
        $result = $query->getResult();
        return ($result) ? true : false;
    }
    
    public function getUserFriends($userId) {
        $query = $this->createQueryBuilder('f')
        ->where("((f.userIdFrom = :userId) OR (f.userIdTo = :userId)) AND f.status = '1'")
        ->setParameter('userId', $userId)
        ->getQuery();
        
        return $query->getResult();
    }
    
    public function getUserFriendsObjects($userId) {
        $query = $this->createQueryBuilder('f')
        ->where("((f.userIdFrom = :userId) OR (f.userIdTo = :userId)) AND f.status = '1'")
        ->setParameter('userId', $userId)
        ->getQuery();
        $result = $query->getResult();
        
        $objects = array();
        if(sizeof($result)) {
            foreach($result AS $friend) {
                if($friend->getUserIdFrom() != $userId) {
                    $objects[] = $friend->getUserFrom();
                } else {
                    $objects[] = $friend->getUserTo();
                }
            }
        }
        
        return $objects;
    }
}
