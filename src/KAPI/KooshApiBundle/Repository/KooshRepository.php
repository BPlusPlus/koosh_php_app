<?php
namespace KAPI\KooshApiBundle\Repository;

use Doctrine\ORM\EntityRepository;
use KAPI\KooshApiBundle\Entity\Koosh;

class KooshRepository extends EntityRepository
{
    /**
     * 
     * @param int $userId
     * @param string $searchString
     * @return type
     */
    public function findAllBySearchString($searchString, $genreString,  $limit = 0) {
         
        
        $whereSearch = (!empty($searchString)) ? " AND (k.title LIKE :searchString OR u.firstName LIKE :searchString OR u.lastName LIKE :searchString OR u.username LIKE :searchString)" : "";
        $whereGenre = (!empty($genreString) && (strtolower($genreString) != 'all')) ? " AND (k.genre = :genreString)" : "";
        
        $query = $this->createQueryBuilder('k')
        ->innerJoin('k.user','u')
        ->where("1 = 1 " . $whereSearch . $whereGenre . " AND k.status = '3'");
        if(!empty($searchString)) {
            $query = $query->setParameter('searchString', '%'.$searchString.'%');
        }
        if(!empty($genreString) && (strtolower($genreString) != 'all')) {
            $query = $query->setParameter('genreString', $genreString);
        }
        $query = $query
        ->orderBy('k.created', 'DESC')
        ->getQuery();
        
        if(!empty($limit)) {
            //$query->setMaxResults($limit);
        }
        
        //$query = $query->getQuery();
        
        return $query->getResult();
    }
    
    public function findAllIncomplete($userId) {
        $query = $this->createQueryBuilder('k')
        ->where("(k.status = '0') AND k.userId = :userId")
        ->setParameter('userId', $userId)
        ->orderBy('k.created', 'DESC')
        ->getQuery();
        
        
        return $query->getResult();
    }
}
