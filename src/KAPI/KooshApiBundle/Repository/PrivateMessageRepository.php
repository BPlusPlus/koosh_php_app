<?php

namespace KAPI\KooshApiBundle\Repository;

use Doctrine\ORM\EntityRepository;
use KAPI\KooshApiBundle\Entity\PrivateMessage;

/**
 * PrivateMessageRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class PrivateMessageRepository extends EntityRepository
{
    
    /**
     * 
     * @param int $userId
     * @return type
     */
    public function findAllGroupedBySender($userId) {
  
        $em = $this->getEntityManager();
        
        // Permanent array
        $resultData = array();

        $query = $em->createQuery(
            'SELECT pm.id AS messageId, 
            CASE WHEN (pm.userIdFrom = :userId) THEN pm.userIdTo ELSE pm.userIdFrom AS senderId  
            FROM KAPIKooshApiBundle:PrivateMessage pm
            WHERE (pm.userIdFrom = :userId OR pm.userIdTo = :userId)
            GROUP BY senderId 
            ORDER BY pm.created DESC'
        )->setParameter('userId', $userId);
        $messages = $query->getResult();
        
        foreach($messages AS $message) {
            $resultData[] = $this->findOneBy(array('id' => $message['messageId']));
        }
        
        return $resultData;
    }
    
    /**
     * 
     * @param int $userIdTo
     * @param int $userIdFrom
     * @return type
     */
    public function findBySender($userIdTo, $userIdFrom) {
        
        // Permanent array
        $resultData = array();

        $em = $this->getEntityManager();
        $query = $em->createQuery(
            'SELECT pm.id AS messageId 
            FROM KAPIKooshApiBundle:PrivateMessage pm
            WHERE ((pm.userIdFrom = :userIdTo AND pm.userIdTo = :userIdFrom) OR (pm.userIdFrom = :userIdFrom AND pm.userIdTo = :userIdTo))
            ORDER BY pm.created DESC'
        )
        ->setParameter('userIdTo', $userIdTo)
        ->setParameter('userIdFrom', $userIdFrom);
        $messages = $query->getResult();
        
        foreach($messages AS $message) {
            $resultData[] = $this->findOneBy(array('id' => $message['messageId']));
        }
        
        return $resultData;
    }
    
    /**
     * 
     * @param int $userIdTo
     * @param int $userIdFrom
     * @return type
     */
    public function getLatestMessage($userIdTo, $userIdFrom) {
        $query = $this->createQueryBuilder('m')
        ->where("m.userIdTo = :userIdTo AND m.userIdFrom = :userIdFrom")
        ->orderBy('m.created', 'DESC')
        ->setParameter('userIdTo', $userIdTo)
        ->setParameter('userIdFrom', $userIdFrom)
        ->setMaxResults(1)
        ->getQuery();
        
        return $query->getSingleResult();
    }
    
    /**
     * 
     * @param int $userIdTo
     * @param int $userIdFrom
     * @return type
     */
    public function getLatestMessageText($userIdTo, $userIdFrom) {
        $em = $this->getEntityManager();
        $query = $em->createQuery(
            'SELECT pm.message AS message_text
            FROM KAPIKooshApiBundle:PrivateMessage pm
            WHERE ((pm.userIdFrom = :userIdTo AND pm.userIdTo = :userIdFrom) OR (pm.userIdFrom = :userIdFrom AND pm.userIdTo = :userIdTo))
            ORDER BY pm.created DESC'
        )
        ->setParameter('userIdTo', $userIdTo)
        ->setParameter('userIdFrom', $userIdFrom);
        
        $message = $query->setMaxResults(1)->getOneOrNullResult();
        
        return isset($message['message_text']) ? $message['message_text'] : '';
    }
    
}
