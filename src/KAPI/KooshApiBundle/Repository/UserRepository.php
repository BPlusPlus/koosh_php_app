<?php
namespace KAPI\KooshApiBundle\Repository;

use Doctrine\ORM\EntityRepository;
use KAPI\KooshApiBundle\Entity\User;
use KAPI\KooshApiBundle\Entity\Video;
use KAPI\KooshApiBundle\Entity\KooshItem;

class UserRepository extends EntityRepository
{   
    /**
     * 
     * @param type $kooshId
     * @param type $kooshType
     * @return type
     */
    public function findAllInvolvedByKoosh($kooshId, $kooshType = 'koosh_video')
    {
        
        if($kooshType == 'koosh_kapture') {
            
            $query = $this->_em->createQueryBuilder()
            ->select(array('i.id'))
            ->from('KAPI\KooshApiBundle\Entity\Image', 'i')
            ->join('KAPI\KooshApiBundle\Entity\User', 'u', 'WITH', 'i.userId = u.id')
            ->where('i.kooshId = :kid')
            ->setParameter('kid', $kooshId)
            ->getQuery();
            
        } elseif($kooshType == 'koosh_video') {
            
            $query = $this->_em->createQueryBuilder()
            ->select(array('v.id'))
            ->from('KAPI\KooshApiBundle\Entity\Video', 'v')
            ->join('KAPI\KooshApiBundle\Entity\User', 'u', 'WITH', 'v.userId = u.id')
            ->where('v.kooshId = :kid')
            ->setParameter('kid', $kooshId)
            ->getQuery();
             
        } else {
            $query = $this->_em->createQueryBuilder()
            ->select(array('ki.id'))
            ->from('KAPI\KooshApiBundle\Entity\KooshItem', 'ki')
            ->join('KAPI\KooshApiBundle\Entity\User', 'u', 'WITH', 'ki.userId = u.id')
            ->where('ki.kooshId = :kid')
            ->setParameter('kid', $kooshId)
            ->getQuery();
        }
        
        return $query->getResult();
    }
    
    
    /**
     * 
     * @param type $kooshId
     * @param type $kooshType
     * @return type
     */
    public function getAllUsersInvolvedByKoosh($kooshId, $kooshType = 'koosh_video')
    {
        
        if($kooshType == 'koosh_kapture') {
            $query = $this->createQueryBuilder('e')
            ->leftJoin('e.images', 'i')
            ->where('i.kooshId = :id')
            ->setParameter('id', $kooshId)
            ->getQuery();
        } elseif($kooshType == 'koosh_video') {
            $query = $this->createQueryBuilder('e')
            ->leftJoin('e.videos', 'v')
            ->where('v.kooshId = :id')
            ->setParameter('id', $kooshId)
            ->getQuery();
        } else {
            $query = $this->createQueryBuilder('e')
            ->leftJoin('e.kooshItems', 'it')
            ->where('it.kooshId = :id')
            ->setParameter('id', $kooshId)
            ->getQuery();
        }
        
        return $query->getResult();
    }
    
    /**
     * 
     * @param type $kooshId
     * @return type
     */
    public function getAllUsersFromKooshComments($kooshId) {

        $query = $this->createQueryBuilder('e')
                ->leftJoin('e.comments', 'c')
                ->where('c.kooshId = :id')
                ->setParameter('id', $kooshId)
                ->getQuery();

        return $query->getResult();
    }

    /**
     * 
     * @param int $userId
     * @param string $searchString
     * @return type
     */
    public function findAllBySearchString($searchString, $userId) {
        $query = $this->createQueryBuilder('u')
        ->where("((u.username LIKE :searchString) OR (CONCAT(u.firstName, ' ', u.lastName) LIKE :searchString) OR (u.firstName = :searchString) OR (u.lastName = :searchString) OR (u.email = :searchString)) AND u.id != :userId")
        ->setParameter('searchString', '%'.$searchString.'%')
        ->setParameter('userId', $userId)
        ->getQuery();
        
        return $query->getResult();
    }
    
    /**
     * 
     * @param type $kooshId
     * @param type $kooshType
     * @return type
     */
    public function checkInvolvedByKoosh($userId, $kooshId, $kooshType = 'koosh_video')
    {
        
        if($kooshType == 'koosh_kapture') {
            $query = $this->createQueryBuilder('e')
            ->leftJoin('e.images', 'i')
            ->where('i.kooshId = :id AND i.userId = :uid')
            ->setParameter('id', $kooshId)
            ->setParameter('uid', $userId)
            ->getQuery();
        } elseif($kooshType == 'koosh_video') {
            $query = $this->createQueryBuilder('e')
            ->leftJoin('e.videos', 'v')
            ->where('v.kooshId = :id AND v.userId = :uid')
            ->setParameter('id', $kooshId)
            ->setParameter('uid', $userId)
            ->getQuery();
        } else {
            $query = $this->createQueryBuilder('e')
            ->leftJoin('e.kooshItems', 'it')
            ->where('it.kooshId = :id AND it.userId = :uid')
            ->setParameter('id', $kooshId)
            ->setParameter('uid', $userId)
            ->getQuery();
        }
        
        return $query->getResult();
    }
}
