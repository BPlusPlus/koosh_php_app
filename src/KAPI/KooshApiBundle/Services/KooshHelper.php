<?php

namespace KAPI\KooshApiBundle\Services;

use KAPI\KooshApiBundle\Entity\User;
use KAPI\KooshApiBundle\Entity\Koosh;
use KAPI\KooshApiBundle\Entity\Video;
use KAPI\KooshApiBundle\Entity\Image;
use Symfony\Component\DependencyInjection\ContainerInterface;

class KooshHelper
{

    /**
     * @var ContainerInterface
     */
    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param User|string $input a user entity or full size image URL
     * @param bool $absolute
     * @return string
     */
    public function getProfileThumbnailURL($input, $absolute = false)
    {
        $filter = 'profile_image';
        
        if($input instanceof User) {
            $path = $input->getImageWebPath();
        } elseif($input instanceof Koosh) {
            $path = $input->getImageWebPath();
            $filter = 'koosh_image';
        } elseif($input instanceof Video) {
            $path = $input->getImageFileWebPath();
        } elseif($input instanceof Image) {
            $path = $input->getImageFileWebPath();
        } else {
            $path = $input;
        }

        $router = $this->container->get('router');
        return $router->generate('liip_imagine_filter', [
            'path' => ltrim($path, '/'),
            'filter' => $filter
        ], $absolute);
    }

}