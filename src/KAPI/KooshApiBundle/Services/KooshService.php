<?php

namespace KAPI\KooshApiBundle\Services;

use KAPI\KooshApiBundle\Entity\Koosh;
use Symfony\Component\DependencyInjection\ContainerInterface;

class KooshService
{
    /**
     * @var ContainerInterface
     */
    protected $container;
    protected $em;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->em = $container->get('doctrine.orm.entity_manager');
    }

    public function update(Koosh $koosh) {
        $koosh->setRamProcessDirPath($this->container->getParameter('ram_disk_process_folder_path'));
        $this->em->persist($koosh);
        $this->em->flush();
    }
}
