<?php

namespace KAPI\KooshApiBundle\Utils;

class LocationHelper
{

    private static $instance = false;

    public function __construct()
    {
        /**
         * 
         */
    }

    public static function getInstance(){
        if(self::$instance === false){
            self::$instance = new LocationHelper;
        }
        return self::$instance;
    }
    
    /**
     * Function returns distance between two points defined by lat and lon in miles.
     * 
     * @param float $latitude1
     * @param float $longitude1
     * @param float $latitude2
     * @param float $longitude2
     * @return float
     */
    static function calculateDistance($latitude1, $longitude1, $latitude2, $longitude2) {
        $theta = $longitude1 - $longitude2;
        $miles = (sin(deg2rad($latitude1)) * sin(deg2rad($latitude2))) + (cos(deg2rad($latitude1)) * cos(deg2rad($latitude2)) * cos(deg2rad($theta)));
        $miles = acos($miles);
        $miles = rad2deg($miles);
        $miles = $miles * 60 * 1.1515;
        return $miles; 
    }
    
    static function convertMilesToMeters($value) {
        return $value * 1609.34;
    }

}